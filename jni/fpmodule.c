#include <string.h>
#include<pthread.h>
#include "fprotocol.h"
#include <jni.h>

#ifdef __cplusplus
extern "C"
#endif

#define DEV_ADDR 	0xffffffff

#define NCM_IMAGE		0x00
#define NCM_ENROL		0x01
#define NCM_GENCHAR		0x02

#define FPM_DEVICE		0x01
#define FPM_PLACE		0x02
#define FPM_LIFT		0x03
#define FPM_CAPTURE		0x04
#define FPM_GENCHAR		0x05
#define FPM_ENRFPT		0x06
#define FPM_NEWIMAGE	0x07
#define FPM_TIMEOUT		0x08
#define FPM_IMGVAL		0x09

static int		g_iTransStyle=-1;
static int		g_iworkmsg[5];
static int		g_iretmsg[5];
static int		g_imsginc;

static int		g_IsOpen=0;
static int		g_IsLink=0;
static int		g_IsWork=0;
static int    	g_IsExit=0;
static int		g_IsBig=1;
static int		g_IsUp=1;

static pthread_t 		g_thid;
static pthread_attr_t 	g_thattr;

int g_nImageLen=0;
unsigned char g_pImageData[IMAGE_X*IMAGE_Y];

int g_nImageLenEx=0;
unsigned char g_pImageDataEx[OIMAGE_X*OIMAGE_Y];

unsigned char g_pTpRef[512];
unsigned char g_pTpMat[512];
int g_nRefSize=0;
int g_nMatSize=0; 

unsigned char g_pTpBuf[512];
int g_nTpSize=0;

void DelayX(int nTimes)
{
	usleep(nTimes*1000);
	return;
}

jint Java_com_module_fpmodule_FPModule_SetImageType(JNIEnv* env,jobject obe,jint itype)
{
	g_IsBig=itype;
	return 1;
}

jint Java_com_module_fpmodule_FPModule_SetUpImage(JNIEnv* env,jobject obe,jint iup)
{
	g_IsUp=iup;
	return 1;
}

jint Java_com_module_fpmodule_FPModule_OpenDevice(JNIEnv* env,jobject obe)
{
	if(!g_IsOpen)
	{
		g_IsOpen=PSOpenDevice(DEVICE_COM,3,48,2);
	}
	return g_IsOpen;
}

jint Java_com_module_fpmodule_FPModule_StopWork(JNIEnv* env,jobject obe)
{
	int i=0;
	if(g_IsWork)
	{
		g_IsExit=1;
		while(1)
		{
		 	Delay(100*1000);
		 	if(g_IsExit==0)
		 		break;
		}
	}
	memset(g_iworkmsg,-1,5);
	memset(g_iretmsg,-1,5);
	g_imsginc=0;
	return 1;
}

jint Java_com_module_fpmodule_FPModule_CloseDevice(JNIEnv* env,jobject obe)
{
	int i=0;
	if(g_IsWork)
	{
		g_IsExit=1;
		while(1)
		{
		 	Delay(100*1000);
		 	if(g_IsExit==0)
		 		break;
		}
	}
	memset(g_iworkmsg,-1,5);
	memset(g_iretmsg,-1,5);
	g_imsginc=0;
	
	if(g_IsOpen==1)
	{
		g_IsOpen=0;
		g_IsLink=0;
		PSCloseDevice();
	}
	return 1;
}

jint Java_com_module_fpmodule_FPModule_LinkDevice(JNIEnv* env,jobject obe)
{
	unsigned char pwd[4];
	pwd[0]=0x00;
	pwd[1]=0x00;
	pwd[2]=0x00;
	pwd[3]=0x00;
		
	DelayX(100);
	
	if(g_IsWork==1)
		return 0;

	if(PSVfyPwd(DEV_ADDR,pwd)==0)
	{
		g_IsLink=1;
		return 1;
	}
	g_IsLink=0;
	return 0;
}

void PostNclMsg(int sdkmsg,int psdkresult)
{
	g_iworkmsg[g_imsginc]=sdkmsg;
	g_iretmsg[g_imsginc]=psdkresult;	
	g_imsginc++;
	if(g_imsginc>4)
	{
		g_imsginc=0;
	}
}

jint Java_com_module_fpmodule_FPModule_GetWorkMsg(JNIEnv* env,jobject obe)
{
	int i;
	int bret=-1;
	for(i=0;i<5;i++)
	{
		if(g_iworkmsg[i]>-1)
		{
			bret=g_iworkmsg[i];
			g_iworkmsg[i]=-1;
			return bret;
		}
	}
	g_imsginc=0;
	return -1;
}

jint Java_com_module_fpmodule_FPModule_GetRetMsg(JNIEnv* env,jobject obe)
{
	int i;
	int bret=-1;
	for(i=0;i<5;i++)
	{	
		if(g_iretmsg[i]>-1)
		{
			bret=g_iretmsg[i];
			g_iretmsg[i]=-1;
			return bret;
		}
	}
	g_imsginc=0;
	return -1;
}

jint Java_com_module_fpmodule_FPModule_ReleaseMsg(JNIEnv* env,jobject obe)
{
	return 0;
}

int ZGetImage()
{
	DelayX(100);
	if(g_IsBig==1)
	{
		return PSGetImage(DEV_ADDR);
	}
	else
	{
		return FPGetImageEx(DEV_ADDR);
	}
}

int ZUpImage()
{
	if(g_IsUp==0)
		return 0;

	DelayX(100);
	if(g_IsBig==1)
	{
		return PSUpImage(DEV_ADDR,g_pImageData,&g_nImageLen);
	}
	else
	{
		return FPUpImageEx(DEV_ADDR,g_pImageDataEx,&g_nImageLenEx);
	}
}

int ZGenChar(int iBufferID)
{
	DelayX(100);
	if(g_IsBig==1)
	{
		return PSGenChar(DEV_ADDR,iBufferID);
	}
	else
	{
		return FPGenCharEx(DEV_ADDR,iBufferID);
	}
}

void * ThreadProc()
{
	int count=0;
	if(!g_IsOpen)
	{
		PostNclMsg(FPM_DEVICE,0);
		goto exit;
	}
	if(!g_IsLink)
	{
		PostNclMsg(FPM_DEVICE,0);
		goto exit;
	}

	switch(g_iTransStyle)
	{
	case NCM_IMAGE:
		{
			PostNclMsg(FPM_PLACE,0);
			while((ZGetImage())==PS_NO_FINGER)
			{
			   if(g_IsExit==1)
				   goto exit;
			   count++;
			   if(count>=20){
				   PostNclMsg(FPM_TIMEOUT,0);
				   goto exit;
			   }
			}
			//PostNclMsg(FPM_LIFT,0);
			if(ZUpImage()==PS_OK)
			{
				if(g_IsUp)
					PostNclMsg(FPM_NEWIMAGE,0);
				PostNclMsg(FPM_CAPTURE,1);
			}
			else
			{
				PostNclMsg(FPM_CAPTURE,0);
			}
		}
		break;
	case NCM_ENROL:
		{
			PostNclMsg(FPM_PLACE,0);
			while((ZGetImage())==PS_NO_FINGER)
			{
			  if(g_IsExit==1)
			   				goto exit;
			}
			if(ZUpImage()!=PS_OK)
			{
				PostNclMsg(FPM_ENRFPT,0);
					goto exit;
				count++;
				if(count>=20){
				PostNclMsg(FPM_TIMEOUT,0);
				   goto exit;
				}
			}
			if(g_IsUp)
				PostNclMsg(FPM_NEWIMAGE,0);
			DelayX(100);
			if(ZGenChar(0x01)!=0)
			{
				PostNclMsg(FPM_ENRFPT,0);
				goto exit;
			}
			PostNclMsg(FPM_LIFT,0);
			count=0;
			DelayX(100);
			while((ZGetImage())!=PS_NO_FINGER)
			{
			   DelayX(100);
			   if(g_IsExit==1)
				   goto exit;
				count++;
				if(count>=20){
					PostNclMsg(FPM_TIMEOUT,0);
				   goto exit;
				}
			}
			PostNclMsg(FPM_PLACE,0);
			count=0;
			while((ZGetImage())==PS_NO_FINGER)
			{
				DelayX(100);
			  	if(g_IsExit==1)
				  goto exit;
				count++;
				if(count>=20){
				PostNclMsg(FPM_TIMEOUT,0);
				   goto exit;
				}
			}
			if(ZUpImage()!=PS_OK)
			{
				PostNclMsg(FPM_ENRFPT,0);
				goto exit;
			}
			if(g_IsUp)
				PostNclMsg(FPM_NEWIMAGE,0);
			DelayX(100);
			if(ZGenChar(0x02)!=0)
			{
				PostNclMsg(FPM_ENRFPT,0);
				goto exit;
			}
			DelayX(100);
			if(PSRegModule(DEV_ADDR)==0)
			{
				DelayX(100);
				PSUpChar(DEV_ADDR,0x01,g_pTpRef,&g_nRefSize);
				g_nRefSize=512;
				PostNclMsg(FPM_ENRFPT,1);
			}
			else
			{
				PostNclMsg(FPM_ENRFPT,0);
			}
		}
		break;
	case NCM_GENCHAR:
		{
			PostNclMsg(FPM_PLACE,0);
			while((ZGetImage())==PS_NO_FINGER)
			{
			    if(g_IsExit==1)
			    	goto exit;
				count++;
				if(count>=20){
				PostNclMsg(FPM_TIMEOUT,0);
				   goto exit;
				}
			}
			//PostNclMsg(FPM_LIFT,0)
			if(ZUpImage()==PS_OK)
			{
				if(g_IsUp)
					PostNclMsg(FPM_NEWIMAGE,0);
				DelayX(100);
				if(ZGenChar(0x01)==0)
				{
					DelayX(100);
					PSUpChar(DEV_ADDR,0x01,g_pTpMat,&g_nMatSize);
					DelayX(100);
					g_nMatSize=256;
					PostNclMsg(FPM_GENCHAR,1);
				}
				else
				{
					PostNclMsg(FPM_GENCHAR,0);
				}	
			}
			else
			{
				PostNclMsg(FPM_GENCHAR,0);
			}
		}
		break;
	}
exit:
	g_IsExit=0;
	g_IsWork=0;
	pthread_exit(0);
	return ((void *)0);
}

jint Java_com_module_fpmodule_FPModule_CaptureImage(JNIEnv* env,jobject obe)
{
	int ret;
	if(!g_IsWork)
	{
		memset(g_iworkmsg,-1,5);
		memset(g_iretmsg,-1,5);
		g_imsginc=0;
		
		g_IsExit=0;
		g_IsWork=1;
		g_iTransStyle=NCM_IMAGE;
		//pthread_attr_init(&g_thattr);
		//pthread_attr_setdetachstate(&g_thattr,PTHREAD_CREATE_DETACHED);
		//ret=pthread_create(&g_thid,&g_thattr,ThreadProc,0);		
		ret=pthread_create(&g_thid,NULL,ThreadProc,0);
	}	
	return 1;
}

jint Java_com_module_fpmodule_FPModule_GenFpChar(JNIEnv* env,jobject obe)
{
	int ret;
	if(!g_IsWork)
	{
		memset(g_iworkmsg,-1,5);
		memset(g_iretmsg,-1,5);
		g_imsginc=0;
	
		g_IsExit=0;
		g_IsWork=1;
		g_iTransStyle=NCM_GENCHAR;
		ret=pthread_create(&g_thid,NULL,ThreadProc,0);		
	}	
	return 1;
}

jint Java_com_module_fpmodule_FPModule_EnrolFpChar(JNIEnv* env,jobject obe)
{
	int ret;
	if(!g_IsWork)
	{
		memset(g_iworkmsg,-1,5);
		memset(g_iretmsg,-1,5);
		g_imsginc=0;
	
		g_IsExit=0;
		g_IsWork=1;
		g_iTransStyle=NCM_ENROL;
		ret=pthread_create(&g_thid,NULL,ThreadProc,0);
	}	
	return 1;
}

jint Java_com_module_fpmodule_FPModule_SaveImageToFile(JNIEnv* env,jobject obe,jstring sfile)
{
	if(g_IsBig==1)
	{
		if(g_nImageLen>0)
		{
			const char * filename;
			filename=(*env)->GetStringUTFChars(env,sfile,0);
			PSImgData2BMP(g_pImageData,filename,IMAGE_X,IMAGE_Y);
			(*env)->ReleaseStringUTFChars(env,sfile,filename);
			return 1;
		}
	}
	else
	{
		if(g_nImageLenEx>0)
		{
			const char * filename;
			filename=(*env)->GetStringUTFChars(env,sfile,0);
			PSImgData2BMP(g_pImageDataEx,filename,OIMAGE_X,OIMAGE_Y);
			(*env)->ReleaseStringUTFChars(env,sfile,filename);
			return 1;
		}
	}

	return 0;
}
			
jint Java_com_module_fpmodule_FPModule_GetImageData(JNIEnv* env,jobject obe,jbyteArray imagedata,jintArray size)
{
	if(g_IsBig==1)
	{
		if(g_nImageLenEx>0)
		{
			(*env)->SetByteArrayRegion(env,imagedata,0,g_nImageLen,(jbyte*)g_pImageData);
			(*env)->SetIntArrayRegion(env,size,0,1,(jint *)&g_nImageLen);
			return 1;
		}
	}
	else
	{
		if(g_nImageLen>0)
		{
			(*env)->SetByteArrayRegion(env,imagedata,0,g_nImageLenEx,(jbyte*)g_pImageDataEx);
			(*env)->SetIntArrayRegion(env,size,0,1,(jint *)&g_nImageLenEx);

			return 1;
		}
	}

	return 0;
}

jint Java_com_module_fpmodule_FPModule_GetImageBmp(JNIEnv* env,jobject obe,jbyteArray bmpdata,jintArray size)
{
	if(g_IsBig==1)
	{
		unsigned char bmpbuf[74806];
		int bufsize=74806;
		unsigned char head[1078]={
		    	0x42,0x4d,//file type
				0x0,0x0,0x0,0x00, //file size***
				0x00,0x00, //reserved
				0x00,0x00,//reserved
				0x36,0x4,0x00,0x00,//head byte***
				0x28,0x00,0x00,0x00,//struct size
				0x00,0x00,0x0,0x00,//map width***
				0x00,0x00,0x00,0x00,//map height***
				0x01,0x00,//must be 1
				0x08,0x00,//color count***
				0x00,0x00,0x00,0x00, //compression
				0x00,0x00,0x00,0x00,//data size***
				0x00,0x00,0x00,0x00, //dpix
				0x00,0x00,0x00,0x00, //dpiy
				0x00,0x00,0x00,0x00,//color used
				0x00,0x00,0x00,0x00,//color important
		};
		int i,j;
		long num;
		num=IMAGE_X; head[18]= num & 0xFF;
		num=num>>8;  head[19]= num & 0xFF;
		num=num>>8;  head[20]= num & 0xFF;
		num=num>>8;  head[21]= num & 0xFF;


		num=IMAGE_Y; head[22]= num & 0xFF;
		num=num>>8;  head[23]= num & 0xFF;
		num=num>>8;  head[24]= num & 0xFF;
		num=num>>8;  head[25]= num & 0xFF;
		j=0;
		for (i=54;i<1078;i=i+4)
		{
			head[i]=head[i+1]=head[i+2]=j;
			head[i+3]=0;
			j++;
		}
		memcpy(bmpbuf,head,1078);
		memcpy(&bmpbuf[1078],g_pImageData,g_nImageLen);
		//for(i=0;i<=IMAGE_Y-1;i++ )
		//{
		//	memcpy(&bmpbuf[1078*sizeof(char)+(IMAGE_Y-1-i)*IMAGE_X],&g_pImageData[i*IMAGE_X],IMAGE_X);
		//}

		if(g_nImageLen>0)
		{
			(*env)->SetByteArrayRegion(env,bmpdata,0,bufsize,(jbyte*)bmpbuf);
			(*env)->SetIntArrayRegion(env,size,0,1,(jint *)&bufsize);
			return 1;
		}
	}
	else
	{
		unsigned char bmpbuf[31478];
		int bufsize=31478;
		unsigned char head[1078]={
		    	0x42,0x4d,//file type
				0x0,0x0,0x0,0x00, //file size***
				0x00,0x00, //reserved
				0x00,0x00,//reserved
				0x36,0x4,0x00,0x00,//head byte***
				0x28,0x00,0x00,0x00,//struct size
				0x00,0x00,0x0,0x00,//map width***
				0x00,0x00,0x00,0x00,//map height***
				0x01,0x00,//must be 1
				0x08,0x00,//color count***
				0x00,0x00,0x00,0x00, //compression
				0x00,0x00,0x00,0x00,//data size***
				0x00,0x00,0x00,0x00, //dpix
				0x00,0x00,0x00,0x00, //dpiy
				0x00,0x00,0x00,0x00,//color used
				0x00,0x00,0x00,0x00,//color important
		};
		int i,j;
		long num;
		num=OIMAGE_X; head[18]= num & 0xFF;
		num=num>>8;  head[19]= num & 0xFF;
		num=num>>8;  head[20]= num & 0xFF;
		num=num>>8;  head[21]= num & 0xFF;


		num=OIMAGE_Y; head[22]= num & 0xFF;
		num=num>>8;  head[23]= num & 0xFF;
		num=num>>8;  head[24]= num & 0xFF;
		num=num>>8;  head[25]= num & 0xFF;
		j=0;
		for (i=54;i<1078;i=i+4)
		{
			head[i]=head[i+1]=head[i+2]=j;
			head[i+3]=0;
			j++;
		}
		memcpy(bmpbuf,head,1078);
		memcpy(&bmpbuf[1078],g_pImageDataEx,g_nImageLenEx);
		//for(i=0;i<=OIMAGE_Y-1;i++ )
		//{
		//	memcpy(&bmpbuf[1078*sizeof(char)+(OIMAGE_Y-1-i)*OIMAGE_X],&g_pImageDataEx[i*OIMAGE_X],OIMAGE_X);
		//}

		if(g_nImageLenEx>0)
		{
			(*env)->SetByteArrayRegion(env,bmpdata,0,bufsize,(jbyte*)bmpbuf);
			(*env)->SetIntArrayRegion(env,size,0,1,(jint *)&bufsize);
			return 1;
		}
		return 0;
	}

	return 0;
}

jint Java_com_module_fpmodule_FPModule_GetFpCharByGen(JNIEnv* env,jobject obe,jbyteArray tpbuf,jintArray tpsize)
{
	if(g_nMatSize>0)
	{
		(*env)->SetByteArrayRegion(env,tpbuf,0,g_nMatSize,(jbyte*)g_pTpMat);   
		(*env)->SetIntArrayRegion(env,tpsize,0,1,(jint *)&g_nMatSize);
		return 1;
	}
	return 0;
}

jint Java_com_module_fpmodule_FPModule_GetFpCharByEnl(JNIEnv* env,jobject obe,jbyteArray fpbuf,jintArray fpsize)
{
	if(g_nRefSize>0)
	{
		(*env)->SetByteArrayRegion(env,fpbuf,0,g_nRefSize,(jbyte*)g_pTpRef);   
		(*env)->SetIntArrayRegion(env,fpsize,0,1,(jint *)&g_nRefSize);
		
		return 1;
	}
	return 0;
}

jint Java_com_module_fpmodule_FPModule_MatchTemplate(JNIEnv * env, jobject obe, jbyteArray lpref, jint refsize, jbyteArray lpmat, jint matsize)
{
	int iScore=0;
	if(!g_IsOpen)return 0;

	jbyte * arrayByte1= (*env)->GetByteArrayElements(env,lpref,0);
	unsigned char * pref = (unsigned char *)arrayByte1;
		
	jbyte * arrayByte2= (*env)->GetByteArrayElements(env,lpmat,0);
	unsigned char * pmat = (unsigned char *)arrayByte2;

	DelayX(100);
	if(PSDownChar(DEV_ADDR,0x02,pref,refsize)==0)
	{
		DelayX(100);
		if(PSDownChar(DEV_ADDR,0x01,pmat,matsize)==0)
		{
			DelayX(100);
			PSMatch(DEV_ADDR,&iScore);
		}
	}
	
	(*env)->ReleaseByteArrayElements(env, lpref, arrayByte1, 0); 
 	(*env)->ReleaseByteArrayElements(env, lpmat, arrayByte2, 0);
 	
 	return iScore;
}


//�ײ㺯��
jint Java_com_module_fpmodule_FPModule_FPGetImage(JNIEnv* env,jobject obe)
{
	if((g_IsLink==1)&&(g_IsWork==0))
	{
		if(g_IsBig==1)
		{
			if(PSGetImage(DEV_ADDR)==PS_OK)
				return 1;
		}
		else
		{
			if(FPGetImageEx(DEV_ADDR)==PS_OK)
				return 1;
		}
	}
	return 0;
}

jint Java_com_module_fpmodule_FPModule_FPGenChar(JNIEnv* env,jobject obe,jint iBufferID)
{
	if((g_IsLink==1)&&(g_IsWork==0))
	{
		if(g_IsBig==1)
		{
			if(PSGenChar(DEV_ADDR,iBufferID)==PS_OK)
				return 1;
		}
		else
		{
			if(FPGenCharEx(DEV_ADDR,iBufferID)==PS_OK)
				return 1;
		}
	}
	return 0;
}

jint Java_com_module_fpmodule_FPModule_FPMatch(JNIEnv* env,jobject obe,jintArray iScore)
{
	if((g_IsLink==1)&&(g_IsWork==0))
	{
		int score=0;
		if(PSMatch(DEV_ADDR,&score)==0)
		{
			(*env)->SetIntArrayRegion(env,iScore,0,1,(jint *)&score);
			return score;
		}
	}
	return 0;
}

jint Java_com_module_fpmodule_FPModule_FPSearch(JNIEnv* env,jobject obe,jint iBufferID, jint iStartPage, jint iPageNum, jintArray iMbAddress)
{
	if((g_IsLink==1)&&(g_IsWork==0))
	{
		int iAddress=0;
		if(PSSearch(DEV_ADDR,iBufferID,iStartPage,iPageNum,&iAddress)==0)
		{
			(*env)->SetIntArrayRegion(env,iMbAddress,0,1,(jint *)&iAddress);
			return 1;
		}
	}
	return 0;
}

jint Java_com_module_fpmodule_FPModule_FPRegModule(JNIEnv* env,jobject obe)
{
	if((g_IsLink==1)&&(g_IsWork==0))
	{
		if(PSRegModule(DEV_ADDR)==0)
			return 1;
	}
	return 0;
}

jint Java_com_module_fpmodule_FPModule_FPStoreChar(JNIEnv* env,jobject obe,jint iBufferID, jint iPageID)
{
	if((g_IsLink==1)&&(g_IsWork==0))
	{
		if(PSStoreChar(DEV_ADDR,iBufferID,iPageID)==0)
			return 1;
	}
	return 0;
}

jint Java_com_module_fpmodule_FPModule_FPLoadChar(JNIEnv* env,jobject obe,jint iBufferID,jint iPageID)
{
	if((g_IsLink==1)&&(g_IsWork==0))
	{
		if(PSLoadChar(DEV_ADDR,iBufferID,iPageID)==0)
			return 1;
	}
	return 0;
}

jint Java_com_module_fpmodule_FPModule_FPUpChar(JNIEnv* env,jobject obe,jint iBufferID, jbyteArray pTemplet, jintArray iTempletLength)
{
	if((g_IsLink==1)&&(g_IsWork==0))
	{
		if(PSUpChar(DEV_ADDR,iBufferID, g_pTpBuf, &g_nTpSize)==0)
		{
			(*env)->SetByteArrayRegion(env,pTemplet,0,g_nTpSize,(jbyte*)g_pTpBuf);
			(*env)->SetIntArrayRegion(env,iTempletLength,0,1,(jint *)&g_nTpSize);
		}
	}
	return 0;
}

jint Java_com_module_fpmodule_FPModule_FPDownChar(JNIEnv* env,jobject obe,jint iBufferID, jbyteArray pTemplet, jint iTempletLength)
{
	if((g_IsLink==1)&&(g_IsWork==0))
	{
		jbyte * arrayByte= (*env)->GetByteArrayElements(env,pTemplet,0);
		unsigned char * pref = (unsigned char *)arrayByte;
		if(PSDownChar(DEV_ADDR,iBufferID,pref,iTempletLength)==0)
		{
			(*env)->ReleaseByteArrayElements(env, pTemplet, arrayByte, 0);
			return 1;
		}
		(*env)->ReleaseByteArrayElements(env, pTemplet, arrayByte, 0);
	}
	return 0;
}

jint Java_com_module_fpmodule_FPModule_FPUpImage(JNIEnv* env,jobject obe,jbyteArray pImageData, jintArray iImageLength)
{
	if((g_IsLink==1)&&(g_IsWork==0))
	{
		if(g_IsBig==1)
		{
			if(PSUpImage(DEV_ADDR,g_pImageData,&g_nImageLen)==PS_OK)
			{
				(*env)->SetByteArrayRegion(env,pImageData,0,g_nImageLen,(jbyte*)g_pImageData);
				(*env)->SetIntArrayRegion(env,iImageLength,0,1,(jint *)&g_nImageLen);
				return 1;
			}
		}
		else
		{
			if(FPUpImageEx(DEV_ADDR,g_pImageDataEx,&g_nImageLenEx)==PS_OK)
			{
				(*env)->SetByteArrayRegion(env,pImageData,0,g_nImageLenEx,(jbyte*)g_pImageDataEx);
				(*env)->SetIntArrayRegion(env,iImageLength,0,1,(jint *)&g_nImageLenEx);
				return 1;
			}
		}
	}
	return 0;
}

jint Java_com_module_fpmodule_FPModule_FPDownImage(JNIEnv* env,jobject obe,jbyteArray pImageData, jint iLength)
{
	if((g_IsLink==1)&&(g_IsWork==0))
	{
		jbyte * arrayByte= (*env)->GetByteArrayElements(env,pImageData,0);
		unsigned char * pimage = (unsigned char *)arrayByte;
		if(PSDownImage(DEV_ADDR,pimage,iLength)==0)
		{
			(*env)->ReleaseByteArrayElements(env, pImageData, arrayByte, 0);
			return 1;
		}
		(*env)->ReleaseByteArrayElements(env, pImageData, arrayByte, 0);
	}
	return 0;
}

jint Java_com_module_fpmodule_FPModule_FPDelChar(JNIEnv* env,jobject obe,jint iStartPageID,jint nDelPageNum)
{
	if((g_IsLink==1)&&(g_IsWork==0))
	{
		if(PSDelChar(DEV_ADDR,iStartPageID,nDelPageNum)==0)
			return 1;
	}
	return 0;
}

jint Java_com_module_fpmodule_FPModule_FPEmpty(JNIEnv* env,jobject obe)
{
	if((g_IsLink==1)&&(g_IsWork==0))
	{
		if(PSEmpty(DEV_ADDR)==PS_OK)
			return 1;
	}
	return 0;
}

jint Java_com_module_fpmodule_FPModule_FPReadInfo(JNIEnv* env,jobject obe,jint nPage,jbyteArray UserContent)
{
	if((g_IsLink==1)&&(g_IsWork==0))
	{
		unsigned char buffer[32];
		if(PSReadInfo(DEV_ADDR,nPage,buffer)==PS_OK)
		{
			(*env)->SetByteArrayRegion(env,UserContent,0,32,(jbyte*)buffer);
			return 1;
		}
	}
	return 0;
}

jint Java_com_module_fpmodule_FPModule_FPWriteInfo(JNIEnv* env,jobject obe,jint nPage,jbyteArray UserContent)
{
	if((g_IsLink==1)&&(g_IsWork==0))
	{
		jbyte * arrayByte= (*env)->GetByteArrayElements(env,UserContent,0);
		unsigned char * pcontent = (unsigned char *)arrayByte;
		if(PSWriteInfo(DEV_ADDR,nPage,pcontent)==0)
		{
			(*env)->ReleaseByteArrayElements(env, UserContent, arrayByte, 0);
			return 1;
		}
		(*env)->ReleaseByteArrayElements(env, UserContent, arrayByte, 0);
	}
	return 0;
}

jint Java_com_module_fpmodule_FPModule_FPSetSecurLevel(JNIEnv* env,jobject obe,jint nLevel)
{
	if((g_IsLink==1)&&(g_IsWork==0))
	{
		if(PSSetSecurLevel(DEV_ADDR,nLevel)==PS_OK)
			return 1;
	}
	return 0;
}

jint Java_com_module_fpmodule_FPModule_FPGetRandomData(JNIEnv* env,jobject obe,jbyteArray pRandom)
{
	if((g_IsLink==1)&&(g_IsWork==0))
	{
		unsigned char buffer[32];
		if(PSGetRandomData(DEV_ADDR,buffer)==PS_OK)
		{
			(*env)->SetByteArrayRegion(env,pRandom,0,4,(jbyte*)buffer);
			return 1;
		}
	}
	return 0;
}


