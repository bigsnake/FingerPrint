 
#include <stdio.h>
#include <time.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>
#include "serial.h"
#include "fpdevice.h"

int       g_nComPort     = 0;
int       g_nBaudRate    = 57600;
const int   WAITTIME_PER_BYTE =  100;   //(ms)

/**********************************************
  FUNCTION:   InitCom
***********************************************/
BOOL   OpenCom(int comPort, int baudRate)
{
	int baud;
	baud=baudRate*9600; 
	g_nBaudRate = baud;
	g_nComPort = comPort;
	if( OpenComPort(comPort,baud,8,"2",0)==0)
    {
		ClearComPort();
		 return TRUE;
    }
	return FALSE;
}

/**********************************************
  FUNCTION:   ReleaseCom
***********************************************/
BOOL   CloseCom()
{
	CloseComPort();
	return TRUE;
}
 
void ClearCom()
{
  ClearComPort();
}

/**********************************************
  FUNCTION:   GetByte
***********************************************/
BOOL   GetByte(unsigned char* tranChar)
{
	if (tranChar == NULL)
		return FALSE;

    if(ReadComPort(tranChar,1)==-1)
	//if(ReadCom(tranChar,1)==-1)
	   return FALSE;

    return TRUE;
}

/**********************************************
  FUNCTION:   SendByte
***********************************************/
BOOL  SendByte(unsigned char tranChar)
{
   
    unsigned char pBuf[2]={0};
    pBuf[0]=tranChar;
    if(WriteComPort(pBuf,1)!=1)
    //if(WriteCom(pBuf,1)!=1)
		return FALSE;

    return TRUE;
} 

void dDelay(int nTimes)
{
  usleep(nTimes);
  return;
 }
