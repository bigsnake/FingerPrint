#ifndef _SYDEVICE_H_
#define _SYDEVICE_H_

#include "vardef.h"

extern int       g_nComPort;
extern int       g_nBaudRate;

//COM RS232
extern void dDelay(int nTimes);
extern void ClearCom();
extern BOOL GetByte(unsigned char* tranChar);
extern BOOL  SendByte(unsigned char tranChar);
extern BOOL OpenCom(int comPort, int baudRate);
extern BOOL  CloseCom();

#endif
