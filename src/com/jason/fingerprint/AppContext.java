/**
 * @Title: AppContext.java
 * @Package: com.jason.fingerprint
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月18日 下午5:19:48
 * @Version: V1.0
 */
package com.jason.fingerprint;

import org.kymjs.aframe.database.KJDB;
import org.kymjs.aframe.utils.StringUtils;
import org.kymjs.aframe.utils.SystemTool;

import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.os.Vibrator;
import android.util.Log;

import com.baidu.location.GeofenceClient;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.LocationClientOption.LocationMode;
import com.jason.fingerprint.beans.UserBean;
import com.jason.fingerprint.common.FileHelper;
import com.jason.fingerprint.common.ImageLoaderHelper;
import com.jason.fingerprint.common.PreferenceHelper;
import com.jason.fingerprint.listener.MyDbUpdateListener;
import com.jason.fingerprint.listener.MyLocationListener;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * @ClassName: AppContext
 * @Description: 全局应用程序类：用于保存和调用全局应用配置及访问网络数据
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月18日 下午5:19:48
 */
public class AppContext extends Application {
	
	private static final String TAG = "AppContext";

	private static final boolean DB_ISDEBUG = true;
	private static final String DB_NAME = "FingerPrint";
	private static final int DB_VERSION = 1;
	public static final int PAGE_SIZE = 20;// 默认分页大小

	/** 单例*/
	public static AppContext mInstance;
	/** 全局Context*/
	public static Context mAppContext;

	// Baidu LBS
	public LocationClient mLocationClient;
	public GeofenceClient mGeofenceClient;
	public MyLocationListener mMyLocationListener;
	public Vibrator mVibrator;

	private KJDB kjdb;// 数据库初始化
	private UserBean userBean;// 当前的登录信息
	
	public static AppContext getInstance(){
		return mInstance;
	}
	
	public static Context getAppContext(){
		if (mAppContext == null)
			mAppContext = mInstance.getApplicationContext();
		return mAppContext;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		mInstance = this;
		mAppContext = getApplicationContext();
		
		//程序异常崩溃捕获
	    Thread.setDefaultUncaughtExceptionHandler(new UncaughtException());
		// 初始化数据库
		//kjdb = KJDB.create(getApplicationContext(), DB_NAME, DB_ISDEBUG);
		kjdb = KJDB.create(mAppContext, FileHelper.getSqlPath(mAppContext), DB_NAME,
				DB_ISDEBUG, DB_VERSION, new MyDbUpdateListener());
		initBaiduLBS();
	}

	private void initBaiduLBS() {
		mLocationClient = new LocationClient(this.getApplicationContext());
		mMyLocationListener = new MyLocationListener(mAppContext);
		mLocationClient.registerLocationListener(mMyLocationListener);
		mGeofenceClient = new GeofenceClient(getApplicationContext());
		mVibrator = (Vibrator) getApplicationContext().getSystemService(
				Service.VIBRATOR_SERVICE);
		initLocation();
	}

	// 初始化定位
	private void initLocation() {
		LocationClientOption option = new LocationClientOption();
		option.setLocationMode(LocationMode.Hight_Accuracy);
		option.setCoorType("bd09ll");// 返回的定位结果是百度经纬度,默认值gcj02
		option.setScanSpan(5000);// 设置发起定位请求的间隔时间为5000ms
		option.setIsNeedAddress(true);
		option.setNeedDeviceDirect(true);// 返回的定位结果包含手机机头的方向
		mLocationClient.setLocOption(option);
	}
	
	/**
	 * @return the kjdb
	 */
	public KJDB getKjdb() {
		return kjdb;
	}

	/**
	 * @param kjdb
	 *            the kjdb to set
	 */
	public void setKjdb(KJDB kjdb) {
		this.kjdb = kjdb;
	}

	/**
	 * @return the userBean
	 */
	public UserBean getUserBean() {
		return PreferenceHelper.getLoginInfoFormPreference(this);
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserBean userBean) {
		if (userBean == null) {
			this.userBean = PreferenceHelper.getLoginInfoFormPreference(this);
		} else {
			this.userBean = userBean;
		}
	}

	// 是否登录
	public boolean isLogin() {
		return userBean != null
				&& !StringUtils.isEmail(userBean.getLoginName());
	}

	/**
	 * @return the imei 当前设备的imei号码
	 */
	public String getImei() {
		return SystemTool.getPhoneIMEI(this);
	}

	/**
	 * @return the machId 当前登录用户的隶属机构ID
	 */
	public int getMachId() {
		int machId = 0;
		if (isLogin()) {
			machId = userBean.getOrganId();
		}
		return machId;
	}

	public DisplayImageOptions getDisplayImageOptions() {
		return ImageLoaderHelper.getDisplayImageOptions(mAppContext);
	}
	
	/**
	 * 当程序出现未捕获异常崩溃时调用
	 */
	public void onFC(){
		Log.e(TAG, "onFC");
		int pid = android.os.Process.myPid();
        android.os.Process.killProcess(pid);
	}

}
