/**
 * @Title: PreviewPagerAdapter.java
 * @Package: com.jason.fingerprint.adapter
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月22日 下午9:39:58
 * @Version: V1.0
 */
package com.jason.fingerprint.adapter;

import org.kymjs.aframe.bitmap.KJBitmap;
import org.kymjs.aframe.bitmap.KJBitmapConfig;
import org.kymjs.aframe.utils.DensityUtils;
import org.kymjs.aframe.utils.StringUtils;

import uk.co.senab.photoview.PhotoView;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.jason.fingerprint.R;
import com.jason.fingerprint.common.FileHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * @ClassName: PreviewPagerAdapter
 * @Description: 图片预览器适配器
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月22日 下午9:39:58
 */
public class PreviewPagerAdapter extends PagerAdapter {
	
	private DisplayImageOptions mDisplayImageOptions;
	private LayoutInflater mLayoutInflater;
	private Context mContext;
	private String[] mImages;
	private KJBitmap mKjBitmap;
	private int mWidth;
	private int mHeight;
	
	public PreviewPagerAdapter(Context context,String [] image,DisplayImageOptions displayImageOptions){
		this.mContext = context;
		mLayoutInflater = LayoutInflater.from(context);
		this.mImages = image;
		this.mDisplayImageOptions = displayImageOptions;
		initBitmap(context);
	}

	public PreviewPagerAdapter(Context context,String [] image){
		this.mContext = context;
		mLayoutInflater = LayoutInflater.from(context);
		this.mImages = image;
		initBitmap(context);
	}

	private void initBitmap(Context context){
		KJBitmapConfig config = new KJBitmapConfig();
		config.openMemoryCache = false;
		config.cachePath = new FileHelper(context).getCachesDir().getPath();
		mKjBitmap = KJBitmap.create(config);
		mWidth = DensityUtils.getScreenW((Activity)context);
		mHeight = DensityUtils.getScreenH((Activity) context);
	}

	/* (non-Javadoc)
	 * @see android.support.v4.view.PagerAdapter#getCount()
	 */
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mImages.length;
	}

	/* (non-Javadoc)
	 * @see android.support.v4.view.PagerAdapter#isViewFromObject(android.view.View, java.lang.Object)
	 */
	
	private View imageLayout = null;
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@Override
	public View instantiateItem(ViewGroup container, int position) {
		ViewHolder holder = null;
		if (imageLayout ==  null) {
			holder = new ViewHolder();
			imageLayout = mLayoutInflater.inflate(R.layout.item_pager_image, container, false);
			holder.imageView =  (PhotoView) imageLayout.findViewById(R.id.image);
			holder.spinner = (ProgressBar) imageLayout.findViewById(R.id.loading);
			imageLayout.setTag(holder);
		}else {
			holder = (ViewHolder) imageLayout.getTag(); 
		}
		String imagePath = mImages[position];
		if (!StringUtils.isEmpty(imagePath)) {
			mKjBitmap.display(holder.imageView, imagePath, mWidth, mHeight);
			holder.spinner.setVisibility(View.INVISIBLE);
		}else {
			holder.imageView.setBackground(null);
		}
		/*final ProgressBar spinner = holder.spinner;
		ImageLoader.getInstance().displayImage("file://" + mImages[position], holder.imageView, mDisplayImageOptions, new SimpleImageLoadingListener() {
			@Override
			public void onLoadingStarted(String imageUri, View view) {
				spinner.setVisibility(View.VISIBLE);
			}

			@Override
			public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
				String message = null;
				switch (failReason.getType()) {
					case IO_ERROR:
						message = "Input/Output error";
						break;
					case DECODING_ERROR:
						message = "Image can't be decoded";
						break;
					case NETWORK_DENIED:
						message = "Downloads are denied";
						break;
					case OUT_OF_MEMORY:
						message = "Out Of Memory error";
						break;
					case UNKNOWN:
						message = "Unknown error";
						break;
				}
				Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();

				spinner.setVisibility(View.GONE);
			}

			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				spinner.setVisibility(View.GONE);
			}
		});*/

		container.addView(imageLayout, 0);
		return imageLayout;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}
	
	
	class ViewHolder{
		PhotoView imageView;
		ProgressBar spinner;
	}

}
