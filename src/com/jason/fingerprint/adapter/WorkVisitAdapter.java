/**
 * @Title: WorkVisitAdapter.java
 * @Package: com.jason.fingerprint.adapter
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月14日10:18:03
 * @Version: V1.0
 */
package com.jason.fingerprint.adapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jason.fingerprint.R;
import com.jason.fingerprint.beans.WorkVisitBean;
import com.jason.fingerprint.common.DateFormat;
import com.jason.fingerprint.utils.DataUtils;

/**
 * @ClassName: WorkVisitAdapter
 * @Description: 工作走访
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月25日20:51:15
 */
public class WorkVisitAdapter extends BaseAdapter {

	private static final String TAG = "WorkVisitAdapter";

	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private List<WorkVisitBean> mWorkVisitBeans;

	public WorkVisitAdapter() {
		this.mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mWorkVisitBeans = new ArrayList<WorkVisitBean>();
	}

	public WorkVisitAdapter(Context context) {
		this.mContext = context;
		this.mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mWorkVisitBeans = new ArrayList<WorkVisitBean>();
	}

	public WorkVisitAdapter(Context context, List<WorkVisitBean> reportBeans) {
		this.mContext = context;
		this.mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mWorkVisitBeans = reportBeans;
		Collections.sort(mWorkVisitBeans, new MyComparator());
	}

	@Override
	public int getCount() {
		return isEmptyOfWorkVisitBeans() ? mWorkVisitBeans.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return isEmptyOfWorkVisitBeans() ? mWorkVisitBeans.get(position) : null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public void addAll(List<WorkVisitBean> reportBeans) {
		this.mWorkVisitBeans = reportBeans;
		Collections.sort(mWorkVisitBeans, new MyComparator());
		notifyDataSetChanged();
	}

	public void addOne(WorkVisitBean bean) {
		if (!isEmptyOfWorkVisitBeans()) {
			mWorkVisitBeans = new ArrayList<WorkVisitBean>();
		}
		if (bean != null) {
			mWorkVisitBeans.add(bean);
		}
		Collections.sort(mWorkVisitBeans, new MyComparator());
		notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Holder holder = null;
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(
					R.layout.item_work_visit_listview, null);
			holder = new Holder();
			holder.mUserNameTextView = (TextView) convertView
					.findViewById(R.id.userName);
			holder.mRymcNameTextView = (TextView) convertView
					.findViewById(R.id.rymcName);
			holder.mTimeTextView = (TextView) convertView
					.findViewById(R.id.time);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		WorkVisitBean bean = (WorkVisitBean) getItem(position);
		if (bean != null) {
			holder.mUserNameTextView.setText(bean.getUserName());
			holder.mRymcNameTextView.setText(bean.getRymcName());
			holder.mTimeTextView.setText(DataUtils.converDatLongToString(
					bean.getInterViewTime(), DateFormat.DATE_YMD_HMS));
		}
		return convertView;
	}

	class Holder {
		TextView mUserNameTextView;
		TextView mRymcNameTextView;
		TextView mTimeTextView;
	}
	
	private class MyComparator implements Comparator<WorkVisitBean>{

		/* (non-Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		@Override
		public int compare(WorkVisitBean lhs, WorkVisitBean rhs) {
			// TODO Auto-generated method stub
			long time1 = lhs.getInterViewTime();
			long time2 = rhs.getInterViewTime();
			return time1 >= time2 ? -1 : 1;
		}
		
	}

	// 判断当前数据是否为空
	public boolean isEmptyOfWorkVisitBeans() {
		return mWorkVisitBeans != null && !mWorkVisitBeans.isEmpty()
				&& mWorkVisitBeans.size() > 0;
	}

}
