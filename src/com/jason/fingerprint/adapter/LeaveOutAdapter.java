/**
 * @Title: LeaveOutAdapter.java
 * @Package: com.jason.fingerprint.adapter
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月9日23:32:04
 * @Version: V1.0
 */
package com.jason.fingerprint.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jason.fingerprint.R;
import com.jason.fingerprint.beans.LeaveOutBean;
import com.jason.fingerprint.common.DataHelper;

/**
 * @ClassName: LeaveOutAdapter
 * @Description: 请销假 ListView 适配器
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月9日23:32:16
 */
public class LeaveOutAdapter extends BaseAdapter {

	private static final String TAG = "LeaveOutAdapter";

	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private List<LeaveOutBean> mLeaveOutBeans;

	public LeaveOutAdapter() {
		this.mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mLeaveOutBeans = new ArrayList<LeaveOutBean>();
	}

	public LeaveOutAdapter(Context context) {
		this.mContext = context;
		this.mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mLeaveOutBeans = new ArrayList<LeaveOutBean>();
	}

	public LeaveOutAdapter(Context context, List<LeaveOutBean> reportBeans) {
		this.mContext = context;
		this.mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mLeaveOutBeans = reportBeans;
	}

	@Override
	public int getCount() {
		return isEmptyOfLeaveOutBeans() ? mLeaveOutBeans.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return isEmptyOfLeaveOutBeans() ? mLeaveOutBeans.get(position) : null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public void addAll(List<LeaveOutBean> reportBeans) {
		this.mLeaveOutBeans = reportBeans;
		notifyDataSetChanged();
	}

	public void addOne(LeaveOutBean bean) {
		if (!isEmptyOfLeaveOutBeans()) {
			mLeaveOutBeans = new ArrayList<LeaveOutBean>();
		}
		if (bean != null) {
			mLeaveOutBeans.add(bean);
		}
		notifyDataSetChanged();
	}

	public void updateOne(LeaveOutBean bean) {
		if (!isEmptyOfLeaveOutBeans()) {
			mLeaveOutBeans = new ArrayList<LeaveOutBean>();
		} else {
			for (LeaveOutBean LeaveOutBean : mLeaveOutBeans) {
				if (LeaveOutBean.getId().equals(bean.getId())) {
					mLeaveOutBeans.remove(LeaveOutBean);
					mLeaveOutBeans.add(bean);
				}
			}
		}
		notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Holder holder = null;
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(
					R.layout.item_leave_out_listview, null);
			holder = new Holder();
			holder.mNameTextView = (TextView) convertView
					.findViewById(R.id.name);
			holder.mOrganTextView = (TextView) convertView
					.findViewById(R.id.organ);
			holder.mContentTextView = (TextView) convertView
					.findViewById(R.id.content);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		LeaveOutBean bean = (LeaveOutBean) getItem(position);
		if (bean != null) {
			holder.mNameTextView.setText(bean.getName());
			holder.mOrganTextView.setText(bean.getOrgan());
			holder.mContentTextView.setText(DataHelper
					.getContentBySignType(bean.getSignType()));
		}
		return convertView;
	}

	class Holder {
		TextView mNameTextView;
		TextView mOrganTextView;
		TextView mContentTextView;
	}

	// 判断当前数据是否为空
	public boolean isEmptyOfLeaveOutBeans() {
		return mLeaveOutBeans != null && !mLeaveOutBeans.isEmpty()
				&& mLeaveOutBeans.size() > 0;
	}

}
