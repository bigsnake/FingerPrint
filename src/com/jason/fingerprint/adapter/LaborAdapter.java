package com.jason.fingerprint.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jason.fingerprint.R;
import com.jason.fingerprint.beans.SponsorInfo;

/**
 * @ClassName: LaborAdapter
 * @Description: CentralizedLaborSignInActivity 适配器
 * @Author: John
 * @Date: 2014/10/21
 */
public class LaborAdapter extends BaseAdapter {

	private Context mContext;

	private LayoutInflater mLayoutInflater;

	private List<SponsorInfo> mSponsorInfo;

	public LaborAdapter(Context context) {
		this.mContext = context;
		this.mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public LaborAdapter(Context context, List<SponsorInfo> sponsorInfo) {
		this.mContext = context;
		this.mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mSponsorInfo = sponsorInfo;
	}

	public void setSponsorInfoList(List<SponsorInfo> sponsorInfo) {
		this.mSponsorInfo = sponsorInfo;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return isEmptyOfSponsorBeans() ? mSponsorInfo.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return isEmptyOfSponsorBeans() ? mSponsorInfo.get(position) : null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		Holder holder = null;
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.item_labor_listview,
					null);
			holder = new Holder();
			holder.mLaborName = (TextView) convertView
					.findViewById(R.id.labor_name);
			holder.mLaborAddress = (TextView) convertView
					.findViewById(R.id.labor_address);
			holder.mLaborTime = (TextView) convertView
					.findViewById(R.id.labor_time);
			holder.mLaborOgran = (TextView) convertView
					.findViewById(R.id.labor_organ);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		SponsorInfo info = (SponsorInfo) getItem(position);
		if (info != null) {
			holder.mLaborName.setText(info.getContent());
			holder.mLaborAddress.setText(info.getAddress());
			holder.mLaborTime.setText(info.getTime());
			holder.mLaborOgran.setText(info.getOrganName());
		}
		return convertView;
	}

	class Holder {
		TextView mLaborName;
		TextView mLaborAddress;
		TextView mLaborTime;
		TextView mLaborOgran;
	}

	// 判断当前数据是否为空
	public boolean isEmptyOfSponsorBeans() {
		return mSponsorInfo != null && mSponsorInfo.size() > 0;
	}

}
