/**
 * @Title: HomeAdapter.java
 * @Package: com.jason.fingerprint.adapter
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月19日 上午9:21:22
 * @Version: V1.0
 */
package com.jason.fingerprint.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jason.fingerprint.R;
import com.jason.fingerprint.beans.ui.ModelBean;

/**
 * @ClassName: HomeAdapter
 * @Description: HomeActitivty GridView 适配器
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月19日 上午9:21:22
 */
public class HomeModelAdapter extends BaseAdapter {
	
	private static final String TAG = "HomeModelAdapter";

	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private String [] mTitleName;
	private int [] mIconId = {R.drawable.module_icon_luru_selector,
			R.drawable.module_icon_tongbu_selector,R.drawable.module_icon_chaxun_selector,
			R.drawable.module_icon_fuwu_selector,R.drawable.module_icon_xuexi_selector,R.drawable.module_icon_baodao_selector,
			R.drawable.module_icon_zoufang_selector,R.drawable.module_icon_qingjia_selector,R.drawable.module_icon_tongzhi_selector,
			R.drawable.module_icon_tixing_selector,R.drawable.module_icon_peizhi_selector,R.drawable.module_icon_women_selector};
	private List<ModelBean> mModelBeans;
	
	public HomeModelAdapter(){
		this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mModelBeans = new ArrayList<ModelBean>();
	}
	
	public HomeModelAdapter(Context context){
		this.mContext = context;
		this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mTitleName = context.getResources().getStringArray(R.array.module_title);
		//this.mIconId = context.getResources().getIntArray(R.array.module_icon);
		initModelBeans();
	}
	
	public HomeModelAdapter(Context context,String [] titleName,int [] iconId){
		this.mContext = context;
		this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mTitleName = titleName;
		this.mIconId = iconId;
		initModelBeans();
	}
	
	//初始化模块数据
	private void initModelBeans(){
		if (this.mModelBeans == null) {
			this.mModelBeans  = new ArrayList<ModelBean>();
		}
		if (this.mTitleName != null && this.mTitleName.length > 0) {
			for (int i = 0; i < mTitleName.length; i++) {
				mModelBeans.add(new ModelBean(mTitleName[i],mIconId[i]));
				Log.i(TAG, "name:" + mTitleName[i] + ",id:" + mIconId[i]);
			}
		}
	}

	@Override
	public int getCount() {
		return isEmptyOfModelBeans() ? mModelBeans.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return isEmptyOfModelBeans() ? mModelBeans.get(position) : null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Holder holder = null;
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.itme_home_gridview, null);
			holder = new Holder();
			holder.mTextView = (TextView) convertView.findViewById(R.id.item_home_gridview_title);
			holder.mImageView = (ImageView) convertView.findViewById(R.id.item_home_gridview_icon);
			convertView.setTag(holder);
		}else{
			holder = (Holder) convertView.getTag();
		}
		ModelBean bean = (ModelBean)getItem(position);
		if (bean != null) {
//			Drawable drawable= mContext.getResources().getDrawable(bean.getIconId());
//			/// 这一步必须要做,否则不会显示.
//			drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
//			holder.mTextView.setCompoundDrawables(null,drawable,null,null);
			holder.mTextView.setText(bean.getTitle());
			holder.mImageView.setBackgroundResource(bean.getIconId());
		}
		return convertView;
	}
	
	class Holder{
		TextView mTextView;
		ImageView mImageView;
	}
	
	//判断当前数据是否为空
	public boolean isEmptyOfModelBeans(){
		return mModelBeans != null && mModelBeans.size() > 0;
	}

}
