/**
 * @Title: ChooseOrganAdapter.java
 * @Package: com.jason.fingerprint.adapter
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月2日05:12:47
 * @Version: V1.0
 */
package com.jason.fingerprint.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jason.fingerprint.R;
import com.jason.fingerprint.beans.OrganBean;

/**
 * @ClassName: ChooseOrganAdapter
 * @Description: 选择机构
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月2日05:12:47
 */
public class ChooseOrganAdapter extends BaseAdapter {
	
	private static final String TAG = "ChooseOrganAdapter";

	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private List<OrganBean> mOrganBeans;
	
	public ChooseOrganAdapter(){
		this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	public ChooseOrganAdapter(Context context){
		this.mContext = context;
		this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		initModelBeans();
	}
	
	public ChooseOrganAdapter(Context context,List<OrganBean> organBeans){
		this.mContext = context;
		this.mOrganBeans = organBeans;
		this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		initModelBeans();
	}
	
	//初始化模块数据
	private void initModelBeans(){
		if (this.mOrganBeans == null) {
			this.mOrganBeans  = new ArrayList<OrganBean>();
		}
	}

	@Override
	public int getCount() {
		return isEmptyOfOrganBeans() ? mOrganBeans.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return isEmptyOfOrganBeans() ? mOrganBeans.get(position) : null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Holder holder = null;
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.item_choose_organ_listview, null);
			holder = new Holder();
			holder.mTextView = (TextView) convertView.findViewById(R.id.item_choose_organ_name);
			convertView.setTag(holder);
		}else{
			holder = (Holder) convertView.getTag();
		}
		OrganBean bean = (OrganBean)getItem(position);
		if (bean != null) {
			Log.i(TAG, "----> getView " + bean);
			holder.mTextView.setText(bean.getMachName());
		}
		return convertView;
	}
	
	class Holder{
		TextView mTextView;
	}
	
	//判断当前数据是否为空
	public boolean isEmptyOfOrganBeans(){
		return mOrganBeans != null && mOrganBeans.size() > 0;
	}

}
