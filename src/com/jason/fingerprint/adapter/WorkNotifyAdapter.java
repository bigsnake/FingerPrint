/**
 * @Title: WorkNotifyAdapter.java
 * @Package: com.jason.fingerprint.adapter
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月14日10:18:03
 * @Version: V1.0
 */
package com.jason.fingerprint.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jason.fingerprint.R;
import com.jason.fingerprint.beans.WorkNotifyBean;

/**
 * @ClassName: WorkNotifyAdapter
 * @Description: 工作通知
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月14日10:17:54
 */
public class WorkNotifyAdapter extends BaseAdapter {

	private static final String TAG = "WorkNotifyAdapter";

	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private List<WorkNotifyBean> mWorkNotifyBeans;

	public WorkNotifyAdapter() {
		this.mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mWorkNotifyBeans = new ArrayList<WorkNotifyBean>();
	}

	public WorkNotifyAdapter(Context context) {
		this.mContext = context;
		this.mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mWorkNotifyBeans = new ArrayList<WorkNotifyBean>();
	}

	public WorkNotifyAdapter(Context context, List<WorkNotifyBean> reportBeans) {
		this.mContext = context;
		this.mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mWorkNotifyBeans = reportBeans;
	}

	@Override
	public int getCount() {
		return isEmptyOfWorkNotifyBeans() ? mWorkNotifyBeans.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return isEmptyOfWorkNotifyBeans() ? mWorkNotifyBeans.get(position)
				: null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	public void addAll(List<WorkNotifyBean> reportBeans){
		this.mWorkNotifyBeans = reportBeans;
		notifyDataSetChanged();
	}
	
	public void addOne(WorkNotifyBean bean){
		if (!isEmptyOfWorkNotifyBeans()) {
			mWorkNotifyBeans = new ArrayList<WorkNotifyBean>();
		}
		if (bean != null) {
			mWorkNotifyBeans.add(bean);
		}
		notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Holder holder = null;
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.item_notify_listview,
					null);
			holder = new Holder();
			holder.mTitleTextView = (TextView) convertView
					.findViewById(R.id.title);
			holder.mTimeTextView = (TextView) convertView
					.findViewById(R.id.time);
			holder.mCityTextView = (TextView) convertView.findViewById(R.id.city);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		WorkNotifyBean bean = (WorkNotifyBean) getItem(position);
		if (bean != null) {
			holder.mTitleTextView.setText(bean.getSTitle());
			holder.mTimeTextView.setText(bean.getPubTime());
			holder.mCityTextView.setText(bean.getPubUnit());
		}
		return convertView;
	}

	class Holder {
		TextView mTitleTextView;
		TextView mTimeTextView;
		TextView mCityTextView;
	}

	// 判断当前数据是否为空
	public boolean isEmptyOfWorkNotifyBeans() {
		return mWorkNotifyBeans != null && !mWorkNotifyBeans.isEmpty()
				&& mWorkNotifyBeans.size() > 0;
	}

}
