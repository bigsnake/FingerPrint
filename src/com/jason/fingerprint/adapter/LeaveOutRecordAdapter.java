/**
 * @Title: LeaveOutRecordAdapter.java
 * @Package: com.jason.fingerprint.adapter
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月21日10:57:50
 * @Version: V1.0
 */
package com.jason.fingerprint.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jason.fingerprint.R;
import com.jason.fingerprint.beans.LeaveOutBean;
import com.jason.fingerprint.common.DataHelper;

/**
 * @ClassName: LeaveOutRecordAdapter
 * @Description: 请销假记录 ListView 适配器
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月21日10:57:44
 */
public class LeaveOutRecordAdapter extends BaseAdapter {

	private static final String TAG = "LeaveOutRecordAdapter";

	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private List<LeaveOutBean> mLeaveOutBeans;

	public LeaveOutRecordAdapter() {
		this.mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mLeaveOutBeans = new ArrayList<LeaveOutBean>();
	}

	public LeaveOutRecordAdapter(Context context) {
		this.mContext = context;
		this.mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mLeaveOutBeans = new ArrayList<LeaveOutBean>();
	}

	public LeaveOutRecordAdapter(Context context, List<LeaveOutBean> reportBeans) {
		this.mContext = context;
		this.mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mLeaveOutBeans = reportBeans;
	}

	@Override
	public int getCount() {
		return isEmptyOfLeaveOutBeans() ? mLeaveOutBeans.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return isEmptyOfLeaveOutBeans() ? mLeaveOutBeans.get(position) : null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public void addAll(List<LeaveOutBean> reportBeans) {
		this.mLeaveOutBeans = reportBeans;
		notifyDataSetChanged();
	}

	public void addOne(LeaveOutBean bean) {
		if (!isEmptyOfLeaveOutBeans()) {
			mLeaveOutBeans = new ArrayList<LeaveOutBean>();
		}
		if (bean != null) {
			mLeaveOutBeans.add(bean);
		}
		notifyDataSetChanged();
	}

	public void updateOne(LeaveOutBean bean) {
		if (!isEmptyOfLeaveOutBeans()) {
			mLeaveOutBeans = new ArrayList<LeaveOutBean>();
		} else {
			for (LeaveOutBean LeaveOutBean : mLeaveOutBeans) {
				if (LeaveOutBean.getId().equals(bean.getId())) {
					mLeaveOutBeans.remove(LeaveOutBean);
					mLeaveOutBeans.add(bean);
				}
			}
		}
		notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Holder holder = null;
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(
					R.layout.item_leave_out_record_listview, null);
			holder = new Holder();
			holder.mNameTextView = (TextView) convertView
					.findViewById(R.id.name);
			holder.mTimeTextView = (TextView) convertView
					.findViewById(R.id.time);
			holder.mTypeTextView = (TextView) convertView
					.findViewById(R.id.type);
			holder.mPhotoTextView = (TextView) convertView
					.findViewById(R.id.photo);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		LeaveOutBean bean = (LeaveOutBean) getItem(position);
		if (bean != null) {
			holder.mNameTextView.setText(bean.getName());
			holder.mTimeTextView.setText(bean.getTime());
			holder.mTypeTextView.setText(DataHelper
					.getContentBySignType(bean.getSignType()));
			//TODO 关于图片显示，需要根据当前数据查询数据库，如果存在就会显示，如果不存在提示用户拍照
			holder.mPhotoTextView.setText("查看图片");
		}
		return convertView;
	}

	class Holder {
		TextView mNameTextView;
		TextView mTimeTextView;
		TextView mTypeTextView;
		TextView mPhotoTextView;
	}

	// 判断当前数据是否为空
	public boolean isEmptyOfLeaveOutBeans() {
		return mLeaveOutBeans != null && !mLeaveOutBeans.isEmpty()
				&& mLeaveOutBeans.size() > 0;
	}

}
