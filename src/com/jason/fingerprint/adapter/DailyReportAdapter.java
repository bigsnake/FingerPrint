/**
 * @Title: RecordAdapter.java
 * @Package: com.jason.fingerprint.adapter
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月9日23:32:04
 * @Version: V1.0
 */
package com.jason.fingerprint.adapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jason.fingerprint.R;
import com.jason.fingerprint.beans.DailyReportBean;
import com.jason.fingerprint.common.DateFormat;
import com.jason.fingerprint.utils.DataUtils;

/**
 * @ClassName: HomeAdapter
 * @Description: 日常报到 档案查询 ListView 适配器
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月9日23:32:16
 */
public class DailyReportAdapter extends BaseAdapter {

	private static final String TAG = "DailyReportAdapter";

	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private List<DailyReportBean> mDailyReportBeans;

	public DailyReportAdapter() {
		this.mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mDailyReportBeans = new ArrayList<DailyReportBean>();
	}

	public DailyReportAdapter(Context context) {
		this.mContext = context;
		this.mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mDailyReportBeans = new ArrayList<DailyReportBean>();
	}

	public DailyReportAdapter(Context context, List<DailyReportBean> reportBeans) {
		this.mContext = context;
		this.mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mDailyReportBeans = reportBeans;
	}

	@Override
	public int getCount() {
		return isEmptyOfDailyReportBeans() ? mDailyReportBeans.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return isEmptyOfDailyReportBeans() ? mDailyReportBeans.get(position)
				: null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	public void addAll(List<DailyReportBean> reportBeans){
		this.mDailyReportBeans = reportBeans;
		Collections.sort(mDailyReportBeans, new MyComparator());
		notifyDataSetChanged();
	}
	
	public void addOne(DailyReportBean bean){
		if (!isEmptyOfDailyReportBeans()) {
			mDailyReportBeans = new ArrayList<DailyReportBean>();
		}
		if (bean != null) {
			mDailyReportBeans.add(bean);
		}
		Collections.sort(mDailyReportBeans, new MyComparator());
		notifyDataSetChanged();
	}
	
	public void updateOne(DailyReportBean bean){
		if (!isEmptyOfDailyReportBeans()) {
			mDailyReportBeans = new ArrayList<DailyReportBean>();
		}else {
			for (DailyReportBean dailyReportBean : mDailyReportBeans) {
				if (dailyReportBean.getStartTime() == bean.getStartTime()) {
					mDailyReportBeans.remove(dailyReportBean);
					mDailyReportBeans.add(bean);
				}
			}
		}
		Collections.sort(mDailyReportBeans, new MyComparator());
		notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Holder holder = null;
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.item_daily_listview,
					null);
			holder = new Holder();
			holder.mNameTextView = (TextView) convertView
					.findViewById(R.id.name);
			holder.mBeginTextView = (TextView) convertView
					.findViewById(R.id.begin);
			holder.mEndTextView = (TextView) convertView.findViewById(R.id.end);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		DailyReportBean bean = (DailyReportBean) getItem(position);
		if (bean != null) {
			holder.mNameTextView.setText(bean.getRymcName());
			long beginTime = bean.getStartTime();
			long endTime = bean.getEndTime();
			holder.mBeginTextView.setText(DataUtils.converDatLongToString(beginTime*1000,
							DateFormat.DATE_YMD_HMS));
			holder.mEndTextView.setText(DataUtils.converDatLongToString(endTime*1000,
					DateFormat.DATE_YMD_HMS));
		}
		return convertView;
	}

	class Holder {
		TextView mNameTextView;
		TextView mBeginTextView;
		TextView mEndTextView;
	}

	// 判断当前数据是否为空
	public boolean isEmptyOfDailyReportBeans() {
		return mDailyReportBeans != null && !mDailyReportBeans.isEmpty()
				&& mDailyReportBeans.size() > 0;
	}
	
	private class MyComparator implements Comparator<DailyReportBean>{

		/* (non-Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		@Override
		public int compare(DailyReportBean lhs, DailyReportBean rhs) {
			// TODO Auto-generated method stub
			long time1 = lhs.getUpdateTime();
			long time2 = rhs.getUpdateTime();
			return time1 >= time2 ? -1 : 1;
		}
		
	}

}
