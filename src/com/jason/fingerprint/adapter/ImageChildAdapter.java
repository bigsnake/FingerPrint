/**
 * @Title: ImageChildAdapter.java
 * @Package: com.jason.fingerprint.adapter
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月26日 下午2:47:58
 * @Version: V1.0
 */
package com.jason.fingerprint.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.Toast;

import com.jason.fingerprint.R;
import com.jason.fingerprint.common.NativeImageLoader;
import com.jason.fingerprint.common.NativeImageLoader.NativeImageCallBack;
import com.jason.fingerprint.ui.ChoosePhotoChildActivity;
import com.jason.fingerprint.widget.ImageView;
import com.jason.fingerprint.widget.ImageView.OnMeasureListener;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;

/**
 * @ClassName: ImageChildAdapter
 * @Description: 查看一个文件夹中的所有图片适配器
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月26日 下午2:47:58
 */
public class ImageChildAdapter extends BaseAdapter {

	private Point mPoint = new Point(0, 0);// 用来封装ImageView的宽和高的对象
	private Context mContext;
	private GridView mGridView;
	private List<String> list;
	private List<String> mCacheCheckImages;
	protected LayoutInflater mInflater;
	private int mChooseCount;

	public ImageChildAdapter(Context context, List<String> list,
			GridView gridView) {
		this.mContext = context;
		this.list = list;
		this.mGridView = gridView;
		mInflater = LayoutInflater.from(context);
	}

	public ImageChildAdapter(Context context, List<String> list,
			GridView gridView, List<String> cacheCheckImages) {
		this.mContext = context;
		this.list = list;
		this.mGridView = gridView;
		this.mInflater = LayoutInflater.from(context);
		this.mCacheCheckImages = cacheCheckImages;
	}

	public ImageChildAdapter(Context context, List<String> list,
			GridView gridView, List<String> cacheCheckImages, int chooseCount) {
		this.mContext = context;
		this.list = list;
		this.mGridView = gridView;
		this.mInflater = LayoutInflater.from(context);
		this.mCacheCheckImages = cacheCheckImages;
		this.mChooseCount = chooseCount;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder viewHolder;
		String path = list.get(position);

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_image_child_gridview,
					null);
			viewHolder = new ViewHolder();
			viewHolder.mImageView = (ImageView) convertView
					.findViewById(R.id.child_image);
			viewHolder.mCheckBox = (CheckBox) convertView
					.findViewById(R.id.child_checkbox);
			viewHolder.mShowView = (android.widget.ImageView) convertView
					.findViewById(R.id.child_show);

			// 用来监听ImageView的宽和高
			viewHolder.mImageView.setOnMeasureListener(new OnMeasureListener() {

				@Override
				public void onMeasureSize(int width, int height) {
					mPoint.set(width, height);
				}
			});

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
			viewHolder.mImageView
					.setImageResource(R.drawable.friends_sends_pictures_no);
		}
		viewHolder.mCheckBox.setTag(position);
		Log.i("ImageChildAdapter", "---->path:" + path);
		viewHolder.mImageView.setTag(path);
		final String imagePath = path;
		if (isCheck(path)) {
			viewHolder.mShowView
					.setBackgroundResource(R.drawable.friends_sends_pictures_select_icon_selected);
		} else {
			viewHolder.mShowView
					.setBackgroundResource(R.drawable.friends_sends_pictures_select_icon_unselected);
		}

		viewHolder.mShowView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO 自动生成的方法存根
				if (mCacheCheckImages == null) {
					mCacheCheckImages = new ArrayList<String>();
				}
				//addAnimation(viewHolder.mShowView);
				if (mCacheCheckImages.size() > (mChooseCount - 1) ) {
					if (!isCheck(imagePath)) {
						Toast.makeText(mContext,
								"你最多只能选取" + mChooseCount + "张照片",
								Toast.LENGTH_SHORT).show();
						if (mChooseCount == 1){
							if (mContext instanceof ChoosePhotoChildActivity){
								((ChoosePhotoChildActivity) mContext).finishPhotoActivity();
							}
						}
					} else {
						mCacheCheckImages.remove(imagePath);
					}
					viewHolder.mShowView
							.setBackgroundResource(R.drawable.friends_sends_pictures_select_icon_unselected);
				} else {
					if (!isCheck(imagePath)) {
						mCacheCheckImages.add(imagePath);
						viewHolder.mShowView
								.setBackgroundResource(R.drawable.friends_sends_pictures_select_icon_selected);
						if (mChooseCount == 1){
							if (mContext instanceof ChoosePhotoChildActivity){
								((ChoosePhotoChildActivity) mContext).finishPhotoActivity();
							}
						}
					} else {
						mCacheCheckImages.remove(imagePath);
						viewHolder.mShowView
								.setBackgroundResource(R.drawable.friends_sends_pictures_select_icon_unselected);
					}
				}
				((ChoosePhotoChildActivity) mContext)
						.updateViewData(mCacheCheckImages);
			}
		});

		// 利用NativeImageLoader类加载本地图片
		Bitmap bitmap = NativeImageLoader.getInstance().loadNativeImage(path,
				mPoint, new NativeImageCallBack() {

					@Override
					public void onImageLoader(Bitmap bitmap, String path) {
						ImageView mImageView = (ImageView) mGridView
								.findViewWithTag(path);
						if (bitmap != null && mImageView != null) {
							mImageView.setImageBitmap(bitmap);
						}
					}
				});

		if (bitmap != null) {
			viewHolder.mImageView.setImageBitmap(bitmap);
		} else {
			viewHolder.mImageView
					.setImageResource(R.drawable.friends_sends_pictures_no);
		}

		return convertView;
	}

	/**
	 * 给CheckBox加点击动画，利用开源库nineoldandroids设置动画
	 * 
	 * @param view
	 */
	private void addAnimation(View view) {
		float[] vaules = new float[] { 0.5f, 0.6f, 0.7f, 0.8f, 0.9f, 1.0f,
				1.1f, 1.2f, 1.3f, 1.25f, 1.2f, 1.15f, 1.1f, 1.0f };
		AnimatorSet set = new AnimatorSet();
		set.playTogether(ObjectAnimator.ofFloat(view, "scaleX", vaules),
				ObjectAnimator.ofFloat(view, "scaleY", vaules));
		set.setDuration(150);
		set.start();
	}

	private boolean isCheck(String path) {
		if (mCacheCheckImages == null || mCacheCheckImages.isEmpty()
				|| mCacheCheckImages.size() < 1) {
			return false;
		}
		for (int i = 0; i < mCacheCheckImages.size(); i++) {
			if (mCacheCheckImages.get(i).equals(path)) {
				return true;
			}
		}
		return false;
	}

	public static class ViewHolder {
		public ImageView mImageView;
		public android.widget.ImageView mShowView;
		public CheckBox mCheckBox;
	}

}
