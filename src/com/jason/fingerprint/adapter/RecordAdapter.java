/**
 * @Title: RecordAdapter.java
 * @Package: com.jason.fingerprint.adapter
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月19日23:43:26
 * @Version: V1.0
 */
package com.jason.fingerprint.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.kymjs.aframe.utils.StringUtils;

import com.jason.fingerprint.R;
import com.jason.fingerprint.beans.RecordBean;

/**
 * @ClassName: HomeAdapter
 * @Description: RecordActitivty 档案查询 ListView 适配器
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月19日23:43:40
 */
public class RecordAdapter extends BaseAdapter {
	
	private static final String TAG = "HomeModelAdapter";

	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private List<RecordBean> recordBeans;
	
	public RecordAdapter(){
		this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.recordBeans = new ArrayList<RecordBean>();
	}
	
	public RecordAdapter(Context context){
		this.mContext = context;
		this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.recordBeans = new ArrayList<RecordBean>();
	}
	
	public RecordAdapter(Context context,List<RecordBean> recordBeans){
		this.mContext = context;
		this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.recordBeans = recordBeans;
	}

	@Override
	public int getCount() {
		return isEmptyOfRecordBeans() ? recordBeans.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return isEmptyOfRecordBeans() ? recordBeans.get(position) : null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Holder holder = null;
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.item_record_listview, null);
			holder = new Holder();
			holder.mNameTextView = (TextView) convertView.findViewById(R.id.name);
			holder.mPhoneTextView = (TextView) convertView.findViewById(R.id.phone);
			holder.mIdTextView = (TextView) convertView.findViewById(R.id.id);
			holder.mSignTextView = (TextView) convertView.findViewById(R.id.sign);
			convertView.setTag(holder);
		}else{
			holder = (Holder) convertView.getTag();
		}
		RecordBean bean = (RecordBean)getItem(position);
		if (bean != null) {
			holder.mNameTextView.setText(bean.getName());
			holder.mIdTextView.setText(bean.getCardNO());
			holder.mPhoneTextView.setText(bean.getTelephone());
			if (bean.getIsEntry() == 0) {
				holder.mSignTextView.setText("未录");
				holder.mSignTextView.setTextColor(mContext.getResources().getColor(R.color.red));
			}else {
				holder.mSignTextView.setText("已录");
				holder.mSignTextView.setTextColor(mContext.getResources().getColor(R.color.black));
			}
		}
		return convertView;
	}
	
	class Holder{
		TextView mNameTextView;
		TextView mPhoneTextView;
		TextView mIdTextView;
		TextView mSignTextView;
	}
	
	//判断当前数据是否为空
	public boolean isEmptyOfRecordBeans(){
		return recordBeans != null && !recordBeans.isEmpty() && recordBeans.size() > 0;
	}

}
