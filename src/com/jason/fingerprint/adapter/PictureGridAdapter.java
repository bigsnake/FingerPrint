/**
 * @Title: HomeAdapter.java
 * @Package: com.jason.fingerprint.adapter
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月19日 上午9:21:22
 * @Version: V1.0
 */
package com.jason.fingerprint.adapter;

import java.util.ArrayList;
import java.util.List;

import org.kymjs.aframe.bitmap.KJBitmap;
import org.kymjs.aframe.bitmap.KJBitmapConfig;
import org.kymjs.aframe.utils.StringUtils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.jason.fingerprint.R;
import com.jason.fingerprint.common.FileHelper;

/**
 * @ClassName: PictureGridAdapter
 * @Description: 图片库 GridView 适配器，并且限制个数为5个
 * @Author: John
 * @Date: 2014年11月1日
 */
public class PictureGridAdapter extends BaseAdapter {

	private static final String TAG = "PictureGridAdapter";

	private Context mContext;

	private LayoutInflater mLayoutInflater;

	private List<String> mBitmapArray = new ArrayList<String>();
	
	private KJBitmap mKjBitmap;

	public PictureGridAdapter(Context context) {
		this.mContext = context;
		this.mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		KJBitmapConfig config = new KJBitmapConfig();
		config.openMemoryCache = false;
		config.cachePath = new FileHelper(mContext).getCachesDir().getPath();
		this.mKjBitmap = KJBitmap.create(config);
	}

	public PictureGridAdapter(Context context, List<String> array) {
		this.mContext = context;
		this.mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mBitmapArray.clear();
		mBitmapArray.addAll(array);
	}

	public void setAdapter(List<String> array) {
		mBitmapArray.clear();
		mBitmapArray.addAll(array);
		notifyDataSetChanged();
	}

	public boolean addBitmap(String b) {
		if (mBitmapArray.size() < 5) {
			mBitmapArray.add(b);
			notifyDataSetChanged();
			return true;
		}
		return false;
	}

	public void clear() {
        mBitmapArray.clear();
	}

	public List<String> getArray() {
		return mBitmapArray;
	}

	@Override
	public int getCount() {
		return mBitmapArray.size();
	}

	@Override
	public Object getItem(int position) {
		return mBitmapArray.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		Holder holder = null;
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(
					R.layout.item_picture_gridview, null);
			holder = new Holder();
			holder.picture = (ImageView) convertView
					.findViewById(R.id.item_picture_imageview);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		String b = (String) getItem(position);
		if (!StringUtils.isEmpty(b)) {
			mKjBitmap.display(holder.picture, b, 70, 90);
			//holder.picture.setImageBitmap(b);
		}
		return convertView;
	}

	class Holder {
		ImageView picture;
	}
}
