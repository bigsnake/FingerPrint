package com.jason.fingerprint.adapter;

import java.util.List;

import org.kymjs.aframe.bitmap.KJBitmap;
import org.kymjs.aframe.bitmap.KJBitmapConfig;
import org.kymjs.aframe.utils.StringUtils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jason.fingerprint.R;
import com.jason.fingerprint.beans.SignInRecordBean;
import com.jason.fingerprint.common.FileHelper;
import com.jason.fingerprint.configs.DataConfig;

/**
 * @ClassName: SignInRecordAdapter
 * @Description: 各种签到记录查看适配器
 * @Author: John
 * @Date: 2014/10/21
 */
public class SignInRecordAdapter extends BaseAdapter {

	private static final String TAG = "SignInRecordAdapter";

	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private List<SignInRecordBean> mList;
	private String[] mFlags;
	private KJBitmap mKjBitmap;
    private OnClickListener mTakePictureListener;

	public SignInRecordAdapter(Context context) {
		this.mContext = context;
		this.mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		mFlags = new String[] { context.getString(R.string.begin_signin),
				context.getString(R.string.call_signin),
				context.getString(R.string.end_signin) };

		KJBitmapConfig config = new KJBitmapConfig();
		config.openMemoryCache = false;
		config.cachePath = new FileHelper(mContext).getCachesDir().getPath();
		mKjBitmap = KJBitmap.create(config);
	}

	public SignInRecordAdapter(Context context, List<SignInRecordBean> list) {
		this(context);
		this.mList = list;
	}

	public void setTakePictureListener(OnClickListener listener) {
		mTakePictureListener = listener;
	}

	public void setSignInRecordList(List<SignInRecordBean> list) {
		this.mList = list;
	}

	@Override
	public int getCount() {
		return isEmptyOfList() ? mList.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return isEmptyOfList() ? mList.get(position) : null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		Holder holder = null;
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(
					R.layout.item_sign_record_listview, null);
			holder = new Holder();
			holder.mSignName = (TextView) convertView
					.findViewById(R.id.sign_name);
			holder.mSignTime = (TextView) convertView
					.findViewById(R.id.sign_time);
			holder.mSignFlag = (TextView) convertView
					.findViewById(R.id.sign_flag);
			holder.mSignNote = (ImageView) convertView
					.findViewById(R.id.sign_note);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		SignInRecordBean bean = (SignInRecordBean) getItem(position);
		if (bean != null) {
			holder.mSignName.setText(bean.getName());
			holder.mSignTime.setText(bean.getTime());

			String flag = bean.getFlag();
			if (flag.equals(DataConfig.SIGN_FLAG_BEGIN)) {
				holder.mSignFlag.setText(mFlags[0]);
			} else if (flag.equals(DataConfig.SIGN_FLAG_CALL)) {
				holder.mSignFlag.setText(mFlags[1]);
			} else if (flag.equals(DataConfig.SIGN_FLAG_END)) {
				holder.mSignFlag.setText(mFlags[2]);
			} else {
				holder.mSignFlag.setText("");
			}

			String imagePath = bean.getFilePath();
			if (!StringUtils.isEmpty(imagePath)) {
				mKjBitmap.display(holder.mSignNote, imagePath, 70, 90);
			}else {
				holder.mSignNote.setImageResource(R.drawable.camera);
			}

			String type = bean.getType();
			if (type.equals(DataConfig.REGISTER_TYPE_0)) {
				// holder.mSignType.setText(mTypes[0]);
				holder.mSignNote.setVisibility(View.GONE);
			} else if (type.equals(DataConfig.REGISTER_TYPE_1)) {
				// holder.mSignType.setText(mTypes[1]);
				holder.mSignNote.setVisibility(View.GONE);
			} else if (type.equals(DataConfig.REGISTER_TYPE_2)) {
				// holder.mSignType.setText(mTypes[2]);
				if (flag.equals(DataConfig.SIGN_FLAG_BEGIN)) {
					holder.mSignNote.setVisibility(View.VISIBLE);
				} else {
					holder.mSignNote.setVisibility(View.INVISIBLE);
				}
			} else if (type.equals(DataConfig.REGISTER_TYPE_3)) {
				// holder.mSignType.setText(mTypes[3]);
				if (flag.equals(DataConfig.SIGN_FLAG_BEGIN)) {
					holder.mSignNote.setVisibility(View.VISIBLE);
				} else {
					holder.mSignNote.setVisibility(View.INVISIBLE);
				}
			} else {
				// holder.mSignFlag.setText("");
				holder.mSignNote.setVisibility(View.GONE);
			}

			if( mTakePictureListener != null) {
				holder.mSignNote.setTag(position);
				holder.mSignNote.setOnClickListener(mTakePictureListener);
			}
		}
		return convertView;
	}

	class Holder {
		TextView mSignName;
		TextView mSignTime;
		TextView mSignType;
		TextView mSignFlag;
		ImageView mSignNote;
	}

	// 判断当前数据是否为空
	public boolean isEmptyOfList() {
		return mList != null && mList.size() > 0;
	}

}
