package com.jason.fingerprint.widget;

import java.lang.reflect.Field;

import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.ListView;

import com.jason.fingerprint.R;

public class ExpandListView extends ListView {

	private int exactlyHeight = 100; // 缺省高度

	public ExpandListView(Context context, AttributeSet attrs) {
		super(context, attrs);

		WindowManager manager = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		DisplayMetrics metric = new DisplayMetrics();
		manager.getDefaultDisplay().getRealMetrics(metric);

		int titleBarHeight = context.getResources().getDimensionPixelSize(
				R.dimen.titlebar_height);
		int signDetailHeight = context.getResources().getDimensionPixelSize(
				R.dimen.signin_detail_height);

		int statusbarHeight = getStatusHeight();
		if (statusbarHeight == 0) {
			statusbarHeight = (int) (metric.density * 30);
		}

		exactlyHeight = metric.heightPixels - titleBarHeight - signDetailHeight
				- statusbarHeight;
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int expandSpec = MeasureSpec.makeMeasureSpec(exactlyHeight,
				MeasureSpec.EXACTLY);
		super.onMeasure(widthMeasureSpec, expandSpec);

	}

	private int getStatusHeight() {
		int statusBar = 0;
		try {
			Class<?> c = Class.forName("com.android.internal.R$dimen");
			Object obj = c.newInstance();
			Field field = c.getField("status_bar_height");
			int x = Integer.parseInt(field.get(obj).toString());
			statusBar = getResources().getDimensionPixelSize(x);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return statusBar;
	}
}
