/**
 * @Title: ProgressDialog.java
 * @Package: com.jason.fingerprint.widget
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年12月13日 下午5:36:45
 * @Version: V1.0
 */
package com.jason.fingerprint.widget;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.jason.fingerprint.R;

/**
 * @ClassName: ProgressDialog
 * @Description: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年12月13日 下午5:36:45
 */
public class ProgressDialog extends Dialog {

	
    private Context mContext;
    private TextView mMessageTextView;
    
    public ProgressDialog(Context context) {
        super(context);
        this.mContext = context;
        init();
    }

    public ProgressDialog(Context context, int theme) {
        super(context, theme);
        this.mContext = context;
    }
    
    private void init(){
    	LayoutInflater inflater = LayoutInflater.from(mContext);
    	View view = inflater.inflate(R.layout.view_dialog_loading, null);
    	mMessageTextView = (TextView) view.findViewById(R.id.message);
    	setContentView(view);
    }
    
    public void setMessage(String message){
    	if (isShowing()) {
			dismiss();
			mMessageTextView.setText(message);
		}
    }

}
