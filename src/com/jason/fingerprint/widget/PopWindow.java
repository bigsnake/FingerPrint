/**
 * @Title: PopWindow.java
 * @Package: com.jason.fingerprint.widget
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月22日 下午10:24:22
 * @Version: V1.0
 */
package com.jason.fingerprint.widget;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.widget.PopupWindow;

/**
 * @ClassName: PopWindow
 * @Description: 自定义PopWindow
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月22日 下午10:24:22
 */
public class PopWindow extends PopupWindow {
	
	private int mWidth;
	private int mHeight;
	
	
	public static PopWindow getInstance(final Context context){
		return new PopWindow(context);
	}
	
	public PopWindow(final Context context){
		this.setOutsideTouchable(true);
		this.setFocusable(true);
		this.setTouchable(true);
		this.setBackgroundDrawable(new BitmapDrawable());
	}
	
	public void setView(View view,int w,int h){
		this.setContentView(view);
		this.setWidth(w);
		this.setHeight(h);
	}
	
	public void show(View view,int x,int y,int gravity){
		if (!this.isShowing()) {
			this.showAtLocation(view, gravity, x, y);
		}else {
			this.dismiss();
		}
	}

}
