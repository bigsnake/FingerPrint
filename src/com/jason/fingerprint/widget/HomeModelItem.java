/**
 * @Title: HomeModelItem.java
 * @Package: com.jason.fingerprint.widget
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年12月1日 下午11:58:39
 * @Version: V1.0
 */
package com.jason.fingerprint.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * @ClassName: HomeModelItem
 * @Description: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年12月1日 下午11:58:39
 */
public class HomeModelItem extends RelativeLayout {

	/**
	 * @Descripton: TODO
	 * @Param @param context
	 * @Return $
	 * @Throws 
	 */
	public HomeModelItem(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @Descripton: TODO
	 * @Param @param context
	 * @Param @param attrs
	 * @Return $
	 * @Throws 
	 */
	public HomeModelItem(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @Descripton: TODO
	 * @Param @param context
	 * @Param @param attrs
	 * @Param @param defStyle
	 * @Return $
	 * @Throws 
	 */
	public HomeModelItem(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}
	
	/* (non-Javadoc)
	 * @see android.widget.RelativeLayout#onMeasure(int, int)
	 */
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// TODO Auto-generated method stub
		setMeasuredDimension(getDefaultSize(0, widthMeasureSpec), getDefaultSize(0, heightMeasureSpec));
		 
        // Children are just made to fill our space.
        int childWidthSize = getMeasuredWidth();
        int childHeightSize = getMeasuredHeight();
        //高度和宽度一样
        heightMeasureSpec = widthMeasureSpec = MeasureSpec.makeMeasureSpec(childWidthSize, MeasureSpec.EXACTLY);

		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		
	}
	
	

}
