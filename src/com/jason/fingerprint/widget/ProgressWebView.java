/**
 * @Title: ProgressWebView.java
 * @Package: com.jason.fingerprint.widget
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月14日 下午4:20:08
 * @Version: V1.0
 */
package com.jason.fingerprint.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;
import android.widget.ProgressBar;

/**
 * @ClassName: ProgressWebView
 * @Description: 自定义webview,添加进度条
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月14日 下午4:20:08
 */
public class ProgressWebView extends WebView {
	
	private ProgressBar mProgressbar;

	/**
	 * @Descripton: TODO
	 * @Param @param context
	 * @Return $
	 * @Throws 
	 */
	public ProgressWebView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	@SuppressWarnings("deprecation")
	public ProgressWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mProgressbar = new ProgressBar(context, null, android.R.attr.progressBarStyleHorizontal);
        mProgressbar.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 3, 0, 0));
        addView(mProgressbar);
        //        setWebViewClient(new WebViewClient(){});
        setWebChromeClient(new WebChromeClient());
    }
	
	public class WebChromeClient extends android.webkit.WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            if (newProgress == 100) {
            	mProgressbar.setVisibility(GONE);
            } else {
                if (mProgressbar.getVisibility() == GONE)
                	mProgressbar.setVisibility(VISIBLE);
                mProgressbar.setProgress(newProgress);
            }
            super.onProgressChanged(view, newProgress);
        }

    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        LayoutParams lp = (LayoutParams) mProgressbar.getLayoutParams();
        lp.x = l;
        lp.y = t;
        mProgressbar.setLayoutParams(lp);
        super.onScrollChanged(l, t, oldl, oldt);
    }

}
