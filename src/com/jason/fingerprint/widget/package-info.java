/**
 * @Title: package-info.java
 * @Package: com.jason.fingerprint.widget
 * @Descripton: 自定义控件
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月19日 下午5:16:18
 * @Version: V1.0
 */
package com.jason.fingerprint.widget;