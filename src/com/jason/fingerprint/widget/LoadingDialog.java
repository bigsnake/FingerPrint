package com.jason.fingerprint.widget;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.jason.fingerprint.R;
import com.jason.fingerprint.logic.Logic;
import com.jason.fingerprint.logic.MatchFingerprintSyncLogic;

public class LoadingDialog extends Dialog {

	private TextView mTextView;
	private Logic mLogic;
	private MatchFingerprintSyncLogic mSyncLogic;

	public LoadingDialog(Context context) {
		super(context, R.style.LoadingDialog);

		setContentView(R.layout.ui_dialog_loading);
		mTextView = (TextView) findViewById(android.R.id.message);
		//setCancelable(false);
		setCanceledOnTouchOutside(false);
	}

	@Override
	public void show() {
		super.show();
	}

	@Override
	public void dismiss() {
		// TODO Auto-generated method stub
		super.dismiss();
		// if(mAnimationDrawable!=null)
		// mAnimationDrawable.stop();
	}

	public void setMessage(String s) {
		if (isShowing()) {
			dismiss();
		}
		if (mTextView != null) {
			mTextView.setText(s);
			mTextView.setVisibility(View.VISIBLE);
		}
	}

	public void setMessage(int res) {
		if (mTextView != null) {
			mTextView.setText(res);
			mTextView.setVisibility(View.VISIBLE);
		}
	}
	
	public void setLogic(Logic logic){
		this.mLogic = logic;
	}
	
	public void setMatchLogic(MatchFingerprintSyncLogic logic){
		this.mSyncLogic = logic;
	}
	
	/* (non-Javadoc)
	 * @see android.app.Dialog#onBackPressed()
	 */
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		Log.i("Loading", "---002-->onBackPressed");
		if (mLogic != null) {
			mLogic.destory();
		}
		if (mSyncLogic != null) {
			mSyncLogic.destory();
		}
		dismiss();
	}

}
