/**
 * @Title: UncaughtException.java
 * @Package: com.jason.fingerprint
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年12月17日 下午10:18:47
 * @Version: V1.0
 */
package com.jason.fingerprint;

import java.lang.Thread.UncaughtExceptionHandler;

import android.util.Log;

/**
 * @ClassName: UncaughtException
 * @Description: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年12月17日 下午10:18:47
 */
public class UncaughtException implements UncaughtExceptionHandler {

	private static final String TAG = "UncaughtException";	
	/** 异常句柄*/
	@SuppressWarnings("unused")
	private Thread.UncaughtExceptionHandler mUeHandler;
	
	public UncaughtException(){
		Thread.UncaughtExceptionHandler handler = Thread.getDefaultUncaughtExceptionHandler();
		this.mUeHandler = handler;
	}
	
	@Override
	public void uncaughtException(Thread thread, Throwable throwable) {
		Log.e(TAG, "", throwable);
		onFC();
	}
	
	private void onFC(){
		AppContext bootApp = AppContext.getInstance();
		if (bootApp != null){
			Log.e(TAG, "UncaughtException-->onFC");
			bootApp.onFC();
		} else{
			Log.e(TAG, "UncaughtException-->killProcess");
			int pid = android.os.Process.myPid();
	        android.os.Process.killProcess(pid);
		}
	}

}
