/**
 * @Title: AppGlobal.java
 * @Package: com.jason.fingerprint
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月21日 下午3:24:17
 * @Version: V1.0
 */
package com.jason.fingerprint;

/**
 * @ClassName: AppGlobal
 * @Description: 全局参数
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月21日 下午3:24:17
 */
public class AppGlobal {
	
	public static final String HTTP_PARAM_IMEI = "imie";
	public static final String HTTP_PARAM_ORGANID = "organId";
	public static final String HTTP_PARAM_CURRENTINDEX = "currentIndex";
	public static final String HTTP_PARAM_TYPE = "type";
	
}
