/**
 * @Title: MyLocationListener.java
 * @Package: com.jason.fingerprint.listener
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月4日 下午9:37:03
 * @Version: V1.0
 */
package com.jason.fingerprint.listener;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.jason.fingerprint.beans.ui.LocationBean;

/**
 * @ClassName: MyLocationListener
 * @Description: 百度位置监听
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月4日 下午9:37:03
 */
public class MyLocationListener implements BDLocationListener {
	
	private static final String TAG = "MyLocationListener";
	public static final String LOCATION_KEY = "location_key";
	public static final String LOCATION_INTENT = "location_intent";
	
	private Context mContext;
	
	public MyLocationListener() {
		super();
	}
	
	public MyLocationListener(Context context){
		super();
		this.mContext = context;
	}

	@Override
	public void onReceiveLocation(BDLocation location) {
		LocationBean bean = new LocationBean();
		bean.setLatitude(String.valueOf(location.getLatitude()));
		bean.setLontitude(String.valueOf(location.getLongitude()));
		if (location.getLocType() == BDLocation.TypeGpsLocation) {
			bean.setAddress(location.getAddrStr());
		} else if (location.getLocType() == BDLocation.TypeNetWorkLocation){
			bean.setAddress(location.getAddrStr());
		}
		Log.i(TAG, "---->" + bean.toString());
		Intent intent = new Intent(LOCATION_INTENT);
		intent.putExtra(LOCATION_KEY, bean);
		mContext.sendBroadcast(intent);
	}

}
