/**
 * @Title: MyDbUpdateListener.java
 * @Package: com.jason.fingerprint.listener
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月4日 下午10:15:32
 * @Version: V1.0
 */
package com.jason.fingerprint.listener;

import org.kymjs.aframe.database.KJDB.DbUpdateListener;

import android.database.sqlite.SQLiteDatabase;

/**
 * @ClassName: MyDbUpdateListener
 * @Description: 数据库升级监听
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月4日 下午10:15:32
 */
public class MyDbUpdateListener implements DbUpdateListener {

	/* (non-Javadoc)
	 * @see org.kymjs.aframe.database.KJDB.DbUpdateListener#onUpgrade(android.database.sqlite.SQLiteDatabase, int, int)
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO 

	}

}
