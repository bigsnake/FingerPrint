/**
 * @Title: InitDataSyncLogic.java
 * @Package: com.jason.fingerprint.logic
 * @Descripton: 初始化应用信息
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月27日15:40:30
 * @Version: V1.0
 */
package com.jason.fingerprint.logic;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;

import com.jason.fingerprint.AppContext;

/**
 * @ClassName: InitDataSyncLogic
 * @Description: 初始化应用信息
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月21日 上午9:58:26
 */
public class InitDataSyncLogic extends Logic {

	private static final String TAG = "InitDataSyncLogic.java";
	private InitDataAsyncTask mInitDataAsyncTask;
	private Handler mHandler;
	private boolean mIsCallBack;// 是否有回调，此需要Handle
	private Message mMessage;
	
	public InitDataSyncLogic(){
		super();
		this.mInitDataAsyncTask = new InitDataAsyncTask();
	}
	
	public InitDataSyncLogic(AppContext appContext,Handler handler,boolean isCallBack){
		super(appContext);
		this.mInitDataAsyncTask = new InitDataAsyncTask();
		this.mIsCallBack = isCallBack;
		this.mHandler = handler;
		this.mMessage = new Message();
	}

	/*
	 * @see com.jason.fingerprint.logic.Logic#execute()
	 */
	@Override
	public void execute() {
		mInitDataAsyncTask.execute();
	}

	/*
	 * @see com.jason.fingerprint.logic.Logic#send()
	 */
	@Override
	public void send() {
		if (mIsCallBack) {
			mHandler.sendMessage(mMessage);
			mMessage = new Message();
		}
	}
	
	
	private class InitDataAsyncTask extends AsyncTask<Void, String, String>{

		/*
		 * @see android.os.AsyncTask#doInBackground(Params[])
		 */
		@Override
		protected String doInBackground(Void... params) {
			// 运行在后台线程中
			
			return null;
		}
		
		/*
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */
		@Override
		protected void onPostExecute(String result) {
			// TODO 自动生成的方法存根
			super.onPostExecute(result);
		}
		
	}


	/* (non-Javadoc)
	 * @see com.jason.fingerprint.logic.Logic#destory()
	 */
	@Override
	public void destory() {
		// TODO Auto-generated method stub
		
	}
}
