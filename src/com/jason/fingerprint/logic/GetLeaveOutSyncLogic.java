/**
 * @Title: GetLeaveOutSyncLogic.java
 * @Package: com.jason.fingerprint.logic
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月18日 下午4:35:19
 * @Version: V1.0
 */
package com.jason.fingerprint.logic;

import java.util.ArrayList;
import java.util.List;

import org.kymjs.aframe.http.HttpCallBack;
import org.kymjs.aframe.http.KJFileParams;
import org.kymjs.aframe.http.KJHttp;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.google.gson.GsonBuilder;
import com.jason.fingerprint.AppContext;
import com.jason.fingerprint.AppGlobal;
import com.jason.fingerprint.beans.LeaveOutBean;
import com.jason.fingerprint.beans.LeaveOutResp;
import com.jason.fingerprint.configs.HttpConfig;
import com.jason.fingerprint.utils.CodecUtils;

/**
 * @ClassName: GetLeaveOutSyncLogic
 * @Description: 查询请假列表
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月18日 下午4:35:19
 */
public class GetLeaveOutSyncLogic extends Logic {
	
	private static final String TAG = "GetLeaveOutSyncLogic";
	private Handler mHandler;
	private boolean mIsCallBack;// 是否有回调，此需要Handle
	private Message mMessage;
	private int mCurrentIndex = 1;
	private List<LeaveOutBean> mLeaveOutBeans = new ArrayList<LeaveOutBean>();
	
	public GetLeaveOutSyncLogic(){
		super();
	}
	
	public GetLeaveOutSyncLogic(AppContext appContext,Handler handler,boolean isCallBack){
		super(appContext);
		this.mIsCallBack = isCallBack;
		this.mHandler = handler;
		this.mMessage = new Message();
	}

	/* (non-Javadoc)
	 * @see com.jason.fingerprint.logic.Logic#execute()
	 */
	@Override
	public void execute() {
		// TODO Auto-generated method stub
		httpPost();
	}

	/* (non-Javadoc)
	 * @see com.jason.fingerprint.logic.Logic#send()
	 */
	@Override
	public void send() {
		// TODO Auto-generated method stub
		if (mIsCallBack) {
			mHandler.sendMessage(mMessage);
			mMessage = new Message();
		}
	}
	
	private void httpPost(){
		mParams = new KJFileParams();
		mParams.put(AppGlobal.HTTP_PARAM_IMEI, mImei);
		mParams.put(AppGlobal.HTTP_PARAM_CURRENTINDEX,
				String.valueOf(mCurrentIndex));
		mParams.put(AppGlobal.HTTP_PARAM_ORGANID, mMachId);
		String url = HttpConfig.POST_URL_EGRESSLEAVE_GETLEAVELIST;
		Log.i(TAG, "----->API:" + url);
		KJHttp kjHttp = new KJHttp();
		kjHttp.post(url, mParams, new HttpCallBack() {

			@Override
			public void onLoading(long count, long current) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onSuccess(Object arg0) {
				// TODO Auto-generated method stub
				String result = CodecUtils.base64Decode(arg0.toString());
				Log.i(TAG, "----->Result:" + result);
				LeaveOutResp leaveOutResp = new GsonBuilder().create().fromJson(
						result, LeaveOutResp.class);
				// 存入数据库
				if (leaveOutResp != null && !leaveOutResp.isEmpty()) {
					Log.i(TAG, "----->leaveOutResp:" + leaveOutResp.toString());
					List<LeaveOutBean> beans = leaveOutResp.getValues();
					mLeaveOutBeans.addAll(beans);
					try {
						
						if (!leaveOutResp.isLastPage()) {
							mCurrentIndex++;
							httpPost();
						} else {
							mMessage.what = 0;
							mMessage.obj = mLeaveOutBeans;
							send();
						}
					} catch (Exception e) {
						mMessage.what = 1;
						send();
					}
				} else {
					mMessage.what = 2;
					send();
				}
			}

			@Override
			public void onFailure(Throwable t, int errorNo, String strMsg) {
				// TODO Auto-generated method stub
				mMessage.what = 2;
				send();
			}
			
		});
	}

	/* (non-Javadoc)
	 * @see com.jason.fingerprint.logic.Logic#destory()
	 */
	@Override
	public void destory() {
		// TODO Auto-generated method stub
		
	}

}
