/**
 * @Title: AppUpdateSyncLogic.java
 * @Package: com.jason.fingerprint.logic
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2015年1月18日 下午8:22:23
 * @Version: V1.0
 */
package com.jason.fingerprint.logic;

import org.apache.http.client.methods.HttpPost;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

/**
 * @ClassName: AppUpdateSyncLogic
 * @Description: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2015年1月18日 下午8:22:23
 */
public class AppUpdateSyncLogic extends Logic {
	
	private Handler mHandler;
	private Message mMessage;
	
	public AppUpdateSyncLogic(Handler handler){
		this.mHandler = handler;
		this.mMessage = new Message();
	}
	

	@Override
	public void execute() {
		
	}
	
	private void HttpPost(){
		
	}

	@Override
	public void send() {
		mHandler.sendMessage(mMessage);
		mMessage = new Message();
	}

	@Override
	public void destory() {

	}

}
