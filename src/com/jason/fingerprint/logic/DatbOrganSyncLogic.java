/**
 * @Title: DatbRecordSyncLogic.java
 * @Package: com.jason.fingerprint.logic
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月21日 上午9:54:27
 * @Version: V1.0
 */
package com.jason.fingerprint.logic;

import java.util.List;

import org.kymjs.aframe.http.HttpCallBack;
import org.kymjs.aframe.http.KJFileParams;
import org.kymjs.aframe.http.KJHttp;

import com.google.gson.GsonBuilder;
import com.jason.fingerprint.AppContext;
import com.jason.fingerprint.AppGlobal;
import com.jason.fingerprint.beans.OrganBean;
import com.jason.fingerprint.beans.OrganResp;
import com.jason.fingerprint.configs.HttpConfig;
import com.jason.fingerprint.utils.CodecUtils;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

/**
 * @ClassName: DatbRecordSyncLogic
 * @Description: 加载登录帐号下可选择的隶属机构
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月21日 上午9:54:27
 */
public class DatbOrganSyncLogic extends Logic {

	private static final String TAG = "DatbOrganSyncLogic";

	private Handler mHandler;
	private boolean mIsCallBack = false;// 是否有回调，此需要Handle,默认不回调
	private int mCurrentIndex = 1;// 当前页
	private Message mMessage;

	public DatbOrganSyncLogic() {

	}

	public DatbOrganSyncLogic(AppContext appContext, Handler handler) {
		super(appContext);
		this.mHandler = handler;
		this.mMessage = new Message();
	}

	public DatbOrganSyncLogic(AppContext appContext, Handler handler,
			boolean isCallBack) {
		super(appContext);
		this.mHandler = handler;
		this.mMessage = new Message();
		this.mIsCallBack = isCallBack;
	}

	@Override
	public void execute() {
		try {
			mKjdb.deleteTable(OrganBean.class);
			
		} catch (Exception e) {
		}
		datbRecordSync();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jason.fingerprint.logic.Logic#send()
	 */
	@Override
	public void send() {
		if (mIsCallBack) {
			mHandler.sendMessage(mMessage);
			mMessage = new Message();
		}

	}

	// 档案同步
	private void datbRecordSync() {
		mParams = new KJFileParams();
		mParams.put(AppGlobal.HTTP_PARAM_IMEI, mImei);
		mParams.put(AppGlobal.HTTP_PARAM_CURRENTINDEX,
				String.valueOf(mCurrentIndex));
		mParams.put(AppGlobal.HTTP_PARAM_ORGANID, mMachId);
		String url = HttpConfig.POST_URL_DATB_ORGANSYNC;
		Log.i(TAG, "----->API:" + url);
		KJHttp kjHttp = new KJHttp();
		kjHttp.post(url, mParams, new HttpCallBack() {

			@Override
			public void onSuccess(Object arg0) {
				String result = CodecUtils.base64Decode(arg0.toString());
				Log.i(TAG, "----->Result:" + result);
				OrganResp organResp = new GsonBuilder().create().fromJson(
						result, OrganResp.class);
				// 存入数据库
				if (organResp != null && !organResp.isEmpty()) {
					Log.i(TAG, "----->organResp:" + organResp.toString());
					List<OrganBean> beans = organResp.getValues();
					try {
						for (OrganBean organBean : beans) {
							if (organBean != null) {
								//先判断此信息是否在数据库中存在，如果存在，不存在直接添加
								mKjdb.save(organBean);
							}
						}
						if (!organResp.isLastPage()) {
							mCurrentIndex++;
							datbRecordSync();
						} else {
							mMessage.arg1 = 0;
							send();
						}
					} catch (Exception e) {
						mMessage.arg1 = 1;
						send();
					}
				} else {
					mMessage.arg1 = 2;
					send();
				}
			}

			@Override
			public void onLoading(long arg0, long arg1) {
				Log.i(TAG, "----->Result:arg0:" + arg0 + ",arg1:" + arg1);
			}

			@Override
			public void onFailure(Throwable arg0, int arg1, String arg2) {
				Log.i(TAG, "----->Result:arg1:" + arg1 + ",arg2:" + arg2);
				mMessage.arg1 = 3;
				send();
			}
		});
	}

	/* (non-Javadoc)
	 * @see com.jason.fingerprint.logic.Logic#destory()
	 */
	@Override
	public void destory() {
		// TODO Auto-generated method stub
		
	}

}
