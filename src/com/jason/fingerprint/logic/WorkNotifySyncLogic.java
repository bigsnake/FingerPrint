/**
 * @Title: WorkNotifySyncLogic.java
 * @Package: com.jason.fingerprint.logic
 * @Descripton: 初始化应用信息
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月14日01:24:52
 * @Version: V1.0
 */
package com.jason.fingerprint.logic;

import java.util.List;

import org.kymjs.aframe.http.HttpCallBack;
import org.kymjs.aframe.http.KJFileParams;
import org.kymjs.aframe.http.KJHttp;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.google.gson.GsonBuilder;
import com.jason.fingerprint.AppContext;
import com.jason.fingerprint.AppGlobal;
import com.jason.fingerprint.beans.WorkNotifyBean;
import com.jason.fingerprint.beans.WorkNotifyResp;
import com.jason.fingerprint.configs.HttpConfig;
import com.jason.fingerprint.utils.CodecUtils;

/**
 * @ClassName: WorkNotifySyncLogic
 * @Description: 工作通知
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月14日01:24:57
 */
public class WorkNotifySyncLogic extends Logic {

	private static final String TAG = "WorkNotifySyncLogic.java";
	private Handler mHandler;
	private boolean mIsCallBack;// 是否有回调，此需要Handle
	private Message mMessage;
	private int mCurrentIndex = 1;
	
	public WorkNotifySyncLogic(){
		super();
	}
	
	public WorkNotifySyncLogic(AppContext appContext,Handler handler,boolean isCallBack){
		super(appContext);
		this.mIsCallBack = isCallBack;
		this.mHandler = handler;
		this.mMessage = new Message();
	}

	/*
	 * @see com.jason.fingerprint.logic.Logic#execute()
	 */
	@Override
	public void execute() {
		httpPost();
	}

	/*
	 * @see com.jason.fingerprint.logic.Logic#send()
	 */
	@Override
	public void send() {
		if (mIsCallBack) {
			mHandler.sendMessage(mMessage);
			mMessage = new Message();
		}
	}
	
	private void httpPost(){
		mParams = new KJFileParams();
		mParams.put(AppGlobal.HTTP_PARAM_IMEI, mImei);
		mParams.put(AppGlobal.HTTP_PARAM_CURRENTINDEX,
				String.valueOf(mCurrentIndex ));
		mParams.put(AppGlobal.HTTP_PARAM_ORGANID, mMachId);
		String url = HttpConfig.POST_URL_INFOPUBLISH_QUERY;
		Log.i(TAG, "----->API:" + url);
		KJHttp kjHttp = new KJHttp();
		kjHttp.post(url, mParams, new HttpCallBack() {

			@Override
			public void onLoading(long count, long current) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onSuccess(Object arg0) {
				// TODO Auto-generated method stub
				String result = CodecUtils.base64Decode(arg0.toString());
				Log.i(TAG, "----->Result:" + result);
				WorkNotifyResp workNotifyResp = new GsonBuilder().create().fromJson(
						result, WorkNotifyResp.class);
				// 存入数据库
				if (workNotifyResp != null && !workNotifyResp.isEmpty()) {
					Log.i(TAG, "----->workNotifyResp:" + workNotifyResp.toString());
					List<WorkNotifyBean> beans = workNotifyResp.getValues();
					try {
						for (WorkNotifyBean bean : beans) {
							if (bean != null) {
								//先判断此信息是否在数据库中存在，如果存在，不存在直接添加
								mKjdb.save(bean);
							}
						}
						if (!workNotifyResp.isLastPage()) {
							mCurrentIndex++;
							httpPost();
						} else {
							mMessage.what = 0;
							send();
						}
					} catch (Exception e) {
						mMessage.what = 1;
						send();
					}
				} else {
					mMessage.what = 2;
					send();
				}
			}

			@Override
			public void onFailure(Throwable t, int errorNo, String strMsg) {
				// TODO Auto-generated method stub
				mMessage.what = 2;
				send();
			}
			
		});
	}

	/* (non-Javadoc)
	 * @see com.jason.fingerprint.logic.Logic#destory()
	 */
	@Override
	public void destory() {
		// TODO Auto-generated method stub
		
	}
	
}
