package com.jason.fingerprint.logic;

import java.util.ArrayList;
import java.util.List;

import org.kymjs.aframe.http.HttpCallBack;
import org.kymjs.aframe.http.KJFileParams;
import org.kymjs.aframe.http.KJHttp;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.google.gson.GsonBuilder;
import com.jason.fingerprint.AppContext;
import com.jason.fingerprint.AppGlobal;
import com.jason.fingerprint.beans.SponsorBean;
import com.jason.fingerprint.beans.SponsorResp;
import com.jason.fingerprint.configs.HttpConfig;
import com.jason.fingerprint.utils.CodecUtils;

/**
 * @ClassName: HandleSponsorBeanLogic
 * @Description: 处理相关社区服务、集中教育发起表
 * @Author: John
 * @Date: 2014/10/21
 */
public class HandleSponsorBeanLogic extends Logic {

	private static final String TAG = "HandleSponsorBeanLogic";

	private Handler mHandler;
	private boolean mIsCallBack = false; // 是否有回调，此需要Handle,默认不回调
	private int mCurrentIndex = 1; // 当前页
	private Message mMessage;

	private List<SponsorBean> mSponsorBeans = new ArrayList<SponsorBean>();

	public HandleSponsorBeanLogic(AppContext appContext) {
		super(appContext);
		mSponsorBeans = new ArrayList<SponsorBean>();
	}

	public HandleSponsorBeanLogic(AppContext appContext, Handler handler) {
		this(appContext);
		this.mHandler = handler;
		this.mMessage = new Message();
	}

	public HandleSponsorBeanLogic(AppContext appContext, Handler handler,
			boolean isCallBack) {
		this(appContext);
		this.mHandler = handler;
		this.mMessage = new Message();
		this.mIsCallBack = isCallBack;
	}

	@Override
	public void execute() {
		getTodayActivities("1");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jason.fingerprint.logic.Logic#send()
	 */
	@Override
	public void send() {
		if (mIsCallBack) {
			mHandler.sendMessage(mMessage);
		}

	}

	public List<SponsorBean> getSponsorBeanList() {
		return mSponsorBeans;
	}

	/**
	 * 获取当前活动（包括社区活动与教育）
	 * @param type 发起类型（0:集中教育，1：社区服务）
	 */
	public void getTodayActivities(final String type) {
		mParams = new KJFileParams();
		mParams.put(AppGlobal.HTTP_PARAM_IMEI, mImei);
		mParams.put(AppGlobal.HTTP_PARAM_ORGANID, mMachId);
		mParams.put(AppGlobal.HTTP_PARAM_TYPE, type);
		String url = HttpConfig.POST_URL_EDUCOM_GETACTIVITYLIST;
		Log.i(TAG, "----->API:" + url);
		KJHttp kjHttp = new KJHttp();
		kjHttp.post(url, mParams, new HttpCallBack() {

			@Override
			public void onSuccess(Object arg0) {
				String result = CodecUtils.base64Decode(arg0.toString());
				Log.i(TAG, "----->Result:" + result);
				SponsorResp sponsorResp = new GsonBuilder().create().fromJson(
						result, SponsorResp.class);
				if (sponsorResp != null && !sponsorResp.isEmpty()) {
					Log.i(TAG, "----->organResp:" + sponsorResp.toString());
					mSponsorBeans = sponsorResp.getValues();
					mMessage.obj = mSponsorBeans;
					mMessage.arg1 = 0;
					send();
				} else {
					mMessage.arg1 = 1;
					send();
				}
			}

			@Override
			public void onLoading(long arg0, long arg1) {
				Log.i(TAG, "----->Result:arg0:" + arg0 + ",arg1:" + arg1);
			}

			@Override
			public void onFailure(Throwable arg0, int arg1, String arg2) {
				Log.i(TAG, "----->Result:arg1:" + arg1 + ",arg2:" + arg2);
				mMessage.arg1 = 2;
				send();
			}
		});
	}

	/* (non-Javadoc)
	 * @see com.jason.fingerprint.logic.Logic#destory()
	 */
	@Override
	public void destory() {
		// TODO Auto-generated method stub
		
	}

}
