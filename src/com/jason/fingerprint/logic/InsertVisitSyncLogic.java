/**
 * @Title: InsertVisitSyncLogic.java
 * @Package: com.jason.fingerprint.logic
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月25日 上午10:54:03
 * @Version: V1.0
 */
package com.jason.fingerprint.logic;

import java.util.ArrayList;
import java.util.List;

import org.kymjs.aframe.http.HttpCallBack;
import org.kymjs.aframe.http.KJFileParams;
import org.kymjs.aframe.http.KJHttp;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.google.gson.GsonBuilder;
import com.jason.fingerprint.AppContext;
import com.jason.fingerprint.AppGlobal;
import com.jason.fingerprint.beans.WorkVisitBean;
import com.jason.fingerprint.beans.WorkVisitResp;
import com.jason.fingerprint.common.DateFormat;
import com.jason.fingerprint.common.UIHelper;
import com.jason.fingerprint.configs.HttpConfig;
import com.jason.fingerprint.utils.CodecUtils;
import com.jason.fingerprint.utils.DataUtils;

/**
 * @ClassName: InsertVisitSyncLogic
 * @Description: 上传走访记录
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月25日 上午10:54:03
 */
public class InsertVisitSyncLogic extends Logic {

	private static final String TAG = "InsertVisitSyncLogic.java";
	private Handler mHandler;
	private boolean mIsCallBack;// 是否有回调，此需要Handle
	private Message mMessage;
	private int mCurrentIndex = 0;
	private int mSize = 0;
	private List<WorkVisitBean> mWorkVisitBeans = new ArrayList<WorkVisitBean>();

	public InsertVisitSyncLogic() {
		super();
	}

	public InsertVisitSyncLogic(AppContext appContext, Handler handler,
			boolean isCallBack) {
		super(appContext);
		this.mIsCallBack = isCallBack;
		this.mHandler = handler;
		this.mMessage = new Message();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jason.fingerprint.logic.Logic#execute()
	 */
	@Override
	public void execute() {
		try {
			mWorkVisitBeans = mKjdb.findAllByWhere(WorkVisitBean.class,
					"state = 1");
			if (mWorkVisitBeans != null && !mWorkVisitBeans.isEmpty()) {
				mSize = mWorkVisitBeans.size();
				httpPost();
			} else {
				mMessage.what = UIHelper.WORK_VISIT_UPLOAD_SUCCESS;
				send();
			}
		} catch (Exception e) {
			mMessage.what = UIHelper.WORK_VISIT_UPLOAD_SUCCESS;
			send();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jason.fingerprint.logic.Logic#send()
	 */
	@Override
	public void send() {
		if (mIsCallBack) {
			mHandler.sendMessage(mMessage);
			mMessage = new Message();
		}
	}

	private void httpPost() {
		if (mCurrentIndex >= mSize) {
			mMessage.what = UIHelper.WORK_VISIT_UPLOAD_SUCCESS;
			send();
			return;
		}
		final WorkVisitBean bean = mWorkVisitBeans.get(mCurrentIndex);
		if (bean == null) {
			mMessage.what = UIHelper.WORK_VISIT_UPLOAD_FAIL;
			send();
			return;
		}else {
			mCurrentIndex ++;
			httpPost();
		}
		mParams = new KJFileParams();
		mParams.put(AppGlobal.HTTP_PARAM_IMEI, mImei);
		mParams.put("userId", bean.getUserId());
		mParams.put("rymcId", bean.getRymcId());
		mParams.put("interViewTime", DataUtils.converDatLongToString(
				bean.getInterViewTime(), DateFormat.DATE_YMD_HMS));
		mParams.put("interViewAddress", bean.getInterViewAddress());
		mParams.put("longitude", bean.getLongitude());
		mParams.put("latitude", bean.getLatitude());
		mParams.put("interViewWqk", bean.getInterViewWqk());
		String url = HttpConfig.POST_URL_WORKINGVISIT_INSERTVISIT;
		Log.i(TAG, "----->API:" + url);
		KJHttp kjHttp = new KJHttp();
		kjHttp.post(url, mParams, new HttpCallBack() {

			@Override
			public void onLoading(long count, long current) {
				
			}

			@Override
			public void onSuccess(Object arg0) {
				// TODO Auto-generated method stub
				try {
					String result = CodecUtils.base64Decode(arg0.toString());
					Log.i(TAG, "----->Result:" + result);
					WorkVisitResp workVisitResp = new GsonBuilder().create()
							.fromJson(result, WorkVisitResp.class);
					if (workVisitResp != null && !workVisitResp.isEmpty()
							&& workVisitResp.isSuccess()) {
						String id = workVisitResp.getValues().get(0);
						// 跟新数据库数据
						bean.setState(2);
						bean.setWorkId(Integer.valueOf(id));
						mKjdb.update(bean);
						mCurrentIndex++;
						httpPost();
					}
				} catch (Exception e) {
					mMessage.what = UIHelper.WORK_VISIT_UPLOAD_FAIL;
					send();
				}

			}

			@Override
			public void onFailure(Throwable t, int errorNo, String strMsg) {
				mMessage.what = UIHelper.WORK_VISIT_UPLOAD_FAIL;
				send();
			}

		});
	}

	/* (non-Javadoc)
	 * @see com.jason.fingerprint.logic.Logic#destory()
	 */
	@Override
	public void destory() {
		// TODO Auto-generated method stub
		
	}

}
