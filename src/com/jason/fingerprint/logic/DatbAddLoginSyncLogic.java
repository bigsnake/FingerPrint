/**
 * @Title: DatbAddLoginSyncLogic.java
 * @Package: com.jason.fingerprint.logic
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月4日23:35:19
 * @Version: V1.0
 */
package com.jason.fingerprint.logic;

import org.kymjs.aframe.http.HttpCallBack;
import org.kymjs.aframe.http.KJFileParams;
import org.kymjs.aframe.http.KJHttp;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.jason.fingerprint.AppContext;
import com.jason.fingerprint.beans.AddLoginInfoReq;
import com.jason.fingerprint.configs.HttpConfig;
import com.jason.fingerprint.utils.CodecUtils;

/**
 * @ClassName: DatbAddLoginSyncLogic
 * @Description: 添加登录日志
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月4日23:34:57
 */
public class DatbAddLoginSyncLogic extends Logic {

	private static final String TAG = "DatbAddLoginSyncLogic";

	private Handler mHandler;
	private boolean mIsCallBack = false;// 是否有回调，此需要Handle,默认不回调
	private Message mMessage;
	private AddLoginInfoReq mAddLoginInfoReq;

	public DatbAddLoginSyncLogic() {

	}

	public DatbAddLoginSyncLogic(AppContext appContext, Handler handler) {
		super(appContext);
		this.mHandler = handler;
		this.mMessage = new Message();
	}

	public DatbAddLoginSyncLogic(AppContext appContext, Handler handler,
			boolean isCallBack) {
		super(appContext);
		this.mHandler = handler;
		this.mMessage = new Message();
		this.mIsCallBack = isCallBack;
	}
	
	public DatbAddLoginSyncLogic(AppContext appContext, Handler handler,
			boolean isCallBack,AddLoginInfoReq loginInfoReq) {
		super(appContext);
		this.mHandler = handler;
		this.mMessage = new Message();
		this.mIsCallBack = isCallBack;
		this.mAddLoginInfoReq = loginInfoReq;
	}

	@Override
	public void execute() {
		datbAddLoginInfoSync();
	}

	@Override
	public void send() {
		if (mIsCallBack) {
			mHandler.sendMessage(mMessage);
		}
	}

	// 档案同步
	private void datbAddLoginInfoSync() {
		mParams = new KJFileParams();
		mParams.put("type", String.valueOf(mAddLoginInfoReq.getType()));
		mParams.put("loginName", mAddLoginInfoReq.getLoginName());
		mParams.put("userName", mAddLoginInfoReq.getUserName());
		//TODO mAddLoginInfoReq.getOrgan()不能为空
		mParams.put("organ", mAddLoginInfoReq.getOrgan());
		mParams.put("organId", mAddLoginInfoReq.getOrganId());
		String url = HttpConfig.POST_URL_DATB_ADDLOGININFO;
		Log.i(TAG, "----->API:" + url);
		Log.i(TAG, "----->Params:" + mAddLoginInfoReq.toString());
		KJHttp kjHttp = new KJHttp();
		kjHttp.post(url, mParams, new HttpCallBack() {

			@Override
			public void onSuccess(Object arg0) {
				String result = CodecUtils.base64Decode(arg0.toString());
				Log.i(TAG, "----->Result:" + result);
				
			}

			@Override
			public void onLoading(long arg0, long arg1) {
				Log.i(TAG, "----->Result:arg0:" + arg0 + ",arg1:" + arg1);
				mMessage.arg1 = 3;
				send();
			}

			@Override
			public void onFailure(Throwable arg0, int arg1, String arg2) {
				Log.i(TAG, "----->Result:arg1:" + arg1 + ",arg2:" + arg2);
				mMessage.arg1 = 3;
				send();
			}
		});
	}

	/* (non-Javadoc)
	 * @see com.jason.fingerprint.logic.Logic#destory()
	 */
	@Override
	public void destory() {
		// TODO Auto-generated method stub
		
	}

}
