/**
 * @Title: Logic.java
 * @Package: com.jason.fingerprint.logic
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月21日 上午9:58:26
 * @Version: V1.0
 */
package com.jason.fingerprint.logic;

import org.kymjs.aframe.database.KJDB;
import org.kymjs.aframe.http.KJFileParams;
import org.kymjs.aframe.utils.StringUtils;

import android.util.Log;

import com.jason.fingerprint.AppContext;

/**
 * @ClassName: Logic
 * @Description: 业务处理的基类
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月21日 上午9:58:26
 */
public abstract class Logic {
	
	private static final String TAG = "Logic";
	
	public String mImei;
	public String mMachId;
	public KJDB mKjdb;
	public KJFileParams mParams;
	
	public Logic(){
		
	}
	
	public Logic(AppContext appContext){
		this.mImei = AppContext.getInstance().getImei();
		
		//TODO 一些新机上面可能获取不到imei号码
		Log.i("Logic", "---->mImei:" + mImei);
		if (StringUtils.isEmpty(mImei)) {
			mImei = "864121017006490";
		}
		Log.i(TAG, "mImei:" + mImei);
		this.mMachId = String.valueOf(AppContext.getInstance().getMachId());
		this.mParams = new KJFileParams();
		this.mKjdb = AppContext.getInstance().getKjdb();
	}

	/***
	 * 方法执行类
	 * @Descripton: TODO
	 * @Param 
	 * @Return $
	 * @Throws
	 */
	public abstract void execute();
	
	/***
	 * 返回处理结果
	 * @Descripton: TODO
	 * @Param 
	 * @Return $
	 * @Throws
	 */
	public abstract void send();
	
	public abstract void destory();
	
}
