/**
 * @Title: package-info.java
 * @Package: com.jason.fingerprint.logic
 * @Descripton: 网络请求的一些逻辑处理
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月21日 上午9:52:28
 * @Version: V1.0
 */

package com.jason.fingerprint.logic;