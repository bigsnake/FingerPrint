package com.jason.fingerprint.logic;
/**
 * @Title: RegisterFingerprintSyncLogic.java
 * @Package: com.jason.fingerprint.logic
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月24日 下午9:38:14
 * @Version: V1.0
 */

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android_serialport_api.FingerprintManager;
import android_serialport_api.FingerprintReader;
import android_serialport_api.FingerprintReader.OnDeleteCharListener;
import android_serialport_api.FingerprintReader.OnEmptyListener;
import android_serialport_api.FingerprintReader.OnGenCharExListener;
import android_serialport_api.FingerprintReader.OnGetImageExListener;
import android_serialport_api.FingerprintReader.OnLoadCharListener;
import android_serialport_api.FingerprintReader.OnRegModelListener;
import android_serialport_api.FingerprintReader.OnSearchListener;
import android_serialport_api.FingerprintReader.OnStoreCharListener;
import android_serialport_api.FingerprintReader.OnUpCharListener;
import android_serialport_api.FingerprintReader.OnUpImageExListener;

import com.jason.fingerprint.AppContext;
import com.jason.fingerprint.common.UIHelper;

/**
 * @ClassName: RegisterFingerprintSyncLogic
 * @Description: 注册指纹
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月24日 下午9:38:14
 */
public class RegisterFingerprintSyncLogic extends Logic {

	private static final String TAG = "RegisterFingerprintSyncLogic";

	private FingerprintReader mRegisterFingerprint;
	private Handler mHandler;
	private boolean mIsCallBack = false;// 是否有回调，此需要Handle
	private boolean mIsShowImage = true;//是否显示图像
	private int mCount;
	private Message mMessage;

	public RegisterFingerprintSyncLogic() {

	}

	public RegisterFingerprintSyncLogic(AppContext appContext, Handler handler) {
		super(appContext);
		this.mHandler = handler;
		this.mMessage = new Message();
		initSerialPort();
	}

	public RegisterFingerprintSyncLogic(AppContext appContext, Handler handler,
			boolean isCallBack) {
		super(appContext);
		this.mHandler = handler;
		this.mMessage = new Message();
		this.mIsCallBack = isCallBack;
		initSerialPort();
		initListener();
	}
	
	public RegisterFingerprintSyncLogic(AppContext appContext, Handler handler,
			boolean isCallBack,boolean isShowImage) {
		super(appContext);
		this.mHandler = handler;
		this.mMessage = new Message();
		this.mIsCallBack = isCallBack;
		this.mIsShowImage = isShowImage;
		initSerialPort();
		initListener();
	}

	//初始化注册信息
	private void initSerialPort() {
		mRegisterFingerprint = FingerprintManager.getInstance().getNewAsyncFingerprint();
	}

	@Override
	public void execute() {
		registerFingerprintSync();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jason.fingerprint.logic.Logic#send()
	 */
	@Override
	public void send() {
		if (mIsCallBack) {
			mHandler.sendMessage(mMessage);
			mMessage = new Message();
		}

	}
	
	//初始化监听
	private void initListener(){
		if (mRegisterFingerprint == null) {
			mMessage.what = UIHelper.FINGERPRINT_FAIL;
			Log.i(TAG, "---->setOnGetImageExListener onGetImageExFail 003");
			send();
			return;
		}
		mCount = 1;
		//判断返回时图片还是文字
		mRegisterFingerprint.setOnGetImageExListener(new OnGetImageExListener() {
			@Override
			public void onGetImageExSuccess() {
				Log.i(TAG, "---->setOnGetImageExListener onGetImageExSuccess 001");
				//cancleProgressDialog();
				if(mIsShowImage){
					mRegisterFingerprint.PS_UpImageEx();//
					//showProgressDialog("正在处理...");
					Log.i(TAG, "---->setOnGetImageExListener onGetImageExSuccess 008");
				}else{
					mRegisterFingerprint.PS_GenCharEx(mCount);
					Log.i(TAG, "---->setOnGetImageExListener onGetImageExSuccess 009");
				}
			}

			@Override
			public void onGetImageExFail() {
				Log.i(TAG, "---->setOnGetImageExListener onGetImageExFail 002");
				mRegisterFingerprint.PS_GetImageEx();
			}
		});
		
		//图片---返回的图片结果
		mRegisterFingerprint.setOnUpImageExListener(new OnUpImageExListener() {
			@Override
			public void onUpImageExSuccess(byte[] data) {
				Log.i(TAG, "---->setOnGetImageExListener onUpImageExSuccess 010");
				Log.i(TAG, "---->setOnUpImageExListener up image data.length="+data.length);
				//Bitmap image = BitmapFactory.decodeByteArray(data, 0,data.length);
				//fingerprintImage.setBackgroundDrawable(new BitmapDrawable(image));
				mRegisterFingerprint.PS_GenCharEx(mCount);
				mMessage.what = UIHelper.FINGERPRINT_UPIMAGE_SUCCESS;
				Bundle bundle = new Bundle();
				bundle.putByteArray(UIHelper.FINGERPRINT_SOURCE, data);
				mMessage.setData(bundle);
				send();
			}

			@Override
			public void onUpImageExFail() {
				Log.i(TAG, "---->setOnGetImageExListener onUpImageExFail 011");
				mMessage.what = UIHelper.FINGERPRINT_UPIMAGE_FAIL;
				send();
			}
		});

		//文字---监听文字返回结果
		mRegisterFingerprint.setOnGenCharExListener(new OnGenCharExListener() {
			@Override
			public void onGenCharExSuccess(int bufferId) {
				Log.i(TAG, "---->setOnGenCharExListener onGenCharExSuccess 012 bufferId:" + bufferId);
				if (bufferId == 1) {
					//cancleProgressDialog();
					//showProgressDialog("请再按一次指纹！");
					mRegisterFingerprint.PS_GetImageEx();
					mCount++;
					mMessage.what = UIHelper.FINGERPRINT_AGAIN;
					send();
				} else if (bufferId == 2) {
					mRegisterFingerprint.PS_RegModel();
				}
			}

			@Override
			public void onGenCharExFail() {
				Log.i(TAG, "---->setOnGenCharExListener onGenCharExFail 013");
				//cancleProgressDialog();
				//ToastUtil.showToast(FingerprintActivity.this,"生成特征值失败！");
				mMessage.what = UIHelper.FINGERPRINT_GENCHAR_FAIL;
				send();
			}
		});

		//文字---文字模板合成
		mRegisterFingerprint.setOnRegModelListener(new OnRegModelListener() {

			@Override
			public void onRegModelSuccess() {
				Log.i(TAG, "---->setOnGenCharExListener onGenCharExSuccess 014");
				//cancleProgressDialog();
				mRegisterFingerprint.PS_UpChar();
				//ToastUtil.showToast(FingerprintActivity.this, "合成模板成功！");
			}

			@Override
			public void onRegModelFail() {
				Log.i(TAG, "---->setOnGenCharExListener onGenCharExSuccess 015");
				//cancleProgressDialog();
				//ToastUtil.showToast(FingerprintActivity.this, "合成模板失败！");
				mMessage.what = UIHelper.FINGERPRINT_REGMODE_FAIL;
				send();
			}
		});

		//文字---返回文字结果集
		mRegisterFingerprint.setOnUpCharListener(new OnUpCharListener() {
			@Override
			public void onUpCharSuccess(byte[] model) {
				Log.i(TAG, "---->setOnGenCharExListener onGenCharExSuccess 016 lenth:" + model.length);
				//cancleProgressDialog();
				//Log.i("whw", "#################model.length="+model.length);
				//FingerprintActivity.this.model = model;
				//ToastUtil.showToast(FingerprintActivity.this, "注册成功！");
				mMessage.what = UIHelper.FINGERPRINT_UPCHAR_SUCCESS;
				//Log.i(TAG, "---->setOnGenCharExListener onGenCharExSuccess 016 model:" + model);
				Bundle bundle = new Bundle();
				bundle.putByteArray(UIHelper.FINGERPRINT_SOURCE, model);
				mMessage.setData(bundle);
				send();
			}

			@Override
			public void onUpCharFail() {
				Log.i(TAG, "---->setOnGenCharExListener onGenCharExSuccess 017");
				//cancleProgressDialog();
				//ToastUtil.showToast(FingerprintActivity.this, "注册失败！");
				mMessage.what = UIHelper.FINGERPRINT_UPCHAR_FAIL;
				send();
			}
		});
		
		//储存模板
		mRegisterFingerprint.setOnStoreCharListener(new OnStoreCharListener() {
			@Override
			public void onStoreCharSuccess() {
				//ToastUtil.showToast(FingerprintActivity.this, "储存模板成功！");
				Log.i(TAG, "---->setOnGenCharExListener onStoreCharSuccess 018");
				mMessage.what = UIHelper.FINGERPRINT_ONSTORECHAR_SUCCESS;
			}

			@Override
			public void onStoreCharFail() {
				//ToastUtil.showToast(FingerprintActivity.this, "储存模板失败！");
				Log.i(TAG, "---->setOnGenCharExListener onStoreCharFail 019");
				mMessage.what = UIHelper.FINGERPRINT_ONSTORECHAR_FAIL;
			}
		});

		//加载模板
		mRegisterFingerprint.setOnLoadCharListener(new OnLoadCharListener() {
			@Override
			public void onLoadCharSuccess() {
				//ToastUtil.showToast(FingerprintActivity.this, "加载模板成功！");
				mMessage.what = UIHelper.FINGERPRINT_LOADCHAR_SUCCESS;
				Log.i(TAG, "---->setOnGenCharExListener onLoadCharSuccess " + 20);
			}

			@Override
			public void onLoadCharFail() {
				//ToastUtil.showToast(FingerprintActivity.this, "加载模板失败！");
				mMessage.what = UIHelper.FINGERPRINT_LOADCHAR_FAIL;
				Log.i(TAG, "---->setOnGenCharExListener onLoadCharFail " + 21);
			}
		});

		//搜索指纹
		mRegisterFingerprint.setOnSearchListener(new OnSearchListener() {
			@Override
			public void onSearchSuccess(int pageId, int matchScore) {
				//ToastUtil.showToast(FingerprintActivity.this, "搜索成功！");
				Log.i("whw", "pageId=" + pageId + "    matchScore="	+ matchScore);
				mMessage.what = UIHelper.FINGERPRINT_SEARCH_SUCCESS;
				Log.i(TAG, "---->setOnGenCharExListener setOnSearchListener " + 22);
			}

			@Override
			public void onSearchFail() {
				//ToastUtil.showToast(FingerprintActivity.this, "搜索失败！");
				mMessage.what = UIHelper.FINGERPRINT_SEARCH_FAIL;
				Log.i(TAG, "---->setOnGenCharExListener onSearchFail " + 23);
			}
		});

		//删除制定的指纹
		mRegisterFingerprint.setOnDeleteCharListener(new OnDeleteCharListener() {
			@Override
			public void onDeleteCharSuccess() {
				Log.i(TAG, "---->setOnDeleteCharListener onSearchFail " + 24);
				mMessage.what = UIHelper.FINGERPRINT_DELETECHAR_SUCCESS;
			}

			@Override
			public void onDeleteCharFail() {
				Log.i(TAG, "---->onDeleteCharFail onSearchFail " + 25);
				mMessage.what = UIHelper.FINGERPRINT_DELETECHAR_FAIL;
			}
		});

		//清空数据
		mRegisterFingerprint.setOnEmptyListener(new OnEmptyListener() {
			@Override
			public void onEmptySuccess() {
				Log.i(TAG, "---->onDeleteCharFail onEmptySuccess " + 26);
				//Log.i("whw", "清空成功！");
				mMessage.what = UIHelper.FINGERPRINT_EMPTY_SUCCESS;
			}

			@Override
			public void onEmptyFail() {
				Log.i(TAG, "---->onDeleteCharFail onEmptyFail " + 27);
				//Log.i("whw", "清空失败！");
				mMessage.what = UIHelper.FINGERPRINT_EMPTY_FAIL;
			}
		});
	}

	// 录入指纹
	private void registerFingerprintSync() {
		if (mRegisterFingerprint != null) {
			mRegisterFingerprint.PS_GetImageEx();
		}
	}

	/* (non-Javadoc)
	 * @see com.jason.fingerprint.logic.Logic#destory()
	 */
	@Override
	public void destory() {
		// TODO Auto-generated method stub
		
	}
	
}
