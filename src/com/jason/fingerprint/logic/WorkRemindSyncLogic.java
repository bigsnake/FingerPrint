/**
 * @Title: WorkRemindSyncLogic.java
 * @Package: com.jason.fingerprint.logic
 * @Descripton: 初始化应用信息
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月14日01:24:52
 * @Version: V1.0
 */
package com.jason.fingerprint.logic;

import org.kymjs.aframe.http.HttpCallBack;
import org.kymjs.aframe.http.KJFileParams;
import org.kymjs.aframe.http.KJHttp;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.jason.fingerprint.AppContext;
import com.jason.fingerprint.AppGlobal;
import com.jason.fingerprint.configs.HttpConfig;

/**
 * @ClassName: WorkRemindSyncLogic
 * @Description: 工作提醒
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月14日01:24:57
 */
public class WorkRemindSyncLogic extends Logic {

	private static final String TAG = "WorkRemindSyncLogic.java";
	private Handler mHandler;
	private boolean mIsCallBack;// 是否有回调，此需要Handle
	private Message mMessage;
	private int mCurrentIndex = 1;
	
	public WorkRemindSyncLogic(){
		super();
	}
	
	public WorkRemindSyncLogic(AppContext appContext,Handler handler,boolean isCallBack){
		super(appContext);
		this.mIsCallBack = isCallBack;
		this.mHandler = handler;
		this.mMessage = new Message();
	}

	/*
	 * @see com.jason.fingerprint.logic.Logic#execute()
	 */
	@Override
	public void execute() {
		httpPost();
	}

	/*
	 * @see com.jason.fingerprint.logic.Logic#send()
	 */
	@Override
	public void send() {
		if (mIsCallBack) {
			mHandler.sendMessage(mMessage);
			mMessage = new Message();
		}
	}
	
	private void httpPost(){
		mParams = new KJFileParams();
		mParams.put(AppGlobal.HTTP_PARAM_IMEI, mImei);
		String url = HttpConfig.POST_URL_WORKINGREMIND_QUERYREMIND;
		Log.i(TAG, "----->API:" + url);
		KJHttp kjHttp = new KJHttp();
		kjHttp.post(url, mParams, new HttpCallBack() {

			@Override
			public void onLoading(long count, long current) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onSuccess(Object arg0) {
				// TODO Auto-generated method stub
				Log.i(TAG, "----->Result:" + arg0.toString());
//				String result = CodecUtils.base64Decode(arg0.toString());
//				Log.i(TAG, "----->Result:" + result);
			}

			@Override
			public void onFailure(Throwable t, int errorNo, String strMsg) {
				// TODO Auto-generated method stub
				
			}
			
		});
	}

	/* (non-Javadoc)
	 * @see com.jason.fingerprint.logic.Logic#destory()
	 */
	@Override
	public void destory() {
		// TODO Auto-generated method stub
		
	}
	
}
