/**
 * @Title: DailyReportSyncLogic.java
 * @Package: com.jason.fingerprint.logic
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月17日 上午10:58:52
 * @Version: V1.0
 */
package com.jason.fingerprint.logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.kymjs.aframe.http.HttpCallBack;
import org.kymjs.aframe.http.KJFileParams;
import org.kymjs.aframe.http.KJHttp;
import org.kymjs.aframe.utils.StringUtils;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.google.gson.GsonBuilder;
import com.jason.fingerprint.AppContext;
import com.jason.fingerprint.AppGlobal;
import com.jason.fingerprint.beans.BaseBean;
import com.jason.fingerprint.common.DateFormat;
import com.jason.fingerprint.configs.HttpConfig;
import com.jason.fingerprint.utils.CodecUtils;
import com.jason.fingerprint.utils.DataUtils;

/**
 * @ClassName: DailyReportSyncLogic
 * @Description: 上传日常报到信息到服务器
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月17日 上午10:58:52
 */
public class DailyReportSyncLogic extends Logic {

	private static final String TAG = "DailyReportSyncLogic";

	private Handler mHandler;
	private boolean mIsCallBack = false;// 是否有回调，此需要Handle,默认不回调
	private Message mMessage;

	public DailyReportSyncLogic() {

	}

	public DailyReportSyncLogic(AppContext appContext, Handler handler) {
		super(appContext);
		this.mHandler = handler;
		this.mMessage = new Message();
	}

	public DailyReportSyncLogic(AppContext appContext, Handler handler,
			boolean isCallBack) {
		super(appContext);
		this.mHandler = handler;
		this.mMessage = new Message();
		this.mIsCallBack = isCallBack;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jason.fingerprint.logic.Logic#execute()
	 */
	@Override
	public void execute() {
		// TODO Auto-generated method stub
		uploadDailyReportInfo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jason.fingerprint.logic.Logic#send()
	 */
	@Override
	public void send() {
		// TODO Auto-generated method stub
		if (mIsCallBack) {
			mHandler.sendMessage(mMessage);
			mMessage = new Message();
		}
	}

	private void uploadDailyReportInfo() {
		// path:/storage/sdcard0/FingerPrint/Images/20141104_032551.jpg

		// serviceId 分发服务ID
		// 1、String rymcId //人员ID
		// 2、String startTime //签到开始时间
		// 3、String endTime //签到结束时间
		// 4、File fileInput //图片文件
		// 5、String fileInputFileName //文件名称
		// 6、String imie;//IMIE号 mImei:860312023914656
		// DataUtils.converDatLongToString(beginTime*1000,DateFormat.DATE_YMD_HMS)
		mParams = new KJFileParams();
		mParams.put(AppGlobal.HTTP_PARAM_IMEI, mImei);
		mParams.put("serviceId", "1");
		mParams.put("rymcId", "100123");
		mParams.put("startTime", DataUtils.converDatLongToString(
				System.currentTimeMillis(), DateFormat.DATE_YMD_HMS));
		mParams.put("endTime", DataUtils.converDatLongToString(
				System.currentTimeMillis() + 1000*60*60*6, DateFormat.DATE_YMD_HMS));
		try {
			File file = new File("/storage/sdcard0/FingerPrint/Images/20141104_032551.jpg"); 
			FileInputStream stream = new FileInputStream(file);
			mParams.put("fileInput", stream, "20141104_032551.jpg");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mParams.put("fileInputFileName", "20141104_032551.jpg");
		String url = HttpConfig.POST_URL_FILEUPLOAD_UPLOADTOTEMP;
		Log.i(TAG, "----->API:" + url);
		KJHttp kjHttp = new KJHttp();
		kjHttp.post(url, mParams, new HttpCallBack() {

			@Override
			public void onSuccess(Object arg0) {
				Log.i(TAG, "----->arg0:" + arg0.toString());
				String result = null;
				try {
					result = CodecUtils.base64Decode(arg0.toString());
				} catch (Exception e) {
					mMessage.what = 3;
					send();
					return;
				}
				Log.i(TAG, "----->Result:" + result);
				BaseBean bean = new GsonBuilder().create().fromJson(result,
						BaseBean.class);
				if (bean != null && bean.isSuccess()) {
					String errorMessage = bean.getRespMsg();
					if (!StringUtils.isEmpty(errorMessage)
							&& errorMessage.equals("上传成功")) {
						mMessage.what = 0;
						send();
					} else {
						mMessage.what = 1;
						mMessage.obj = errorMessage;
						send();
					}
				} else {
					mMessage.what = 2;
					send();
				}
			}

			@Override
			public void onLoading(long arg0, long arg1) {
				Log.i(TAG, "----->Result:arg0:" + arg0 + ",arg1:" + arg1);
			}

			@Override
			public void onFailure(Throwable arg0, int arg1, String arg2) {
				Log.i(TAG, "----->Result:arg1:" + arg1 + ",arg2:" + arg2);
				mMessage.what = 3;
				send();
			}
		});

	}

	/* (non-Javadoc)
	 * @see com.jason.fingerprint.logic.Logic#destory()
	 */
	@Override
	public void destory() {
		// TODO Auto-generated method stub
		
	}

}
