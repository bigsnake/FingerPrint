/**
 * @Title: BrowserActivity.java
 * @Package: com.jason.fingerprint.ui
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月14日 下午3:33:45
 * @Version: V1.0
 */
package com.jason.fingerprint.ui;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.KJActivityManager;
import org.kymjs.aframe.ui.activity.BaseActivity;
import org.kymjs.aframe.utils.StringUtils;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.WebSettings;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import com.jason.fingerprint.R;
import com.jason.fingerprint.widget.ProgressWebView;

/**
 * @ClassName: BrowserActivity
 * @Description: 浏览器
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月14日 下午3:33:45
 */
public class BrowserActivity extends BaseActivity {
	
	private static final String TAG = "BrowserActivity";
	public static final String BROWSER_URL = "browser_url";
	
	@BindView(id = R.id.header_back, click = true)
	private Button mBackButton;
	@BindView(id = R.id.header_title)
	private TextView mTitleTextView;
	@BindView(id = R.id.browser_webview)
	private ProgressWebView mProgressWebView;
	
	private String mUrl;

	/* (non-Javadoc)
	 * @see org.kymjs.aframe.ui.activity.I_KJActivity#setRootView()
	 */
	@Override 
	public void setRootView() {
		// TODO Auto-generated method stub
		setContentView(R.layout.activity_browser);
	}
	
	/* (non-Javadoc)
	 * @see org.kymjs.aframe.ui.activity.KJFrameActivity#initWidget()
	 */
	@Override
	protected void initWidget() {
		// TODO Auto-generated method stub
		super.initWidget();
		initWebView();
	}
	
	@SuppressLint("SetJavaScriptEnabled") @SuppressWarnings("deprecation")
	private void initWebView() { 
		mProgressWebView.getSettings().setJavaScriptEnabled(true); 
		mProgressWebView.getSettings().setRenderPriority(RenderPriority.HIGH); 
		mProgressWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);  //设置 缓存模式 
        // 开启 DOM storage API 功能 
		mProgressWebView.getSettings().setDomStorageEnabled(true);
        //开启 database storage API 功能 
		mProgressWebView.getSettings().setDatabaseEnabled(true);  
		mProgressWebView.setDownloadListener(new DownloadListener(){

			@Override
			public void onDownloadStart(String url, String userAgent,
					String contentDisposition, String mimetype,
					long contentLength) {
				// TODO Auto-generated method stub
				if (url != null && (url.startsWith("http://") || url.startsWith("https://"))) {
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
				}
			}
			
		});
		
		mProgressWebView.setWebViewClient(new WebViewClient(){

			/* (non-Javadoc)
			 * @see android.webkit.WebViewClient#shouldOverrideUrlLoading(android.webkit.WebView, java.lang.String)
			 */
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				// TODO Auto-generated method stub
				// 重写此方法表明点击网页里面的链接还是在当前的webview里跳转，不跳到浏览器那边
				view.loadUrl(url);
				return true;
			}
			
		});
    }  
	
	/* (non-Javadoc)
	 * @see org.kymjs.aframe.ui.activity.KJFrameActivity#initData()
	 */
	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		super.initData();
		Intent intent = getIntent();
		if (intent != null) {
			mUrl = intent.getStringExtra(BROWSER_URL);
			Log.i(TAG, "mUrl:" + mUrl);
			if (!StringUtils.isEmpty(mUrl) && (mUrl.startsWith("http://") || mUrl.startsWith("https://"))) {
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						mProgressWebView.loadUrl(mUrl);
						
					}
				}).run();
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see org.kymjs.aframe.ui.activity.KJFrameActivity#widgetClick(android.view.View)
	 */
	@Override
	public void widgetClick(View v) {
		// TODO Auto-generated method stub
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.header_back:
			KJActivityManager.create().finishActivity(this);
			break;

		default:
			break;
		}
	}
	
	/* (non-Javadoc)
	 * @see org.kymjs.aframe.ui.activity.BaseActivity#onKeyDown(int, android.view.KeyEvent)
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && mProgressWebView.canGoBack()) {
			mProgressWebView.goBack();
			return true;
		}
		return false;
	}

}
