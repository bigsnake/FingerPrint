/**
 * @Title: LoadingActivity.java
 * @Package: com.jason.fingerprint.ui
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月29日15:24:29
 * @Version: V1.0
 */
package com.jason.fingerprint.ui;

import org.kymjs.aframe.database.KJDB;
import org.kymjs.aframe.ui.activity.BaseActivity;
import org.kymjs.aframe.utils.SystemTool;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android_serialport_api.FingerprintManager;
import android_serialport_api.FingerprintReader;

import com.jason.fingerprint.AppContext;
import com.jason.fingerprint.R;
import com.jason.fingerprint.beans.LoginReq;
import com.jason.fingerprint.beans.UserBean;
import com.jason.fingerprint.beans.ui.FingerPrintBean;
import com.jason.fingerprint.common.UIHelper;
import com.jason.fingerprint.configs.HttpConfig;
import com.jason.fingerprint.logic.LoginSyncLogic;

/**
 * @ClassName: LoadingActivity
 * @Description: 程序启动页面，以及数据初始化
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月22日 下午9:29:16
 */
public class LoadingActivity extends BaseActivity {

	private static final String TAG = "LoadingActivity.java";
	private static final int LOADING_TIME = 1500;
	private AppContext mAppContext;
	private Context mContext;
	private KJDB mKjdb;

	@Override
	public void setRootView() {
		setContentView(R.layout.activity_loading);
	}

	/*
	 * @see org.kymjs.aframe.ui.activity.KJFrameActivity#initData()
	 */
	@Override
	protected void initData() {
		super.initData();
		init();

	}

	@Override
	protected void initWidget() {
		super.initWidget();
	}

	private void init() {
		mAppContext = (AppContext) getApplication();
		mContext = getApplicationContext();
		mKjdb = mAppContext.getKjdb();
		mKjdb.deleteTable(UserBean.class);
		mKjdb.deleteTable(FingerPrintBean.class);
		if (SystemTool.checkNet(mContext)) {
			// 同步之前需要清空数据库中该表的数据
			login();
		}else {
			new Handler().postDelayed(mRunnable, LOADING_TIME);
		}
	}

	private void login(){
		LoginReq req = new LoginReq(HttpConfig.POST_URL_DATB_LOGIN,
				LoginSyncLogic.LOGIN_FINGERPRINT);

		LoginSyncLogic logic = new LoginSyncLogic(mAppContext, mContext,
				mLoadingLoginHandler, true, req);
		logic.execute();

	}

	private Runnable mRunnable = new Runnable() {

		@Override
		public void run() {
			startActivity(new Intent(LoadingActivity.this, LoginActivity.class));
			finish();
		}

	};

	private Handler mLoadingLoginHandler = new Handler() {

		@Override
		public void handleMessage(android.os.Message msg) {
			Log.i(TAG, "----> mLoadingLoginHandler");
			switch (msg.what) {
			case UIHelper.LOGIN_SUCCESS:
				startActivity(new Intent(LoadingActivity.this, LoginActivity.class));
				finish();
				break;
			case UIHelper.LOGIN_FAIL:
				startActivity(new Intent(LoadingActivity.this, LoginActivity.class));
				finish();
				break;

			default:
				break;
			}

		};
	};
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onDestroy()
	 */
	@Override
	public void onDestroy() {
		super.onDestroy();
		//FingerprintManager.getInstance().closeSerialPort();
	}

}
