package com.jason.fingerprint.ui;

import java.util.ArrayList;
import java.util.List;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.KJActivityManager;

import android.graphics.Color;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android_serialport_api.FingerprintManager;

import com.jason.fingerprint.R;
import com.jason.fingerprint.fragment.AccountLoginFragment;
import com.jason.fingerprint.fragment.FingerPrintLoginFragment;

public class LoginActivity extends FragmentActivity {
	
	public static final String LOGIN_MODE_FINGERPRINT = "1";
	public static final String LOGIN_MODE_ACCOUNT = "2";
	
	@BindView(id = R.id.viewpager)
	private ViewPager mViewPager;

	private ViewPagerAdapter mAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads()  
	            .detectDiskWrites().detectNetwork().penaltyLog().build());  
	    StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectLeakedSqlLiteObjects()  
	            .detectLeakedClosableObjects().penaltyLog().penaltyDeath().build()); 
		
		mViewPager = (ViewPager) this.findViewById(R.id.viewpager);

		List<Fragment> fragmentList = new ArrayList<Fragment>();
		fragmentList.add(new FingerPrintLoginFragment());
		fragmentList.add(new AccountLoginFragment());

		List<String> titles = new ArrayList<String>();
		titles.add("指纹登录");
		titles.add("账户登录");

		mAdapter = new ViewPagerAdapter(this.getSupportFragmentManager(),
				fragmentList, titles);
		mViewPager.setAdapter(mAdapter);

	}
	
	public ViewPager getViewPager(){
		return mViewPager;
	}
	
	class ViewPagerAdapter extends FragmentPagerAdapter {

		private List<Fragment> fragmentList;
		private List<String> titleList;

		public ViewPagerAdapter(FragmentManager fragmentManager,
				List<Fragment> fragmentList, List<String> titleList) {
			super(fragmentManager);
			this.fragmentList = fragmentList;
			this.titleList = titleList;
		}

		/**
		 * 
		 * 得到每个页面
		 */
		@Override
		public Fragment getItem(int arg0) {
			return (fragmentList == null || fragmentList.size() == 0) ? null
					: fragmentList.get(arg0);
		}

		/**
		 * 
		 * 每个页面的title
		 */
		@Override
		public CharSequence getPageTitle(int position) {
			String title = (titleList.size() > position) ? titleList.get(position) : "";
			SpannableStringBuilder builder = new SpannableStringBuilder(title);
			ForegroundColorSpan fcs = new ForegroundColorSpan(Color.WHITE);
			builder.setSpan(fcs, 0, builder.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);//设置字体颜色
			builder.setSpan(new RelativeSizeSpan(1.0f), 0, builder.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			return builder;
		}

		/**
		 * 
		 * 页面的总个数
		 */
		@Override
		public int getCount() {
			return fragmentList == null ? 0 : fragmentList.size();
		}
		
		
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onStop()
	 */
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onDestroy()
	 */
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onBackPressed()
	 */
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		FingerprintManager.getInstance().closeSerialPort();
		KJActivityManager.create().AppExit(LoginActivity.this);
		super.onBackPressed();
	}
	
}
