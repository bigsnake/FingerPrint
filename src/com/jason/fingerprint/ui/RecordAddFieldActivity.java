/**
 * @Title: RecordAddFieldActivity.java
 * @Package: com.jason.fingerprint.ui
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月2日 上午3:55:09
 * @Version: V1.0
 */
package com.jason.fingerprint.ui;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.KJActivityManager;
import org.kymjs.aframe.ui.activity.BaseActivity;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.jason.fingerprint.R;

/**
 * @ClassName: RecordAddFieldActivity
 * @Description: 修改或者添加record某一个字段
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月2日 上午3:55:09
 */
public class RecordAddFieldActivity extends BaseActivity {
	
	@BindView(id = R.id.header_back,click = true)
	private Button mBackButton;
	@BindView(id = R.id.header_title)
	private TextView mTitleTextView;
	@BindView(id = R.id.header_complete,click = true)
	private Button mCompleteButton;
	@BindView(id = R.id.record_addfield_content)
	private EditText mContentEditText;
	
	private String mTitleName;
	private String mContent;

	@Override
	public void setRootView() {
		setContentView(R.layout.activity_record_addfied);
	}
	
	@Override
	protected void initWidget() {
		super.initWidget();
		mCompleteButton.setVisibility(View.VISIBLE);
	}
	
	@Override
	protected void initData() {
		super.initData();
		Intent intent = getIntent();
		if (intent != null) {
			mTitleName = intent.getStringExtra(RecordAddOrUpdateActivity.RECORD_STATE);
			mContent = intent.getStringExtra(RecordAddOrUpdateActivity.RECORD_INFO);
			mTitleTextView.setText(mTitleName);
			mContentEditText.setText(mContent);
		}
				
	}
	
	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.header_back:
			KJActivityManager.create().finishActivity(RecordAddFieldActivity.this);
			break;
		case R.id.header_complete:
			String content = mContentEditText.getText().toString();
			Intent data = new Intent();
			data.putExtra(RecordAddOrUpdateActivity.RECORD_ADD_FIELD_CONTENT, content);
			setResult(RESULT_OK, data);
			finish();
			break;

		default:
			break;
		}
	}

}
