
package com.jason.fingerprint.ui;

import java.util.List;

import org.kymjs.aframe.http.HttpCallBack;
import org.kymjs.aframe.http.KJFileParams;
import org.kymjs.aframe.http.KJHttp;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.jason.fingerprint.R;
import com.jason.fingerprint.configs.HttpConfig;
import com.jason.fingerprint.utils.CodecUtils;
/**
 * @ClassName: MainActivity
 * @Description: TODO
 * @Author: Administrator zhangyujn1989ok@gmail.com
 * @Date: 2014年10月18日 上午2:38:18
 */
public class MainActivity extends BaseActivity {
	
	private static final String TAG = "MainActivity";
	@BindView(id = R.id.login,click = true)
	private Button loginButton;
	@BindView(id = R.id.result)
	private TextView showTextView;
	@BindView(id = R.id.et_api)
	private EditText editApi;
	
	@Override
	public void setRootView() {
		setContentView(R.layout.activity_main);
	}
	
	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.login:
			testLogin();
			break;

		default:
			break;
		}
	}
	
	public void testLogin(){
		String url = HttpConfig.POST_URL_DATB_LOGIN;
		int id = Integer.valueOf(editApi.getText().toString());
		KJFileParams params = new KJFileParams();
		params.put("imie", "860312023914656");
		switch (id) {
		case 0:
			Log.i(TAG, "----->" + "登录");
			params.put("type", "2");
			params.put("loginName", "gpsf");
			params.put("loginPwd", "111");
			break;
		case 1:
			Log.i(TAG, "----->" + "档案同步-->获取某机构下矫正人员的信息");
			params.put("currentIndex", "1");
			params.put("machId", "100000");
			url = HttpConfig.POST_URL_DATB_RECORDSYNC;
			break;
		case 2:
			Log.i(TAG, "----->" + "社区服务教育-->获取当天的活动");
			params.put("time", "2014-10-1-19");
			params.put("machId", "100000");
			params.put("type", "0");
			url = HttpConfig.POST_URL_EDUCOM_GETACTIVITYLIST;
			break;
		case 3:
			Log.i(TAG, "----->" + "社区服务教育-->新增集中教育、集中劳动、个别教育、个别劳动签到记录");
			params.put("time", "2014-10-1-19");
			params.put("machId", "100000");
			params.put("type", "0");
			url = HttpConfig.POST_URL_EDUCOM_ADDCOMEDULIST;
			break;

		default:
			break;
		}
		Log.i(TAG, "----->API:" + url);
		KJHttp kjHttp = new KJHttp();
		kjHttp.post(url, params, new HttpCallBack() {
			
			@Override
			public void onSuccess(Object arg0) {
				String result = CodecUtils.base64Decode(arg0.toString());
				Log.i(TAG, "----->Result:" + result);
				showTextView.setText(result);
				
			}
			
			@Override
			public void onLoading(long arg0, long arg1) {
				Log.i(TAG, "----->Result:arg0:" + arg0 + ",arg1:" + arg1);
				showTextView.setText("arg0:" + arg0 + ",arg1:" + arg1);
				
			}
			
			@Override
			public void onFailure(Throwable arg0, int arg1, String arg2) {
				showTextView.setText("arg1:" + arg1 + ",arg2:" + arg2);
			}
		});
	}
	
	public class MyClass{
		
		private int myClassId;
		
		private String myClassName;
		
		public MyClass() {
			super();
		}

		public MyClass(int myClassId, String myClassName) {
			super();
			this.myClassId = myClassId;
			this.myClassName = myClassName;
		}

		/**
		 * @return the myClassId
		 */
		public int getMyClassId() {
			return myClassId;
		}

		/**
		 * @param myClassId the myClassId to set
		 */
		public void setMyClassId(int myClassId) {
			this.myClassId = myClassId;
		}

		/**
		 * @return the myClassName
		 */
		public String getMyClassName() {
			return myClassName;
		}

		/**
		 * @param myClassName the myClassName to set
		 */
		public void setMyClassName(String myClassName) {
			this.myClassName = myClassName;
		}
		
		
		
	}
	
	public class Grade{
		
		private int gradeId;
		
		private List<MyClass> lists;
		

		/**
		 * @Descripton: TODO
		 * @Param 
		 * @Return $
		 * @Throws 
		 */
		public Grade() {
			super();
		}

		/**
		 * @Descripton: TODO
		 * @Param @param gradeId
		 * @Param @param lists
		 * @Return $
		 * @Throws 
		 */
		public Grade(int gradeId, List<MyClass> lists) {
			super();
			this.gradeId = gradeId;
			this.lists = lists;
		}

		/**
		 * @return the gradeId
		 */
		public int getGradeId() {
			return gradeId;
		}

		/**
		 * @param gradeId the gradeId to set
		 */
		public void setGradeId(int gradeId) {
			this.gradeId = gradeId;
		}

		/**
		 * @return the lists
		 */
		public List<MyClass> getLists() {
			return lists;
		}

		/**
		 * @param lists the lists to set
		 */
		public void setLists(List<MyClass> lists) {
			this.lists = lists;
		}
		
		
	}
	


	

}
