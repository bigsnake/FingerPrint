/**
 * @Title: WorkVisitContentActivity.java
 * @Package: com.jason.fingerprint.ui
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月26日 下午10:32:19
 * @Version: V1.0
 */
package com.jason.fingerprint.ui;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.KJActivityManager;
import org.kymjs.aframe.ui.activity.BaseActivity;
import org.kymjs.aframe.utils.StringUtils;

import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.jason.fingerprint.R;
import com.jason.fingerprint.beans.WorkVisitBean;

/**
 * @ClassName: WorkVisitContentActivity
 * @Description: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月26日 下午10:32:19
 */
public class WorkVisitContentActivity extends BaseActivity {
	
	public static final String WORK_VISIT_CONTENT_KEY = "work_visit_content_key";
	public static final String WORK_VISIT_STATE_KEY = "work_visit_state_key";
	public static final String WORK_VISIT_KEY = "work_visit_key";
	
	@BindView(id = R.id.header_back, click = true)
	private Button mBackButton;
	@BindView(id = R.id.header_title)
	private TextView mTitleTextView;
	@BindView(id = R.id.header_complete, click = true)
	private Button mCompleteButton;
	@BindView(id = R.id.content)
	private EditText mContentEditText;
	
	private String mContent;
	private int mState;//是否上传 1，没有上传 2已经上传
	private WorkVisitBean mWorkVisitBean;

	/* (non-Javadoc)
	 * @see org.kymjs.aframe.ui.activity.I_KJActivity#setRootView()
	 */
	@Override
	public void setRootView() {
		setContentView(R.layout.activity_work_visit_content);
	}
	
	/* (non-Javadoc)
	 * @see org.kymjs.aframe.ui.activity.KJFrameActivity#initWidget()
	 */
	@Override
	protected void initWidget() {
		super.initWidget();
		mCompleteButton.setVisibility(View.VISIBLE);
	}
	
	/* (non-Javadoc)
	 * @see org.kymjs.aframe.ui.activity.KJFrameActivity#initData()
	 */
	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		super.initData();
		Intent intent = getIntent();
		if (intent != null) {
			mWorkVisitBean = (WorkVisitBean) intent.getSerializableExtra(WORK_VISIT_KEY);
			if (mWorkVisitBean != null) {
				mContent = mWorkVisitBean.getInterViewWqk();
				mState = mWorkVisitBean.getState();
				if (!StringUtils.isEmpty(mContent)) {
					mContentEditText.setText(mContent);
				}else {
					mContentEditText.setText("遵守规定");
				}
				if (mState == 2) {
					//禁止编辑
					mContentEditText.setEnabled(false);
				}
			}
		}
		
		mContentEditText.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				if (s != null && s.length() > 0) {
					mCompleteButton.setClickable(true);
				}else {
					mCompleteButton.setClickable(false);
				}
			}
		});
	}
	
	/* (non-Javadoc)
	 * @see org.kymjs.aframe.ui.activity.KJFrameActivity#widgetClick(android.view.View)
	 */
	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.header_back:
			KJActivityManager.create().finishActivity(WorkVisitContentActivity.this);
			break;
		case R.id.header_complete:
			Intent intent = new Intent();
			intent.putExtra(WORK_VISIT_CONTENT_KEY, mContentEditText.getText().toString());
			setResult(RESULT_OK, intent);
			break;

		default:
			break;
		}
	}

}
