/**
 * @Title: PreviewPhotoActivity.java
 * @Package: com.jason.fingerprint.ui
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月22日 下午9:29:16
 * @Version: V1.0
 */
package com.jason.fingerprint.ui;

import java.util.ArrayList;
import java.util.List;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.KJActivityManager;
import org.kymjs.aframe.ui.activity.BaseActivity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.jason.fingerprint.AppContext;
import com.jason.fingerprint.R;
import com.jason.fingerprint.adapter.PreviewPagerAdapter;
import com.jason.fingerprint.widget.PreviewPhotoViewPager;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * @ClassName: PreviewPhotoActivity
 * @Description: 图片查看器
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月22日 下午9:29:16
 */
public class PreviewPhotoActivity extends BaseActivity {
	
private static final String ISLOCKED_ARG = "isLocked";
	
	private PreviewPhotoViewPager mPreviewViewPager;
//	private DisplayImageOptions mDisplayImageOptions;
	private PreviewPagerAdapter mPreviewPagerAdapter;
	private AppContext mAppContext;
	private String[] mImages;
	private List<String> list;
	
	private List<String> mCacheCheckImages;
	@BindView(id = R.id.header_back,click = true)
	private Button mBackButton;
	@BindView(id = R.id.header_complete,click = true)
	private Button mCompleteButton;
	@BindView(id = R.id.header_title)
	private TextView mTitleTextView;

	
	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if (savedInstanceState != null) {
			boolean isLocked = savedInstanceState
					.getBoolean(ISLOCKED_ARG, true);
			((PreviewPhotoViewPager) mPreviewViewPager).setLocked(isLocked);
		}
	}
	
	@Override
	public void setRootView() {
		setContentView(R.layout.activity_preview_pager);
	}
	
	@Override
	protected void initWidget() {
		super.initWidget();
	}
	
	@Override
	protected void initData() {
		super.initData();
		init();
	}
	
	private void init(){
		list = getIntent().getStringArrayListExtra("images");
		mCacheCheckImages = getIntent().getStringArrayListExtra("mCacheCheckImages");
		int position = getIntent().getIntExtra("position", 0);
		mAppContext = (AppContext) getApplication();
		mPreviewViewPager = (PreviewPhotoViewPager) findViewById(R.id.preview_pager);
		//getResources().getStringArray(R.array.light_images);
		mImages = getImages(list);
//		mDisplayImageOptions = AppContext.getInstance().getDisplayImageOptions();
		mPreviewPagerAdapter = new PreviewPagerAdapter(this, mImages);
		mPreviewViewPager.setAdapter(mPreviewPagerAdapter);
		mPreviewViewPager.setCurrentItem(position);
		mPreviewViewPager.setOnPageChangeListener(new MyOnPageChangeListener());
		updateView(list);
	}
	
	public void updateView(List<String> images){
		int curr = mPreviewViewPager.getCurrentItem();
		mTitleTextView.setText((curr+1) + "/" + list.size());
	}
	
	private String [] getImages(List<String> list){
		if (list == null || list.isEmpty() || list.size() < 1) {
			return null;
		}
		String [] image = new String[list.size()];
		return list.toArray(image);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		if (isViewPagerActive()) {
			outState.putBoolean(ISLOCKED_ARG,
					((PreviewPhotoViewPager) mPreviewViewPager).isLocked());
		}
		super.onSaveInstanceState(outState);
	}

	private boolean isViewPagerActive() {
		return (mPreviewViewPager != null && mPreviewViewPager instanceof PreviewPhotoViewPager);
	}
	
	private class MyOnPageChangeListener implements OnPageChangeListener{

		@Override
		public void onPageScrollStateChanged(int arg0) {
			updateView(list);
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			
		}

		@Override
		public void onPageSelected(int arg0) {
			
		}
		
	}
	
	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.header_back:
			finishActivity();
			break;
		case R.id.header_complete:
			
			break;

		default:
			break;
		}
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		finishActivity();
	}
	
	public void chooseImage(){
		int curr = mPreviewViewPager.getCurrentItem();
		String imagePath = list.get(curr);
		if (mCacheCheckImages == null) {
			mCacheCheckImages = new ArrayList<String>();
		}
		if (mCacheCheckImages.size() > 4){
			if (!isCheck(imagePath)) {
				Toast.makeText(this, "不能超" + mCacheCheckImages.size() + "个", Toast.LENGTH_SHORT).show();
			}else {
				mCacheCheckImages.remove(imagePath);
			}
			//mChooseImageView.setBackgroundResource(R.drawable.friends_sends_pictures_select_icon_unselected);
			updateCountTextView(false);
		}else{
			if (!isCheck(imagePath)) {
				mCacheCheckImages.add(imagePath);
				//mChooseImageView.setBackgroundResource(R.drawable.friends_sends_pictures_select_icon_selected);
				updateCountTextView(true);
			}else {
				mCacheCheckImages.remove(imagePath);
				//mChooseImageView.setBackgroundResource(R.drawable.friends_sends_pictures_select_icon_unselected);
				updateCountTextView(false);
			}
		}
		updateView(mCacheCheckImages);
	}
	
	private void updateCountTextView(boolean flag){
		Drawable drawable = getResources().getDrawable(R.drawable.friends_sends_pictures_select_icon_unselected);
		if (flag) {
			drawable = getResources().getDrawable(R.drawable.friends_sends_pictures_select_icon_selected);
		}
	}
	
	private boolean isCheck(String path){
		if (mCacheCheckImages == null || mCacheCheckImages.isEmpty() || mCacheCheckImages.size() < 1 ) {
			return false;
		}
		for (int i = 0; i < mCacheCheckImages.size(); i++) {
			if (mCacheCheckImages.get(i).equals(path)) {
				return true;
			}
		}
		return false;
	}
	
	
	private void finishActivity(){
		KJActivityManager.create().finishActivity(PreviewPhotoActivity.this);
	}

}
