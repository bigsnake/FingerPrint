/**
 * @Title: LeaveOutRecordActivity.java
 * @Package: com.jason.fingerprint.ui
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月21日 上午10:34:31
 * @Version: V1.0
 */
package com.jason.fingerprint.ui;

import java.util.ArrayList;
import java.util.List;

import org.kymjs.aframe.database.KJDB;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.KJActivityManager;
import org.kymjs.aframe.ui.activity.BaseActivity;
import org.kymjs.aframe.utils.StringUtils;

import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jason.fingerprint.AppContext;
import com.jason.fingerprint.R;
import com.jason.fingerprint.adapter.LeaveOutRecordAdapter;
import com.jason.fingerprint.beans.LeaveOutBean;
import com.jason.fingerprint.beans.ui.ImageOrAudioBean;

/**
 * @ClassName: LeaveOutRecordActivity
 * @Description: 请销假记录
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月21日 上午10:34:31
 */
public class LeaveOutRecordActivity extends BaseActivity implements
		OnItemClickListener {
	
	private static final String TAG = "LeaveOutRecordActivity";

	@BindView(id = R.id.header_back, click = true)
	private Button mBackButton;
	@BindView(id = R.id.header_title)
	private TextView mTitleTextView;
	@BindView(id = R.id.leave_out_record_listview)
	private ListView mListView;

	private AppContext mAppContext;
	private LeaveOutRecordAdapter mLeaveOutRecordAdapter;
	private KJDB mKjdb;
	private List<LeaveOutBean> mLeaveOutBeans = new ArrayList<LeaveOutBean>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.kymjs.aframe.ui.activity.I_KJActivity#setRootView()
	 */
	@Override
	public void setRootView() {
		setContentView(R.layout.activity_leave_out_record);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.kymjs.aframe.ui.activity.KJFrameActivity#initWidget()
	 */
	@Override
	protected void initWidget() {
		// TODO Auto-generated method stub
		super.initWidget();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.kymjs.aframe.ui.activity.KJFrameActivity#initData()
	 */
	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		super.initData();
		mAppContext = (AppContext) getApplication();
		mKjdb = mAppContext.getKjdb();
		mTitleTextView.setText("请销假记录");
		mLeaveOutRecordAdapter = new LeaveOutRecordAdapter(this);
		mListView.setAdapter(mLeaveOutRecordAdapter);
		mListView.setOnItemClickListener(this);
	}

	// 更新数据
	private Handler mUpdateDataHandle = new Handler() {

		@Override
		public void handleMessage(android.os.Message msg) {

			switch (msg.what) {
			case 100:
				new Thread(new Runnable() {

					@Override
					public void run() {
						try {
							mLeaveOutBeans = mKjdb
									.findDbModelListBySQL(LeaveOutBean.class,
											"select * from table_leaveout where state = 1 order by leaveoutId desc");
							mLeaveOutRecordAdapter.addAll(mLeaveOutBeans);
						} catch (Exception e) {
							Toast.makeText(LeaveOutRecordActivity.this, "没有数据",
									Toast.LENGTH_SHORT).show();
						}

					}
				}).run();
				break;

			default:
				break;
			}
		};

	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.kymjs.aframe.ui.activity.BaseActivity#onResume()
	 */
	@Override
	protected void onResume() {
		super.onResume();
		mUpdateDataHandle.sendEmptyMessage(100);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.kymjs.aframe.ui.activity.KJFrameActivity#widgetClick(android.view
	 * .View)
	 */
	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.header_back:
			KJActivityManager.create().finishActivity(this);
			break;

		default:
			break;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.widget.AdapterView.OnItemClickListener#onItemClick(android.widget
	 * .AdapterView, android.view.View, int, long)
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		LeaveOutBean bean = (LeaveOutBean) parent.getItemAtPosition(position);
		if (bean != null) {
			Log.i(TAG, bean.toString());
			List<ImageOrAudioBean> beans = mKjdb.findAllByWhere(ImageOrAudioBean.class,
					"serviceId = 5 and parentId = '" + bean.getId()
							+ "' and signType = '" + bean.getSignType() + "'");
			if (beans != null && !beans.isEmpty()) {
				ImageOrAudioBean audioBean = beans.get(0);
				Log.i(TAG, audioBean.toString());
				if (audioBean != null) {
					String path = audioBean.getFilePath();
					List<String> mCacheCheckImages = new ArrayList<String>();
					if (!StringUtils.isEmpty(path)) {
						mCacheCheckImages.add(path);
						Intent mIntent = new Intent(this, PreviewPhotoActivity.class);
						mIntent.putStringArrayListExtra("images", (ArrayList<String>) mCacheCheckImages);
						mIntent.putExtra("position", 0);
						startActivity(mIntent);
						return;
					}
				}
			}
		}
		Toast.makeText(LeaveOutRecordActivity.this, "没有图片",
				Toast.LENGTH_SHORT).show();
	}
}
