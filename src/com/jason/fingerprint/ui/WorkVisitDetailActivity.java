/**
 * @Title: WorkVisitDetailActivity.java
 * @Package: com.jason.fingerprint.ui
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月25日 下午11:15:37
 * @Version: V1.0
 */
package com.jason.fingerprint.ui;

import java.util.ArrayList;
import java.util.List;

import org.kymjs.aframe.database.KJDB;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.KJActivityManager;
import org.kymjs.aframe.ui.activity.BaseActivity;
import org.kymjs.aframe.utils.StringUtils;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jason.fingerprint.AppContext;
import com.jason.fingerprint.R;
import com.jason.fingerprint.beans.WorkVisitBean;
import com.jason.fingerprint.beans.ui.ImageOrAudioBean;
import com.jason.fingerprint.common.DateFormat;
import com.jason.fingerprint.utils.DataUtils;

/**
 * @ClassName: WorkVisitDetailActivity
 * @Description: 工作走访详情
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月25日 下午11:15:37
 */
public class WorkVisitDetailActivity extends BaseActivity {

	private static final String TAG = "WorkVisitDetailActivity";
	@BindView(id = R.id.header_back, click = true)
	private Button mBackButton;
	@BindView(id = R.id.header_title)
	private TextView mTitleTextView;

	@BindView(id = R.id.tv_username)
	private TextView mUserNameTextView;
	@BindView(id = R.id.tv_rymcname)
	private TextView mRyncNameTextView;
	@BindView(id = R.id.tv_address)
	private TextView mAddressTextView;
	@BindView(id = R.id.tv_remark)
	private TextView mContentTextView;
	@BindView(id = R.id.tv_time)
	private TextView mTimeTextView;
	@BindView(id = R.id.tv_photo)
	private TextView mPhotoTextView;
	@BindView(id = R.id.tv_recording)
	private TextView mRecordingTextView;

	@BindView(id = R.id.recording_layout, click = true)
	private RelativeLayout mRecordingLayout;
	@BindView(id = R.id.photo_layout, click = true)
	private RelativeLayout mPhotoLayout;
	@BindView(id = R.id.remark_layout, click = true)
	private RelativeLayout mRemarkLayout;

	private WorkVisitBean mWorkVisitBean;
	private AppContext mAppContext;
	private Context mContext;
	private KJDB mKjdb;
	private List<ImageOrAudioBean> mImageBeans;
	private List<ImageOrAudioBean> mAudioBeans;
	private int mImageSize = 0;
	private int mAudioSize = 0;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.kymjs.aframe.ui.activity.I_KJActivity#setRootView()
	 */
	@Override
	public void setRootView() {
		setContentView(R.layout.activity_work_visit_detail);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.kymjs.aframe.ui.activity.KJFrameActivity#initWidget()
	 */
	@Override
	protected void initWidget() {
		super.initWidget();
		mAppContext = (AppContext) getApplication();
		mContext = getApplicationContext();
		mKjdb = mAppContext.getKjdb();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.kymjs.aframe.ui.activity.KJFrameActivity#initData()
	 */
	@Override
	protected void initData() {
		super.initData();
		Intent intent = getIntent();
		mTitleTextView.setText("走访记录详情");
		if (intent != null) {
			mWorkVisitBean = (WorkVisitBean) intent
					.getSerializableExtra("WorkVisitBean");
			Log.i(TAG, "------->initData " + mWorkVisitBean);
			if (mWorkVisitBean != null) {
				mTitleTextView.setText(mWorkVisitBean.getRymcName());
				mRyncNameTextView.setText(mWorkVisitBean.getRymcName());
				mUserNameTextView.setText(mWorkVisitBean.getUserName());

				mTimeTextView.setText(DataUtils.converDatLongToString(
						mWorkVisitBean.getInterViewTime(),
						DateFormat.DATE_YMD_HMS));
				mContentTextView.setText(mWorkVisitBean.getInterViewWqk());
				mAddressTextView.setText(mWorkVisitBean.getInterViewAddress());
				try {
					mImageBeans = mKjdb.findAllByWhere(
							ImageOrAudioBean.class,
							"serviceId = 4 and parentId = '"
									+ mWorkVisitBean.getId()
									+ "' and fileType = 1");
				} catch (Exception e) {
					
				} finally{
					if (mImageBeans != null && !mImageBeans.isEmpty()) {
						mImageSize = mImageBeans.size();
					}
				}
				try {
					mAudioBeans = mKjdb.findAllByWhere(
							ImageOrAudioBean.class,
							"serviceId = 4 and parentId = '"
									+ mWorkVisitBean.getId()
									+ "' and fileType = 2");
				} catch (Exception e) {
					
				} finally{
					if (mAudioBeans != null && !mAudioBeans.isEmpty()) {
						mAudioSize = mAudioBeans.size();
					}
				}
				if (mImageSize > 0) {
					mPhotoTextView.setText("查看图片");
				} else {
					mPhotoTextView.setText("无");
				}
				if (mAudioSize > 0) {
					mRecordingTextView.setText("播放录音");
				} else {
					mRecordingTextView.setText("无");
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.kymjs.aframe.ui.activity.KJFrameActivity#widgetClick(android.view
	 * .View)
	 */
	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.header_back:
			KJActivityManager.create().finishActivity(
					WorkVisitDetailActivity.this);
			break;
		case R.id.recording_layout:
			// TODO 录音
			break;
		case R.id.photo_layout:
			// TODO 照片,待扩展，判断当前的图片是否为5张，是否已经上传
			if (mImageSize > 0) {
				ArrayList<String> images = new ArrayList<String>();
				for (int i = 0; i < mImageSize; i++) {
					ImageOrAudioBean bean = mImageBeans.get(i);
					if (bean != null && !StringUtils.isEmpty(bean.getFilePath())) {
						images.add(bean.getFilePath());
					}
				}
				if (images != null && !images.isEmpty()) {
					Intent mIntent = new Intent(WorkVisitDetailActivity.this,
							PreviewPhotoActivity.class);
					mIntent.putStringArrayListExtra("images", images);
					mIntent.putExtra("position", 0);
					startActivity(mIntent);
				}
			}else {
				//TODO 判断该记录是否已经上传，如果没有上传不足选取图片，
			}
			break;
		case R.id.remark_layout:
			// TODO 访问情况，只有在记录为上传的时候才能做修改
			if (mWorkVisitBean != null) {
				Intent intent = new Intent(WorkVisitDetailActivity.this,
						WorkVisitContentActivity.class);
				intent.putExtra(WorkVisitContentActivity.WORK_VISIT_KEY, mWorkVisitBean);
				startActivityForResult(intent, 1000);
			}
			break;

		default:
			break;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onActivityResult(int, int,
	 * android.content.Intent)
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 1000 && resultCode == RESULT_OK) {
			if (data != null) {
				String content = data
						.getStringExtra(WorkVisitContentActivity.WORK_VISIT_CONTENT_KEY);
				if (StringUtils.isEmpty(content)) {
					content = "遵守规定";
				}
				mContentTextView.setText(content);
				mWorkVisitBean.setInterViewWqk(content);
				mKjdb.update(mWorkVisitBean);
			}
		}
	}
}
