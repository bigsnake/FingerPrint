/**
 * @Title: DailyReportDetailActivity.java
 * @Package: com.jason.fingerprint.ui
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月13日 下午4:35:44
 * @Version: V1.0
 */
package com.jason.fingerprint.ui;

import java.util.ArrayList;
import java.util.List;

import org.kymjs.aframe.database.KJDB;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.KJActivityManager;
import org.kymjs.aframe.ui.activity.BaseActivity;
import org.kymjs.aframe.utils.StringUtils;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jason.fingerprint.AppContext;
import com.jason.fingerprint.R;
import com.jason.fingerprint.beans.DailyReportBean;
import com.jason.fingerprint.beans.ui.ImageOrAudioBean;
import com.jason.fingerprint.common.DateFormat;
import com.jason.fingerprint.utils.DataUtils;

/**
 * @ClassName: DailyReportDetailActivity
 * @Description: 日常签到详细信息
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月13日 下午4:35:44
 */
public class DailyReportDetailActivity extends BaseActivity {

	@BindView(id = R.id.header_back, click = true)
	private Button mBackButton;
	@BindView(id = R.id.header_title)
	private TextView mTitleTextView;

	@BindView(id = R.id.tv_id)
	private TextView mIdTextView;
	@BindView(id = R.id.tv_name)
	private TextView mNameTextView;
	@BindView(id = R.id.tv_begin_time)
	private TextView mBeginTimeTextView;
	@BindView(id = R.id.tv_end_time)
	private TextView mEndTimeTextView;
	@BindView(id = R.id.tv_update_time)
	private TextView mUpdateTimeTextView;
	@BindView(id = R.id.photo_layout, click = true)
	private RelativeLayout mPhotoLayout;
	@BindView(id = R.id.tv_photo)
	private TextView mPhotoTextView;

	private int mReportId;
	private List<ImageOrAudioBean> mImageOrAudioBean;
	private AppContext mAppContext;
	private KJDB mKjdb;
	private List<String> mCacheCheckImages = new ArrayList<String>();
	private boolean mIsShowPhoto = false;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.kymjs.aframe.ui.activity.I_KJActivity#setRootView()
	 */
	@Override
	public void setRootView() {
		// TODO Auto-generated method stub
		setContentView(R.layout.activity_daily_report_detail);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.kymjs.aframe.ui.activity.KJFrameActivity#initData()
	 */
	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		super.initData();
		mAppContext = (AppContext) getApplication();
		mKjdb = mAppContext.getKjdb();
		Intent intent = getIntent();
		if (intent != null) {
			DailyReportBean bean = (DailyReportBean) intent
					.getSerializableExtra("DailyReportBean");
			if (bean != null) {
				mReportId = bean.getId();
				mTitleTextView.setText(bean.getRymcName());
				mIdTextView.setText(bean.getRymcId());
				mNameTextView.setText(bean.getRymcName());
				mBeginTimeTextView.setText(DataUtils.converDatLongToString(
						bean.getStartTime() * 1000, DateFormat.DATE_YMD_HMS));
				mEndTimeTextView.setText(DataUtils.converDatLongToString(
						bean.getEndTime() * 1000, DateFormat.DATE_YMD_HMS));
				mUpdateTimeTextView.setText(DataUtils.converDatLongToString(
						bean.getUpdateTime() * 1000, DateFormat.DATE_YMD_HMS));
				try {
					mImageOrAudioBean = mKjdb.findAllByWhere(
							ImageOrAudioBean.class,
							"parentId = '" + mReportId + "' and serviceId = 1 and fileType = 1");
					if (mImageOrAudioBean != null && !mImageOrAudioBean.isEmpty()) {
						mIsShowPhoto = true;
						int count = mImageOrAudioBean.size();
						for (int i = 0; i < count; i++) {
							ImageOrAudioBean image = mImageOrAudioBean.get(i);
							if (image != null && !StringUtils.isEmpty(image.getFilePath())) {
								mCacheCheckImages.add(image.getFilePath());
							}
						}
					}
				} catch (Exception e) {
					
				}
				if (mIsShowPhoto) {
					mPhotoTextView.setText("查看图片");
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.kymjs.aframe.ui.activity.KJFrameActivity#widgetClick(android.view
	 * .View)
	 */
	@Override
	public void widgetClick(View v) {
		// TODO Auto-generated method stub
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.header_back:
			KJActivityManager.create().finishActivity(this);
			break;
		case R.id.photo_layout:
			if (mIsShowPhoto) {
				Intent mIntent = new Intent(this, PreviewPhotoActivity.class);
				mIntent.putStringArrayListExtra("images", (ArrayList<String>) mCacheCheckImages);
				mIntent.putExtra("position", 0);
				startActivity(mIntent);
			}else{
				Toast.makeText(DailyReportDetailActivity.this, "没有图片", Toast.LENGTH_SHORT).show();
			}
			break;

		default:
			break;
		}
	}

}
