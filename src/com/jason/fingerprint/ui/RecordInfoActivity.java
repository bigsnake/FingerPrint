/**
 * @Title: RecordInfoActivity.java
 * @Package: com.jason.fingerprint.ui
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月21日 下午9:18:22
 * @Version: V1.0
 */
package com.jason.fingerprint.ui;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.KJActivityManager;
import org.kymjs.aframe.ui.activity.BaseActivity;
import org.kymjs.aframe.utils.StringUtils;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.jason.fingerprint.R;
import com.jason.fingerprint.beans.RecordBean;

/**
 * @ClassName: RecordInfoActivity
 * @Description: 矫正人员的详细信息
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月21日 下午9:18:22
 */
public class RecordInfoActivity extends BaseActivity implements OnClickListener {

	@BindView(id = R.id.tv_name)
	private TextView mNameTextView;
	@BindView(id = R.id.tv_telephone)
	private TextView mTelephoneTextView;
	@BindView(id = R.id.tv_identityNO)
	private TextView mIdentityNOTextView;
	@BindView(id = R.id.tv_registeredId)
	private TextView mRegisteredIdTextView;
	@BindView(id = R.id.tv_registered)
	private TextView mRegisteredTextView;
	@BindView(id = R.id.tv_electronicsId)
	private TextView mElectronicsIdTextView;
	
	@BindView(id = R.id.tv_fp1)
	private TextView mFp1TextView;
	@BindView(id = R.id.tv_fp2)
	private TextView mFp2TextView;
	
	@BindView(id = R.id.tv_cardNo)
	private TextView mCardNoTextView;
	
	@BindView(id = R.id.header_back,click = true)
	private Button mBackButton;
	@BindView(id = R.id.header_title)
	private TextView mTitleTextView;
	
	@Override
	public void setRootView() {
		setContentView(R.layout.activity_record_info);
	}
	
	@Override
	protected void initData() {
		super.initData();
		initDatas();
	}
	
	private void initDatas(){
		Intent intent = getIntent();
		if(intent != null){
			RecordBean recordBean = (RecordBean) intent.getSerializableExtra("RecordBean");
			if (recordBean != null) {
				mTitleTextView.setText(recordBean.getName());
				mNameTextView.setText(recordBean.getName());
				mTelephoneTextView.setText(recordBean.getTelephone());
				mElectronicsIdTextView.setText(recordBean.getElectronicsId());
				mIdentityNOTextView.setText(recordBean.getIdentityNO());
				mRegisteredIdTextView.setText(recordBean.getRegisteredId());
				mRegisteredTextView.setText(recordBean.getRegistered());
				mCardNoTextView.setText(recordBean.getCardNO());
				if (StringUtils.isEmpty(recordBean.getFp_1())) {
					mFp1TextView.setText("未录");
				}else {
					mFp1TextView.setText("已录");
				}
				if (StringUtils.isEmpty(recordBean.getFp_2())) {
					mFp2TextView.setText("未录");
				}else {
					mFp2TextView.setText("已录");
				}
			}
		}
	}
	
	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.header_back:
			KJActivityManager.create().finishActivity(RecordInfoActivity.this);
			break;

		default:
			break;
		}
	}
	

}
