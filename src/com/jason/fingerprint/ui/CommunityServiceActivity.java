package com.jason.fingerprint.ui;

import java.util.ArrayList;
import java.util.List;

import org.kymjs.aframe.database.KJDB;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jason.fingerprint.AppContext;
import com.jason.fingerprint.R;
import com.jason.fingerprint.adapter.LaborAdapter;
import com.jason.fingerprint.beans.SponsorBean;
import com.jason.fingerprint.beans.SponsorInfo;
import com.jason.fingerprint.configs.DataConfig;
import com.jason.fingerprint.logic.HandleSponsorBeanLogic;

/**
 * 
 * @ClassName: CommunityServiceActivity
 * @Description: 该类为社区服务类、教育学习主入口
 * @author John
 * @date 2014/10/20
 * 
 */
public class CommunityServiceActivity extends Activity implements
		OnClickListener, OnItemClickListener {

	private static final String TAG = "CommunityServiceActivity";

	private AppContext mAppContext;

	private KJDB mKjdb;

	private HandleSponsorBeanLogic mHandleSponsorBeanLogic;

	private LaborAdapter mLaborAdapter;

	private List<SponsorInfo> mSponsorInfos = new ArrayList<SponsorInfo>();

	private ListView mLaborList;

	private Button mBackBtn;

	private boolean mIsCommunityService = true; // 判断是否为社区服务入口

	private TextView mIndividualView;

	private TextView mCentralView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mAppContext = (AppContext) getApplication();
		mKjdb = mAppContext.getKjdb();

		setContentView(R.layout.activity_communityservice);
		initView();
	}

	// 初始化数据
	private void initView() {
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			mIsCommunityService = extras.getBoolean("CommunityService", true);
		}

		mBackBtn = (Button) this.findViewById(R.id.header_back);
		mBackBtn.setOnClickListener(this);

		TextView titleView = (TextView) this.findViewById(R.id.header_title);
		mIndividualView = (TextView) this
				.findViewById(R.id.individual_service_title);
		mCentralView = (TextView) this.findViewById(R.id.central_service_title);
		if (mIsCommunityService) {
			// 社区服务
			titleView.setText(R.string.module_title_fuwu);
			mIndividualView.setText(R.string.individual_labor);
			mCentralView.setText(R.string.centralized_labor);
		} else {
			// 教育学习
			titleView.setText(R.string.module_title_xuexi);
			mIndividualView.setText(R.string.individual_study);
			mCentralView.setText(R.string.centralized_study);
		}

		View enterIndividual = this.findViewById(R.id.individual_enter);
		enterIndividual.setOnClickListener(this);

		String type = mIsCommunityService ? DataConfig.REGISTER_TYPE_1
				: DataConfig.REGISTER_TYPE_0;

		mLaborAdapter = new LaborAdapter(this);
		mLaborAdapter.setSponsorInfoList(mSponsorInfos);

		mLaborList = (ListView) this.findViewById(R.id.labor_list);
		mLaborList.setAdapter(mLaborAdapter);
		mLaborList.setOnItemClickListener(this);

		mHandleSponsorBeanLogic = new HandleSponsorBeanLogic(mAppContext,
				mSponsorBeanSyncHandler, true);
		mHandleSponsorBeanLogic.getTodayActivities(type);
	}

	// 档案同步
	private Handler mSponsorBeanSyncHandler = new Handler() {

		@Override
		public void handleMessage(android.os.Message msg) {
			switch (msg.arg1) {
			case 0:
				Toast.makeText(mAppContext, "同步成功", Toast.LENGTH_SHORT).show();
				List<SponsorBean> beans = (List<SponsorBean>) msg.obj;
				updateData(beans);
				break;
			case 1:
				// 数据访问正确，但没有任何数据，需要初始化一些数据
				Toast.makeText(mAppContext, "没有数据！", Toast.LENGTH_SHORT)
						.show();
				break;
			default:
				Toast.makeText(mAppContext, "获取数据失败！", Toast.LENGTH_SHORT)
				.show();
				break;
			}
		}
	};
	
	
	private void updateData(List<SponsorBean> list){
		if (list != null && !list.isEmpty()) {
			for (SponsorBean bean : list) {
				if (bean != null) {
					SponsorInfo info = new SponsorInfo(bean);
					mKjdb.save(info);
					mSponsorInfos.add(info);
				}
			}
			mLaborAdapter.setSponsorInfoList(mSponsorInfos);
			mLaborAdapter.notifyDataSetChanged();
		}
		
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.header_back:
			finish();
			break;
		case R.id.individual_enter:
			// 进入个别劳动界面
			Intent intent = new Intent(this, SignInDetailActivity.class);
			intent.putExtra("registerType",
					mIsCommunityService ? DataConfig.REGISTER_TYPE_3
							: DataConfig.REGISTER_TYPE_2);
			intent.putExtra("title", mIndividualView.getText().toString());
			startActivity(intent);
			break;
		}

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		SponsorInfo info = (SponsorInfo) parent.getItemAtPosition(position);
		if (info == null) {
			Toast.makeText(mAppContext, "数据错误！", Toast.LENGTH_SHORT).show();
			return;
		}
		// 点击进入详细活动界面
		Intent intent = new Intent(this, SignInDetailActivity.class);
		intent.putExtra("registerType", info.getType());
		intent.putExtra("SponsorInfo", info);
		intent.putExtra("title", mCentralView.getText().toString());
		startActivity(intent);
	}
}
