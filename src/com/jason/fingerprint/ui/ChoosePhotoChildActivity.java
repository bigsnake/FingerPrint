/**
 * @Title: ChoosePhotoChildActivity.java
 * @Package: com.jason.fingerprint.ui
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月26日 下午3:59:32
 * @Version: V1.0
 */
package com.jason.fingerprint.ui;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.jason.fingerprint.AppContext;
import com.jason.fingerprint.R;
import com.jason.fingerprint.adapter.ImageChildAdapter;

/**
 * @ClassName: ChoosePhotoChildActivity
 * @Description: 某一文件下的所有图片信息
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月26日 下午3:59:32
 */
public class ChoosePhotoChildActivity extends Activity implements
		OnItemClickListener, OnClickListener {

	private static final String TAG = "ChoosePhotoChildActivity";
	public static final int REQUEST_OK = 1000;
	public static final int RESULT_OK = 1001;
	private GridView mGridView;
	private List<String> list;
	private ImageChildAdapter adapter;
	private AppContext mAppContext;
	private List<String> mCacheCheckImages;
	private String mFolderName;

	private Button mBackButton;
	private Button mCompleteButton;
	private TextView mPreviewTextView;
	private TextView mTitleTextView;
	private static int mChooseCount = 5;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_image_child);
		init();
	}

	private void init() {
		mAppContext = (AppContext) getApplication();
		mGridView = (GridView) findViewById(R.id.child_grid);
		Intent intent = getIntent();
		if (intent != null) {
			list = intent.getStringArrayListExtra("data");
			mCacheCheckImages = intent
					.getStringArrayListExtra("mCacheCheckImages");
			mFolderName = intent.getStringExtra("folderName");
			mChooseCount = intent.getIntExtra(
					ChoosePhotoActivity.CHOOSE_PHOTO_COUNT, 5);
		}

		adapter = new ImageChildAdapter(this, list, mGridView,
				mCacheCheckImages,mChooseCount);
		mGridView.setAdapter(adapter);
		mGridView.setOnItemClickListener(this);

		mBackButton = (Button) findViewById(R.id.header_back);
		mCompleteButton = (Button) findViewById(R.id.header_complete);
		mPreviewTextView = (TextView) findViewById(R.id.footer_preview);
		mTitleTextView = (TextView) findViewById(R.id.header_title);

		mBackButton.setOnClickListener(this);
		mCompleteButton.setOnClickListener(this);
		mPreviewTextView.setOnClickListener(this);
		mTitleTextView.setText(mFolderName);
		updateViewData(mCacheCheckImages);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public void onBackPressed() {
		backChoosePhotoAtivity();
		super.onBackPressed();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.i(TAG, "onDestroy");
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}

	public void backChoosePhotoAtivity() {
		Intent mIntent = new Intent();
		mIntent.putStringArrayListExtra("mCacheCheckImages",
				(ArrayList<String>) mCacheCheckImages);
		setResult(0, mIntent);
	}

	public void finishPhotoActivity(){
		new AlertDialog.Builder(this).setTitle("选取照片").setMessage("是否结束选取照片？")
				.setCancelable(true)
				.setPositiveButton("确定", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						ChoosePhotoActivity.mCacheCheckImages = mCacheCheckImages;
						ChoosePhotoActivity.mIsOne = true;
						finish();
//						Intent mIntent = new Intent();
//						mIntent.putStringArrayListExtra("mCacheCheckImages",
//								(ArrayList<String>) mCacheCheckImages);
//						mIntent.putExtra("isOne",true);
//						setResult(0, mIntent);
					}

				}).setNegativeButton("取消", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {

				dialog.dismiss();
			}

		}).create().show();
	}

	public void updateViewData(List<String> images) {
		mCacheCheckImages = images;
		if (images == null || images.isEmpty()) {
			mCompleteButton.setEnabled(false);
			mPreviewTextView.setEnabled(false);
			mCompleteButton.setClickable(false);
			mPreviewTextView.setClickable(false);
			mCompleteButton.setText("完成");
			mPreviewTextView.setText("预览");
			mPreviewTextView.setTextColor(getResources().getColor(
					R.color.photo_font_disable_color));
		} else {
			int count = images.size();
			mCompleteButton.setEnabled(true);
			mPreviewTextView.setEnabled(true);
			mCompleteButton.setClickable(true);
			mPreviewTextView.setClickable(true);
			mCompleteButton.setText("完成(" + count + ")");
			mPreviewTextView.setText("预览(" + count + ")");
			mPreviewTextView.setTextColor(getResources().getColor(
					R.color.photo_font_normal_color));
		}

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		startPreviewPhotoActivity(list, 0);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.header_back:
			if (mChooseCount == 1){
				finishPhotoActivity();
			}else {
				backChoosePhotoAtivity();
				finish();
			}
			break;
		case R.id.header_complete:

			break;
		case R.id.footer_takephoto:

			break;
		case R.id.footer_preview:
			startPreviewPhotoActivity(mCacheCheckImages, 0);
			break;

		default:
			break;
		}

	}

	private void startPreviewPhotoActivity(List<String> iamges, int position) {
		Intent mIntent = new Intent(ChoosePhotoChildActivity.this,
				PreviewPhotoActivity.class);
		mIntent.putStringArrayListExtra("images", (ArrayList<String>) iamges);
		mIntent.putExtra("position", position);
		startActivity(mIntent);
	}

}
