/**
 * @Title: WorkNotifyActivity.java
 * @Package: com.jason.fingerprint.ui
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月2日 上午9:02:44
 * @Version: V1.0
 */
package com.jason.fingerprint.ui;

import java.util.List;

import org.kymjs.aframe.database.KJDB;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.KJActivityManager;
import org.kymjs.aframe.ui.activity.BaseActivity;
import org.kymjs.aframe.utils.StringUtils;

import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jason.fingerprint.AppContext;
import com.jason.fingerprint.R;
import com.jason.fingerprint.adapter.WorkNotifyAdapter;
import com.jason.fingerprint.beans.WorkNotifyBean;
import com.jason.fingerprint.logic.WorkNotifySyncLogic;

/**
 * @ClassName: WorkNotifyActivity
 * @Description: 工作通知
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月2日 上午9:02:44
 */
public class WorkNotifyActivity extends BaseActivity implements OnItemClickListener {
	
	private static final String TAG = "WorkNotifyActivity";

	@BindView(id = R.id.header_back, click = true)
	private Button mBackButton;
	@BindView(id = R.id.header_title)
	private TextView mTitleTextView;
	@BindView(id = R.id.notify_listview)
	private ListView mListView;

	private KJDB mKjdb;
	private AppContext mAppContext;
	private WorkNotifyAdapter mWorkNotifyAdapter;

	private List<WorkNotifyBean> mWorkNotifyBeans;

	@Override
	public void setRootView() {
		setContentView(R.layout.activity_work_notify);
	}

	@Override
	protected void initWidget() {
		// TODO Auto-generated method stub
		super.initWidget();
		mTitleTextView.setText("工作通知");
		mAppContext = (AppContext) getApplication();
		mKjdb = mAppContext.getKjdb();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.kymjs.aframe.ui.activity.KJFrameActivity#initData()
	 */
	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		super.initData();
		mWorkNotifyAdapter = new WorkNotifyAdapter(this);
		mListView.setAdapter(mWorkNotifyAdapter);
		mListView.setOnItemClickListener(this);
		mKjdb.deleteByWhere(WorkNotifyBean.class, "");
		WorkNotifySyncLogic logic = new WorkNotifySyncLogic(mAppContext,
				mWorkNotifyHandler, true);
		logic.execute();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.kymjs.aframe.ui.activity.KJFrameActivity#widgetClick(android.view
	 * .View)
	 */
	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.header_back:
			KJActivityManager.create().finishActivity(this);
			break;

		default:
			break;
		}
	}

	private Handler mWorkNotifyHandler = new Handler() {

		@Override
		public void handleMessage(android.os.Message msg) {
			
			switch (msg.what) {
			case 0:
				// TODO 成功
				mWorkNotifyBeans = mKjdb.findAll(WorkNotifyBean.class);
				mWorkNotifyAdapter.addAll(mWorkNotifyBeans);
				mWorkNotifyAdapter.notifyDataSetChanged();
				break;
			case 1:
			case 2:
				Toast.makeText(WorkNotifyActivity.this, "当前没有数据",
						Toast.LENGTH_SHORT).show();
				break;

			default:
				break;
			}
		};

	};

	/* (non-Javadoc)
	 * @see android.widget.AdapterView.OnItemClickListener#onItemClick(android.widget.AdapterView, android.view.View, int, long)
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		WorkNotifyBean bean = (WorkNotifyBean) parent.getItemAtPosition(position);
		if (bean != null && !StringUtils.isEmpty(bean.getContentUrl())) {
			Intent intent = new Intent(WorkNotifyActivity.this,BrowserActivity.class);
			intent.putExtra(BrowserActivity.BROWSER_URL, bean.getContentUrl());
			startActivity(intent);
		}
	}

}
