/**
 * @Title: SettingActivity.java
 * @Package: com.jason.fingerprint.ui
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月1日 下午4:28:37
 * @Version: V1.0
 */
package com.jason.fingerprint.ui;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.KJActivityManager;
import org.kymjs.aframe.ui.activity.BaseActivity;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jason.fingerprint.R;
import com.jason.fingerprint.common.DialogHelper;

/**
 * @ClassName: SettingActivity
 * @Description: 关于我们
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月1日 下午4:28:37
 */
public class AboutActivity extends BaseActivity {
	
	@BindView(id = R.id.header_back,click = true)
	private Button mBackButton;
	@BindView(id = R.id.header_title)
	private TextView mTitleTextView;
	
	@BindView(id = R.id.about_phone_content_textview,click = true)
	private TextView mPhoneTextView;
	@BindView(id = R.id.about_website_content_textview,click = true)
	private TextView mWebsiteTextView;

	@Override
	public void setRootView() {
		setContentView(R.layout.activity_about);
	}
	
	@Override
	protected void initWidget() {
		super.initWidget();
		mTitleTextView.setVisibility(View.VISIBLE);
		mTitleTextView.setText("关于我们");
	}
	
	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.header_back:
			KJActivityManager.create().finishActivity(AboutActivity.class);
			break;
		case R.id.about_phone_content_textview:
			DialogHelper.openCallDialer(AboutActivity.this,"025-68126557");
			break;
		case R.id.about_website_content_textview:
			Intent it = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.hzlj.net/"));  
			startActivity(it);
			break;

		default:
			break;
		}
	}

}
