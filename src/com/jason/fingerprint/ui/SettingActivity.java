/**
 * @Title: SettingActivity.java
 * @Package: com.jason.fingerprint.ui
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月1日 下午4:28:37
 * @Version: V1.0
 */
package com.jason.fingerprint.ui;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.KJActivityManager;
import org.kymjs.aframe.ui.activity.BaseActivity;

import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jason.fingerprint.R;
import com.jason.fingerprint.common.DialogHelper;

/**
 * @ClassName: SettingActivity
 * @Description: 系统设置
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月1日 下午4:28:37
 */
public class SettingActivity extends BaseActivity {
	
	@BindView(id = R.id.header_back,click = true)
	private Button mBackButton;
	@BindView(id = R.id.header_title)
	private TextView mTitleTextView;
	
	@BindView(id = R.id.setting_layout_help,click = true)
	private RelativeLayout mHelpRelativeLayout;
	
	@BindView(id = R.id.setting_layout_feedback,click = true)
	private RelativeLayout mFeedbackRelativeLayout;
	
	@BindView(id = R.id.setting_layout_comment,click = true)
	private RelativeLayout mCommentRelativeLayout;
	
	@BindView(id = R.id.setting_layout_update,click = true)
	private RelativeLayout mUpdateRelativeLayout;
	
	@BindView(id = R.id.setting_layout_wifi,click = true)
	private RelativeLayout mWifiRelativeLayout;

	@Override
	public void setRootView() {
		setContentView(R.layout.activity_setting);
	}
	
	@Override
	protected void initWidget() {
		super.initWidget();
		mTitleTextView.setVisibility(View.VISIBLE);
		mTitleTextView.setText("系统配置");
	}
	
	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.header_back:
			KJActivityManager.create().finishActivity(SettingActivity.class);
			break;
		case R.id.setting_layout_help:
			break;
		case R.id.setting_layout_feedback:
			break;
		case R.id.setting_layout_comment:
			break;
		case R.id.setting_layout_update:
			checkVersion();
			break;
		case R.id.setting_layout_wifi:
			DialogHelper.openNetSettingsDialog(SettingActivity.this);
			break;

		default:
			break;
		}
	}
	
	private void checkVersion(){
		Toast.makeText(getApplicationContext(), "当前已经是最新版本", Toast.LENGTH_SHORT).show();
	}

}
