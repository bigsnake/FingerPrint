/**
 * @Title: WorkNotifyactivity.java
 * @Package: com.jason.fingerprint.ui
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月2日 上午9:02:44
 * @Version: V1.0
 */
package com.jason.fingerprint.ui;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.KJActivityManager;
import org.kymjs.aframe.ui.activity.BaseActivity;

import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jason.fingerprint.AppContext;
import com.jason.fingerprint.R;
import com.jason.fingerprint.logic.WorkNotifySyncLogic;
import com.jason.fingerprint.logic.WorkRemindSyncLogic;

/**
 * @ClassName: WorkNotifyactivity
 * @Description: 工作通知
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月2日 上午9:02:44
 */
public class WorkRemindActivity extends BaseActivity {
	
	@BindView(id = R.id.header_back,click = true)
	private Button mBackButton;
	@BindView(id = R.id.header_title)
	private TextView mTitleTextView;
	private AppContext mAppContext; 

	@Override
	public void setRootView() {
		setContentView(R.layout.activity_work_remind);
	}
	
	@Override
	protected void initWidget() {
		// TODO Auto-generated method stub
		super.initWidget();
		mTitleTextView.setText("工作提醒");
		mAppContext = (AppContext) getApplication();
	}
	
	/* (non-Javadoc)
	 * @see org.kymjs.aframe.ui.activity.KJFrameActivity#initData()
	 */
	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		super.initData();
		WorkRemindSyncLogic logic = new WorkRemindSyncLogic(mAppContext, new Handler(), false);
		logic.execute();
	}
	
	/* (non-Javadoc)
	 * @see org.kymjs.aframe.ui.activity.KJFrameActivity#widgetClick(android.view.View)
	 */
	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.header_back:
			KJActivityManager.create().finishActivity(this);
			break;

		default:
			break;
		}
	}

}
