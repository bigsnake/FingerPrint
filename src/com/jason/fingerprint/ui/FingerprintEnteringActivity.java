/**
 * @Title: FingerprintEnteringActivity.java
 * @Package: com.jason.fingerprint.ui
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月24日 下午11:50:47
 * @Version: V1.0
 */
package com.jason.fingerprint.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.Toast;
import android_serialport_api.FingerprintManager;

import com.jason.fingerprint.AppContext;
import com.jason.fingerprint.R;
import com.jason.fingerprint.beans.UserBean;
import com.jason.fingerprint.common.UIHelper;
import com.jason.fingerprint.logic.RegisterFingerprintSyncLogic;
import com.jason.fingerprint.logic.ValidateFingerprintSyncLogic;
import com.jason.fingerprint.utils.CodecUtils;

/**
 * @ClassName: FingerprintEnteringActivity
 * @Description:  指纹录入
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月24日 下午11:50:47
 */
@SuppressLint("HandlerLeak")
public class FingerprintEnteringActivity extends Activity implements
		OnClickListener, OnCheckedChangeListener {

	private static final String TAG = "FingerprintEnteringActivity";

	private AppContext mAppContext;
	private CheckBox mCheckBox;
	private Button mInsertButton;
	private Button mCheckButton;
	private ImageView mImageView;
	private boolean mIsShowImage;
	private byte[] mModel;
	private String mFingerInfo;
	private UserBean mUserBean;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_finger_entering);
		initView();
		initData();
	}
	
	private void initView(){
		mAppContext = (AppContext) getApplication();
		mCheckBox = (CheckBox) findViewById(R.id.finger_entering_isimage_cbox);
		mInsertButton = (Button) findViewById(R.id.finger_entering_add_btn);
		mCheckButton = (Button) findViewById(R.id.finger_entering_check_btn);
		mImageView = (ImageView) findViewById(R.id.finger_entering_show_imageview);
		mInsertButton.setOnClickListener(this);
		mCheckButton.setOnClickListener(this);
		mCheckBox.setOnCheckedChangeListener(this);
	}
	
	//初始化数据
	private void initData(){
		mUserBean = mAppContext.getUserBean();
	}

	@Override
	public void onClick(View v) {
		Toast.makeText(FingerprintEnteringActivity.this, "请按手指",
				Toast.LENGTH_SHORT).show();
		switch (v.getId()) {
		case R.id.finger_entering_add_btn:
			RegisterFingerprintSyncLogic logic = new RegisterFingerprintSyncLogic(
					mAppContext, mFingerpringHandler, true, mIsShowImage);
			logic.execute();

			break;
		case R.id.finger_entering_check_btn:
			//mFingerInfo = CodecUtils.base64EncodeToString(mModel);
			mFingerInfo = mUserBean.getFingerprint1();
			mModel = CodecUtils.base64DecodeToByte(mFingerInfo);
			Log.i(TAG, "mFingerpringHandler result :" + mFingerInfo);
			break;

		default:
			break;
		}

	}

	private Handler mFingerpringHandler = new Handler() {

		String toast = "失败";

		@Override
		public void handleMessage(Message msg) {
			Bundle bundle = msg.getData();
			switch (msg.what) {
			case UIHelper.FINGERPRINT_UPCHAR_SUCCESS:
				// char--返回成功
				if (bundle != null) {
					mModel = bundle.getByteArray(UIHelper.FINGERPRINT_SOURCE);
				}
				toast = "文字生成成功";
				mFingerInfo = CodecUtils.base64EncodeToString(mModel);
				Log.i(TAG, "mFingerpringHandler result :" + mFingerInfo);
				break;
			case UIHelper.FINGERPRINT_UPCHAR_FAIL:// char--返回失败
			case UIHelper.FINGERPRINT_REGMODE_FAIL:// char--模板合成失败
			case UIHelper.FINGERPRINT_GENCHAR_FAIL:// char--生成特征值失败
			case UIHelper.FINGERPRINT_DELETECHAR_FAIL:// char--模板下载失败
			case UIHelper.FINGERPRINT_ONMATCH_FAIL:// char--匹配失败
				toast = "失败";
				break;
			case UIHelper.FINGERPRINT_UPIMAGE_FAIL:// image--生成失败
				toast = "image--生成失败";
				break;
			case UIHelper.FINGERPRINT_AGAIN:// 再次按手指
				toast = "再次按手指";
				break;

			case UIHelper.FINGERPRINT_UPIMAGE_SUCCESS:// image--图片合成成功
				if (bundle != null) {
					mModel = bundle.getByteArray(UIHelper.FINGERPRINT_SOURCE);
				}
				toast = "图片合成成功";
				Bitmap image = BitmapFactory.decodeByteArray(mModel, 0,
						mModel.length);
				mImageView.setBackgroundDrawable(new BitmapDrawable(image));
				break;
			case UIHelper.FINGERPRINT_ONMATCH_SUCCESS:// image--图片合成成功
				toast = "匹配成功";
				break;

			default:
				break;
			}
			Toast.makeText(FingerprintEnteringActivity.this,
					toast + "：" + msg.what, Toast.LENGTH_SHORT).show();
		};

	};

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		mIsShowImage = isChecked;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onStop()
	 */
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		FingerprintManager.getInstance().closeSerialPort();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (KeyEvent.KEYCODE_BACK == keyCode) {
			FingerprintManager.getInstance().closeSerialPort();
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}

}
