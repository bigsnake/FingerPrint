/**
 * @Title: ChooseOrganActivty.java
 * @Package: com.jason.fingerprint.ui
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月2日 上午4:56:17
 * @Version: V1.0
 */
package com.jason.fingerprint.ui;

import java.util.List;

import org.kymjs.aframe.database.KJDB;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.KJActivityManager;
import org.kymjs.aframe.ui.activity.BaseActivity;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.jason.fingerprint.AppContext;
import com.jason.fingerprint.R;
import com.jason.fingerprint.adapter.ChooseOrganAdapter;
import com.jason.fingerprint.beans.OrganBean;

/**
 * @ClassName: ChooseOrganActivty
 * @Description: 选择机构名称和Id
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月2日 上午4:56:17
 */
public class ChooseOrganActivty extends BaseActivity implements OnItemClickListener {
	
	private static final String TAG = "ChooseOrganActivty";

	@BindView(id = R.id.header_back,click = true)
	private Button mBackButton;
	@BindView(id = R.id.header_title)
	private TextView mTitleTextView;
	@BindView(id = R.id.choose_organ_listview)
	private ListView mListView;
	
	private ChooseOrganAdapter mChooseOrganAdapter;
	private AppContext mAppContext;
	private KJDB mKjdb;
	private List<OrganBean> mOrganBeans;
	private String mTitleName;
	
	@Override
	public void setRootView() {
		setContentView(R.layout.activity_choose_organ);
	}
	
	@Override
	protected void initWidget() {
		super.initWidget();
		mAppContext = (AppContext) getApplication();
		mKjdb = mAppContext.getKjdb();
	}
	
	@Override
	protected void initData() {
		super.initData();
		Intent intent = getIntent();
		if (intent != null) {
			mTitleName = intent.getStringExtra(RecordAddOrUpdateActivity.RECORD_STATE);
			mTitleTextView.setText(mTitleName);
		}
		mOrganBeans = mKjdb.findAll(OrganBean.class);
		mChooseOrganAdapter = new ChooseOrganAdapter(ChooseOrganActivty.this,mOrganBeans);
		mListView.setAdapter(mChooseOrganAdapter);
		mListView.setOnItemClickListener(this);
	}
	
	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.header_back:
			KJActivityManager.create().finishActivity(ChooseOrganActivty.this);
			break;

		default:
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		String content = null;
		String organId = null;
		if (mOrganBeans != null && !mOrganBeans.isEmpty() && mOrganBeans.size() > 0) {
			content = mOrganBeans.get(position).getMachName();
			organId = String.valueOf(mOrganBeans.get(position).getMachId());
		}
		Log.i(TAG, "------>content:" + content + ",organId:" + organId);
		Intent data = new Intent();
		data.putExtra(RecordAddOrUpdateActivity.RECORD_ADD_FIELD_CONTENT, content);
		data.putExtra("machId", organId);
		setResult(RESULT_OK, data);
		finish();
		
	}

}
