/**
 * @Title: ImageLoaderHelper.java
 * @Package: com.jason.fingerprint.common
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月4日 下午10:17:26
 * @Version: V1.0
 */
package com.jason.fingerprint.common;

import android.content.Context;
import android.graphics.Bitmap;

import com.jason.fingerprint.R;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

/**
 * @ClassName: ImageLoaderHelper
 * @Description: 图片缓存
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月4日 下午10:17:26
 */
public class ImageLoaderHelper {
	
	public static final int DISK_CACHE_SIZE = 100 * 1024 * 1024;// 100 Mb
	
	public static void initImageLoader(Context context){
		if (!ImageLoader.getInstance().isInited()) {
			ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
					context).threadPriority(Thread.NORM_PRIORITY)
					.denyCacheImageMultipleSizesInMemory()
					.diskCacheFileNameGenerator(new Md5FileNameGenerator())
					.diskCacheSize(DISK_CACHE_SIZE)
					// 缓存到文件的最大数据
					.tasksProcessingOrder(QueueProcessingType.LIFO)
					.writeDebugLogs() // Remove for release app
					.build();
			// Initialize ImageLoader with configuration.
			ImageLoader.getInstance().init(config);
		}
	}
	
	public static DisplayImageOptions getDisplayImageOptions(Context context) {
		initImageLoader(context);
		return new DisplayImageOptions.Builder()
				.showImageForEmptyUri(R.drawable.ic_empty)
				.showImageOnFail(R.drawable.ic_error)
				.resetViewBeforeLoading(true).cacheOnDisk(true)
				.imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true)
				.displayer(new FadeInBitmapDisplayer(300)).build();
	}

}
