/**
 * @Title: DialogHelper.java
 * @Package: com.jason.fingerprint.common
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月30日10:13:11
 * @Version: V1.0
 */
package com.jason.fingerprint.common;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;

/**
 * @ClassName: DialogHelper
 * @Description: 弹出框帮助类
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月19日 下午8:23:18
 */
public class DialogHelper {

	public static final int CHOOSE_PHOTO_REQUEST_OK = 1001;

	private static final String TAG = "DialogHelper.java";

	public static void openChoosePhotoDialog(final Activity activity,
			final Intent intent) {
		new AlertDialog.Builder(activity).setTitle("拍照").setMessage("是否进行拍照？")
				.setCancelable(true)
				.setPositiveButton("确定", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// activity.startActivity(new
						// Intent(android.provider.Settings.ACTION_WIFI_SETTINGS));
						activity.startActivityForResult(intent,
								DialogHelper.CHOOSE_PHOTO_REQUEST_OK);
					}

				}).setNegativeButton("取消", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						dialog.dismiss();
					}

				}).create().show();
	}

	// 开启网络设置的弹出框
	public static void openNetSettingsDialog(final Activity activity) {
		new AlertDialog.Builder(activity).setTitle("设置网络")
				.setMessage("是否开启网络设置？").setCancelable(true)
				.setPositiveButton("确定", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						activity.startActivity(new Intent(
								android.provider.Settings.ACTION_WIFI_SETTINGS));
					}

				}).setNegativeButton("取消", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						dialog.dismiss();
					}

				}).create().show();
	}

	// 拨打电话
	public static void openCallDialer(final Activity activity,
			final String phone) {
		new AlertDialog.Builder(activity).setTitle("提示")
				.setMessage("客服工作时间8:30-17:30，是否现在拨打？").setCancelable(true)
				.setPositiveButton("是", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// 用intent启动拨打电话
						Intent intent = new Intent(Intent.ACTION_CALL, Uri
								.parse("tel:" + phone));
						activity.startActivity(intent);
						dialog.dismiss();
					}

				}).setNegativeButton("否", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						dialog.dismiss();
					}

				}).create().show();
	}
}
