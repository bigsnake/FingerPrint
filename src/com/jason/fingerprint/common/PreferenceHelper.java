/**
 * @Title: PreferenceHelper.java
 * @Package: com.jason.fingerprint.common
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月19日 下午8:23:18
 * @Version: V1.0
 */
package com.jason.fingerprint.common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

import com.jason.fingerprint.beans.UserBean;

/**
 * @ClassName: PreferenceHelper
 * @Description: SharedPreferences操作工具包(保存和读取信息)
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月19日 下午8:23:18
 */
@SuppressLint("CommitPrefEdits") 
public class PreferenceHelper {
	
	private static final String TAG = "PreferenceHelper";
	private static final String PREFERENCE_NAME_LOGININFO = "LoginInfo";
	
	/***
	 * 
	 * @Descripton: 保存当前用户登录信息到Preference
	 * @Param @param context 上下文
	 * @Param @param loginBean 登录实体Bean
	 * @Throws
	 */
	public static void saveLoginInfoToPreference(Context context,UserBean loginBean){
		SharedPreferences preferences = context.getSharedPreferences(PREFERENCE_NAME_LOGININFO, Context.MODE_PRIVATE);
		ByteArrayOutputStream baos = null;
		ObjectOutputStream oos = null;
		try {
			baos = new ByteArrayOutputStream();
			oos = new ObjectOutputStream(baos);
			oos.writeObject(loginBean);
			String login = new String(Base64.encode(baos.toByteArray(), Base64.DEFAULT));
			SharedPreferences.Editor editor = preferences.edit();
			editor.putString(PREFERENCE_NAME_LOGININFO, login);
			editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if (baos != null) {
				try {
					baos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (oos != null) {
				try {
					oos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/****
	 * 
	 * @Descripton: 从Preference获取当前登录用户信息
	 * @Param @param context 上下文
	 * @Param @return
	 * @Return $
	 * @Throws
	 */
	public static UserBean getLoginInfoFormPreference(Context context){
		SharedPreferences preferences = context.getSharedPreferences(PREFERENCE_NAME_LOGININFO, Context.MODE_PRIVATE);
		String login = preferences.getString(PREFERENCE_NAME_LOGININFO, "");  
        byte[] base64Bytes = Base64.decode(login.getBytes(),Base64.DEFAULT);
        ByteArrayInputStream bais = null;
        ObjectInputStream ois = null;
        UserBean loginBean = null;
        try {
        	bais = new ByteArrayInputStream(base64Bytes);  
            ois = new ObjectInputStream(bais); 
            loginBean = (UserBean) ois.readObject();
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			if (bais != null) {
				try {
					bais.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (ois != null) {
				try {
					ois.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return loginBean;
	}

}
