/**
 * @Title: DataHelper.java
 * @Package: com.jason.fingerprint.common
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月20日 下午11:38:59
 * @Version: V1.0
 */
package com.jason.fingerprint.common;

import org.kymjs.aframe.utils.StringUtils;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import com.jason.fingerprint.beans.ui.ImageOrAudioBean;

/**
 * @ClassName: DataHelper
 * @Description: 数据处理
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月20日 下午11:38:59
 */
public class DataHelper {

	private static final String TAG = "DataHelper";

	/***
	 * 根据文件路径获取文件信息
	 */
	public static ImageOrAudioBean getImageOrAudioBeanInfoByPath(
			Context context, String path, int fileType) {
		Uri mImageUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
		ImageOrAudioBean bean = new ImageOrAudioBean();
		ContentResolver mContentResolver = context.getContentResolver();
		// 只查询jpeg和png的图片
		if (StringUtils.isEmpty(path)) {

		} else {
			Cursor mCursor = mContentResolver.query(mImageUri, null,
					MediaStore.Images.Media.DATA + "=?", new String[] { path },
					MediaStore.Images.Media.DATE_MODIFIED);
			mCursor.moveToFirst();
			String title = mCursor.getString(mCursor
					.getColumnIndex(MediaStore.Images.Media.TITLE));
			String disName = mCursor.getString(mCursor
					.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME));
			String type = mCursor.getString(mCursor
					.getColumnIndex(MediaStore.Images.Media.MIME_TYPE));
			String id = mCursor.getString(mCursor
					.getColumnIndex(MediaStore.Images.Media._ID));
			String addDate = mCursor.getString(mCursor
					.getColumnIndex(MediaStore.Images.Media.DATE_ADDED));
			String modifyData = mCursor.getString(mCursor
					.getColumnIndex(MediaStore.Images.Media.DATE_MODIFIED));
			String height = mCursor.getString(mCursor
					.getColumnIndex(MediaStore.Images.Media.HEIGHT));
			String width = mCursor.getString(mCursor
					.getColumnIndex(MediaStore.Images.Media.WIDTH));
			String size = mCursor.getString(mCursor
					.getColumnIndex(MediaStore.Images.Media.SIZE));
			Log.i(TAG, ",path:" + path + ",title:" + title + ",disName:"
					+ disName + ",type:" + type + ",id:" + ",addDate:"
					+ addDate + ",modifyData:" + modifyData + ",height:"
					+ height + ",width:" + width + ",size:" + size);
			bean.setCreateDate(String.valueOf(System.currentTimeMillis()));
			bean.setFileName(title);
			bean.setFilePath(path);
			bean.setFileType(fileType);
			bean.setWidth(width);
			bean.setHeight(height);
			bean.setSize(size);
			bean.setState(1);
			if (mCursor != null) {
				mCursor.close();
			}
		}
		return bean;
	}
	
	/***
	 * 获取最新拍照的图片信息
	 * @Descripton: TODO
	 * @Param @return
	 * @Return $
	 * @Throws
	 */
	public static String getTheNewPhotoPath(Context context){
		Uri mImageUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
		ContentResolver mContentResolver = context.getContentResolver();
		String path = null;
		try {
			Cursor mCursor = mContentResolver.query(mImageUri, null,
					null,null,
					MediaStore.Images.Media.DATE_ADDED);
			mCursor.moveToFirst();
			path = mCursor.getString(mCursor
					.getColumnIndex(MediaStore.Images.Media.DATA));
			if (mCursor != null) {
				mCursor.close();
			}
		} catch (Exception e) {
			
		}
		return path;
	}

	// 获取文件的名称
	public static String getFileName(String path) {
		if (!StringUtils.isEmpty(path)) {
			return path.substring(path.lastIndexOf("/") + 1, path.length());
		} else {
			return null;
		}
	}

	/****
	 * 
	 * @Descripton: TODO  @Param @param type 签到类型(0日常，1请假，2解矫，3销假)  @Param @return
	 *               @Return $
	 * @Throws
	 */
	public static String getContentBySignType(String type) {

		if (StringUtils.isEmpty(type)) {
			return "请假";
		} else {
			if (type.equals("1")) {
				return "请假";
			} else if (type.equals("2")) {
				return "解矫";
			} else if (type.equals("3")) {
				return "销假";
			} else {
				return "日常";
			}
		}
	}

	/***
	 * 
	 * @Descripton: TODO  @Param @param isCancel 是否指纹签到 (0:否 1:是)  @Param @return
	 *               @Return $
	 * @Throws
	 */
	public static String getContentByIsCancel(String isCancel) {
		if (StringUtils.isEmpty(isCancel)) {
			return "否";
		} else {
			if (isCancel.equals("0")) {
				return "否";
			} else {
				return "是";
			}
		}
	}

}
