/**
 * @Title: UIHelper.java
 * @Package: com.jason.fingerprint.common
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月4日09:49:14
 * @Version: V1.0
 */
package com.jason.fingerprint.common;

import java.io.File;
import java.util.Calendar;
import java.util.Locale;

import android.content.Context;
import android.text.format.DateFormat;

import com.jason.fingerprint.utils.FileUtils;

/**
 * @ClassName: UIHelper
 * @Description: 应用文件工具
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月4日09:49:20
 */
public class FileHelper {

	private static final String TAG = "FileHelper";

	public static final String ROOT_FILE_NAME = "FingerPrint";// 文件存储根路径
	public static final String IMAGES_FILE_NAME = "Images";// 拍照存储路径
	public static final String AUDIOS_FILE_NAME = "Audios";// 录音存储路径
	public static final String CACHES_FILE_NAME = "Caches";// 网络图片缓存路径
	public static final String APKS_FILE_NAME = "Apks";// 应用更新存储路径
	public static final String SQL_FILE_NAME = "Sql";// 数据库保存位置
	private static File mFingerPrintDir;
	private static File mImagesDir;
	private static File mAudiosDir;
	private static File mCachesDir;
	private static File mApksDir;
	private static File mSqlDir;
	private Context mContext;

	public FileHelper(Context context) {
		this.mContext = context;
		// App缓存根目录
		mFingerPrintDir = FileUtils.getDiskCacheDir(context, ROOT_FILE_NAME);
		if (!mFingerPrintDir.exists()) {
			mFingerPrintDir.mkdirs();
		}
		// 图片
		mImagesDir = getFileFromName(mContext, IMAGES_FILE_NAME);
		if (!mImagesDir.exists()) {
			mImagesDir.mkdirs();
		}
		// 录音
		mAudiosDir = getFileFromName(mContext, AUDIOS_FILE_NAME);
		if (!mAudiosDir.exists()) {
			mAudiosDir.mkdirs();
		}
		// 网络图片
		mCachesDir = getFileFromName(mContext, CACHES_FILE_NAME);
		if (!mCachesDir.exists()) {
			mCachesDir.mkdirs();
		}
		// 软件更新
		mApksDir = getFileFromName(mContext, APKS_FILE_NAME);
		if (!mApksDir.exists()) {
			mApksDir.mkdirs();
		}
		// 数据库
		mSqlDir = getFileFromName(mContext, SQL_FILE_NAME);
		if (!mSqlDir.exists()) {
			mSqlDir.mkdirs();
		}
	}

	// 获取应用缓存根目录下
	public File getFingerPrintDir() {
		return mFingerPrintDir;
	}

	// 获取应用缓存根目录下
	public File getImagesDir() {
		return mImagesDir;
	}

	// 获取应用缓存根目录下
	public File getAudiosDir() {
		return mAudiosDir;
	}

	// 获取应用缓存根目录下
	public File getCachesDir() {
		return mCachesDir;
	}

	// 获取更新的apk
	public File getApksDir() {
		return mApksDir;
	}
	
	//数据库
	public File getSqlDir(){
		return mSqlDir;
	}

	//获取App根目录
	public static String getAppRootDir(Context context) {
		return FileUtils.getDiskCacheDir(context, ROOT_FILE_NAME)
				.getAbsolutePath();
	}

	public static String getSdRootDir(Context context) {
		return FileUtils.getDiskCacheDir(context, ROOT_FILE_NAME)
				.getAbsolutePath();
	}

	// 获取应用根目录下指定文件夹名字的文件
	public static File getFileFromName(Context context, String dirName) {
		File rootFile = FileUtils.getDiskCacheDir(context, ROOT_FILE_NAME);
		if (!rootFile.exists()) {
			rootFile.mkdirs();
		}
		File childFile = new File(rootFile, dirName);
		if (!childFile.exists()) {
			rootFile.mkdirs();
		}
		return childFile;
	}

	// 获取图片随机名字
	public static String getPhotoFileName() {
		String photoName = DateFormat.format("yyyyMMdd_hhmmss",
				Calendar.getInstance(Locale.CHINA))
				+ ".jpg";
		return photoName;
	}

	// 获取一个空的图片文件
	public static File getPhotoFile(Context context) {
		File photoFile = new File(new FileHelper(context).getImagesDir(),
				FileHelper.getPhotoFileName());
		return photoFile;
	}
	
	//获取数据库的路径
	public static String getSqlPath(Context context){
		return new FileHelper(context).getSqlDir().getPath();
	}

}
