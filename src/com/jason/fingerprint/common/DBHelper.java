/**
 * @Title: DBHelper.java
 * @Package: com.jason.fingerprint.common
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月13日 上午11:11:59
 * @Version: V1.0
 */
package com.jason.fingerprint.common;

import org.kymjs.aframe.database.DbModel;
import org.kymjs.aframe.database.KJDB;
import org.kymjs.aframe.utils.StringUtils;

/**
 * @ClassName: DBHelper
 * @Description: 数据库帮助类
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月13日 上午11:11:59
 */
public class DBHelper {
	
	public static int getMaxIdFromTable(KJDB kjdb,String table){
		if (kjdb == null || StringUtils.isEmpty(table)) {
			return 0;
		}
		DbModel dbMobel = kjdb.findDbModelBySQL("select max(id) as id from " + table + " ;");
		return dbMobel.getInt("id");
	}

}
