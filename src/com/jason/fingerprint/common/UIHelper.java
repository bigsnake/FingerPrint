/**
 * @Title: UIHelper.java
 * @Package: com.jason.fingerprint.common
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月18日 下午5:13:56
 * @Version: V1.0
 */
package com.jason.fingerprint.common;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import com.jason.fingerprint.AppManager;
import com.jason.fingerprint.R;

/**
 * @ClassName: UIHelper
 * @Description: 应用程序UI工具包：封装UI相关的一些操作
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月18日 下午5:13:56
 */
public class UIHelper {

	public final static int LISTVIEW_ACTION_INIT = 0x01;
	public final static int LISTVIEW_ACTION_REFRESH = 0x02;
	public final static int LISTVIEW_ACTION_SCROLL = 0x03;
	public final static int LISTVIEW_ACTION_CHANGE_CATALOG = 0x04;
	public final static int LISTVIEW_ACTION_SEARCH = 0x05;

	public final static int LISTVIEW_DATA_MORE = 0x01;
	public final static int LISTVIEW_DATA_LOADING = 0x02;
	public final static int LISTVIEW_DATA_FULL = 0x03;
	public final static int LISTVIEW_DATA_EMPTY = 0x04;

	// 指纹录入
	public final static int FINGERPRINT_SUCCESS = 0x1a;
	public final static int FINGERPRINT_FAIL = 0x1b;
	public final static int FINGERPRINT_AGAIN = 0x1c;

	public final static int FINGERPRINT_GETIMAGE_SUCCESS = 0x01;
	public final static int FINGERPRINT_GETIMAGE_FAIL = 0x02;
	public final static int FINGERPRINT_UPIMAGE_SUCCESS = 0x03;
	public final static int FINGERPRINT_UPIMAGE_FAIL = 0x04;
	public final static int FINGERPRINT_GENCHAR_SUCCESS = 0x05;
	public final static int FINGERPRINT_GENCHAR_FAIL = 0x06;
	public final static int FINGERPRINT_REGMODE_SUCCESS = 0x07;
	public final static int FINGERPRINT_REGMODE_FAIL = 0x08;
	public final static int FINGERPRINT_UPCHAR_SUCCESS = 0x09;
	public final static int FINGERPRINT_UPCHAR_FAIL = 0x10;
	public final static int FINGERPRINT_ONSTORECHAR_SUCCESS = 0x11;
	public final static int FINGERPRINT_ONSTORECHAR_FAIL = 0x12;
	public final static int FINGERPRINT_LOADCHAR_SUCCESS = 0x13;
	public final static int FINGERPRINT_LOADCHAR_FAIL = 0x14;
	public final static int FINGERPRINT_SEARCH_SUCCESS = 0x15;
	public final static int FINGERPRINT_SEARCH_FAIL = 0x16;
	public final static int FINGERPRINT_DELETECHAR_SUCCESS = 0x17;
	public final static int FINGERPRINT_DELETECHAR_FAIL = 0x18;
	public final static int FINGERPRINT_EMPTY_SUCCESS = 0x19;
	public final static int FINGERPRINT_EMPTY_FAIL = 0x20;
	public final static int FINGERPRINT_DOWNCHAR_SUCCESS = 0x21;
	public final static int FINGERPRINT_DOWNCHAR_FAIL = 0x22;
	public final static int FINGERPRINT_ONMATCH_SUCCESS = 0x22;
	public final static int FINGERPRINT_ONMATCH_FAIL = 0x23;

	public final static String FINGERPRINT_SOURCE = "fingerprint_source";

	// 登录
	public final static int LOGIN_SUCCESS = 0x00;
	public final static int LOGIN_FAIL = 0x010;

	// 指纹

	// 文件上传
	public final static int FILE_UPLOAD_SUCCESS = 100000;
	public final static int FILE_UPLOAD_FAIL = 100001;

	// 签到上传
	public final static int SIGNIN_UPLOAD_SUCCESS = 200000;
	public final static int SIGNIN_UPLOAD_FAIL = 200001;
	public final static int SIGNIN_UPLOAD_ERROR = 200002;

	// 请销假
	public final static int LEAVEOUT_UPLOAD_SUCCESS = 1001;
	public final static int LEAVEOUT_UPLOAD_FAIL = 1002;

	//请销假
	public final static int WORK_VISIT_UPLOAD_SUCCESS = 1003;
	public final static int WORK_VISIT_UPLOAD_FAIL = 1004;

	/**
	 * 发送App异常崩溃报告
	 * 
	 * @param cont
	 * @param crashReport
	 */
	public static void sendAppCrashReport(final Context cont,
			final String crashReport) {
		AlertDialog.Builder builder = new AlertDialog.Builder(cont);
		builder.setIcon(android.R.drawable.ic_dialog_info);
		builder.setTitle(R.string.app_error);
		builder.setMessage(R.string.app_error_message);
		builder.setPositiveButton(R.string.submit_report,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						// 发送异常报告
						Intent i = new Intent(Intent.ACTION_SEND);
						// i.setType("text/plain"); //模拟器
						i.setType("message/rfc822"); // 真机
						i.putExtra(Intent.EXTRA_EMAIL,
								new String[] { "zhangyujin@huinfo.com" });
						i.putExtra(Intent.EXTRA_SUBJECT, "FingerPrint - 错误报告");
						i.putExtra(Intent.EXTRA_TEXT, crashReport);
						cont.startActivity(Intent.createChooser(i, "发送错误报告"));
						// 退出
						AppManager.getAppManager().AppExit(cont);
					}
				});
		builder.setNegativeButton(R.string.sure,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						// 退出
						AppManager.getAppManager().AppExit(cont);
					}
				});
		builder.show();
	}

	/**
	 * 退出程序
	 * 
	 * @param cont
	 */
	public static void Exit(final Context cont) {
		AlertDialog.Builder builder = new AlertDialog.Builder(cont);
		builder.setIcon(android.R.drawable.ic_dialog_info);
		builder.setTitle(R.string.app_menu_surelogout);
		builder.setPositiveButton(R.string.sure,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						// 退出
						AppManager.getAppManager().AppExit(cont);
					}
				});
		builder.setNegativeButton(R.string.cancle,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
		builder.show();
	}

}
