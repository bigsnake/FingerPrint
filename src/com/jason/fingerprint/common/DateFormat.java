/**
 * @Title: DateFormat.java
 * @Package: com.jason.fingerprint.common
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月27日17:13:42
 * @Version: V1.0
 */
package com.jason.fingerprint.common;


/**
 * @ClassName: DateFormat
 * @Description: 时间格式化
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月27日17:13:31
 */
public class DateFormat {
	
	public static final String DATE_YMD_HMS = "yyyy-MM-dd HH:mm:ss";

    public static final String DATE_YMD_HMS_SHORT = "yyyyMMddHHmmss";

    public static final String DATE_YMD_HM = "yyyy-MM-dd HH:mm";

    public static final String DATE_YMD_HM_SHORT = "yyyyMMddHHmm";

    public static final String DATE_YMD_H = "yyyy-MM-dd HH";

    public static final String DATE_YMD_H_SHORT = "yyyyMMddHH";

    public static final String DATE_YMD = "yyyy-MM-dd";

    public static final String DATE_YMD_SHORT = "yyyyMMdd";

    public static final String DATE_YM = "yyyy-MM";

    public static final String DATE_YM_SHORT = "yyyyMM";

}
