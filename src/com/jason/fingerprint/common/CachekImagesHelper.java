/*
 * Name   CachekImagesHelper.java
 * Author ZhangZhenli
 * Created on 2014-10-28, 上午11:38:04
 *
 * Copyright (c) 2014 NanJing YiWuXian Network Technology Co., Ltd. All rights reserved
 *
 */
package com.jason.fingerprint.common;

import java.util.ArrayList;
import java.util.List;

import org.kymjs.aframe.utils.StringUtils;

/**
 * @ClassName: CachekImagesHelper
 * @Description: 图片选取处理
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月28日11:38:50
 */
public class CachekImagesHelper {

	private static final String TAG = "CachekImagesHelper.java";
	
	public static List<String> addImages(List<String> source,List<String> targe){
		if (source == null || source.isEmpty()) {
			source = new ArrayList<String>();
		}
		if (targe == null || targe.isEmpty()) {
			return source;
		}
		for (String path : source) {
			for (String string : targe) {
				if (!string.equals(path)) {
					source.add(string);
				}
			}
		}
		return source;
	}
	
	public static List<String> addImage(List<String> source,String targe){
		if (source == null || source.isEmpty()) {
			source = new ArrayList<String>();
		}
		if (StringUtils.isEmpty(targe)) {
			return source;
		}
		for (String path : source) {
			if (!path.equals(targe)) {
				source.add(targe);
			}
		}
		return source;
	}
	
	public static List<String> removeImage(List<String> source,String targe){
		if (source == null || source.isEmpty()) {
			source = new ArrayList<String>();
		}
		if (StringUtils.isEmpty(targe)) {
			return source;
		}
		for (String path : source) {
			if (path.equals(targe)) {
				source.remove(targe);
			}
		}
		return source;
	}
	
	public static List<String> removeImages(List<String> source,List<String> targe){
		if (source == null || source.isEmpty()) {
			source = new ArrayList<String>();
		}
		if (targe == null || targe.isEmpty()) {
			return source;
		}
		for (String path : source) {
			for (String string : targe) {
				if (string.equals(path)) {
					source.remove(string);
				}
			}
		}
		return source;
	}
}
