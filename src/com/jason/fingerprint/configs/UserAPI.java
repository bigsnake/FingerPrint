/**
 * @Title: UserAPI.java
 * @Package: com.jason.fingerprint.test
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月18日 上午11:37:17
 * @Version: V1.0
 */
package com.jason.fingerprint.configs;

import org.kymjs.aframe.http.HttpCallBack;
import org.kymjs.aframe.http.KJFileParams;
import org.kymjs.aframe.http.KJHttp;

/**
 * @ClassName: UserAPI
 * @Description: 用户测试
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月18日 上午11:37:17
 */
public class UserAPI {
	
	private static final String TAG = "UserAPI";
	
	
	public static void main(String[] args) {
		testLogin();
	}
	
	/****
	 * http://223.68.137.165:6666/SuperviseService/android/datb/login.action?imie=860312023914656&type=2&loginName=gpsf%20&loginPwd=111
	 * @Descripton: 登录
	 * @Param 
	 * @Return $
	 * @Throws
	 */
	public static void testLogin(){
		KJHttp kjHttp = new KJHttp();
		KJFileParams params = new KJFileParams();
		params.put("imie", "860312023914656");
		params.put("type", "2");
		params.put("loginName", "gpsf");
		params.put("loginPwd", "111");
		kjHttp.post(HttpConfig.POST_URL_DATB_LOGIN, params, new HttpCallBack() {
			
			@Override
			public void onSuccess(Object arg0) {
				
				System.out.println(arg0.toString());
	
			}
			
			@Override
			public void onLoading(long arg0, long arg1) {
				
				
			}
			
			@Override
			public void onFailure(Throwable arg0, int arg1, String arg2) {
				
			}
		});
	}

}
