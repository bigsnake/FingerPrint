/**
 * @Title: HttpConfig.java
 * @Package: com.jason.fingerprint.configs
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月18日 上午10:51:50
 * @Version: V1.0
 */
package com.jason.fingerprint.configs;

/**
 * @ClassName: HttpConfig
 * @Description: 对于网络请求接口的所有静态配置
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月18日 上午10:51:50
 */
public class HttpConfig {
	
	public final static String HTTP = "http://";
	public final static String HTTPS = "https://";
	public final static String HOST = "223.68.137.165";
	public final static String PORT = "6666";

	private final static String URL_SPLITTER = "/";
	private final static String URL_HOST_PLITTER = ":";
	private final static String URL_API_HOST = HTTP + HOST + URL_HOST_PLITTER + PORT + URL_SPLITTER;
	
	/***
	 * APP登录-->用户指纹登录和账号登录
	 * IM:860312023914656
	 */
	public final static String POST_URL_DATB_LOGIN = URL_API_HOST + "SuperviseService/android/datb/login.action";
	

	/***
	 * APP登录-->成功登录之后，插入登录日志。
	 * IM:860312023914656
	 */
	public final static String POST_URL_DATB_ADDLOGININFO = URL_API_HOST + "SuperviseService/android/datb/addLoginInfo.action";
	
	/***
	 * 档案同步-->获取某机构下矫正人员的信息
	 */
	public final static String POST_URL_DATB_RECORDSYNC = URL_API_HOST + "SuperviseService/android/datb/recordSync.action";
	
	/***
	 * 档案同步-->加载登录帐号下可选择的隶属机构
	 */
	public final static String POST_URL_DATB_ORGANSYNC = URL_API_HOST + "SuperviseService/android/datb/organSync.action";
	
	/***
	 * 档案同步-->判断当前机构是否有权限录入指纹信息
	 */
	public final static String POST_URL_DATB_SELECTORGANBYID = URL_API_HOST + "SuperviseService/android/datb/selectOrganById.action";
	
	/***
	 * 指纹录入-->用户指纹的录入和修改
	 */
	public final static String POST_URL_ELECTRICANDFINGERPRINT_INSERTORUPDATE = URL_API_HOST + "SuperviseService/android/electricAndFingerprint/insertOrUpdate.action";
	
	/***
	 * 社区服务教育-->获取当天的活动
	 */
	public final static String POST_URL_EDUCOM_GETACTIVITYLIST = URL_API_HOST + "SuperviseService/android/eduCom/getActivityList.action";
	
	/***
	 * 社区服务教育-->新增集中教育、集中劳动、个别教育、个别劳动签到记录
	 */
	public final static String POST_URL_EDUCOM_ADDCOMEDULIST = URL_API_HOST + "SuperviseService/android/eduCom/addComEduList.action";
	
	/***
	 * 社区服务教育-->新增集中教育、集中劳动、个别教育、个别劳动签到记录时用到的照片
	 */
	public final static String POST_URL_EDUCOM_ADDCOMEDUINFO = URL_API_HOST + "SuperviseService/android/eduCom/addComEduInfo.action";
	
	/***
	 * 社区服务教育-->补充活动信息 照片、录音
	 */
	public final static String POST_URL_EDUCOM_ADDACTIVITYINFO = URL_API_HOST + "SuperviseService/android/eduCom/addActivityInfo.action";
	
	/***
	 * 日常报到-->新增日常报到的记录
	 */
	public final static String POST_URL_SIGN_SAVESIGN = URL_API_HOST + "SuperviseService/android/sign/saveSign.action";
	
	/***
	 * 日常报到-->新增日常报到图片信息
	 */
	public final static String POST_URL_SIGN_SAVESIGNPHOTO = URL_API_HOST + "SuperviseService/android/sign/saveSignPhoto.action";
	
	/***
	 * 工作走访-->（新增走访记录）将走访的签到记录上传到平台
	 */
	public final static String POST_URL_WORKINGVISIT_INSERTVISIT = URL_API_HOST + "SuperviseService/android/workingVisit/insertVisit.action";
	
	/***
	 * 外出请假-->新增外出请假记录
	 */
	public final static String POST_URL_EGRESSLEAVE_INSERTLEAVE = URL_API_HOST + "SuperviseService/android/egressLeave/insertLeave.action";
	
	/***
	 * 外出请假-->请销假签到
	 */
	public final static String POST_URL_EGRESSLEAVE_UPDATESIGNFLAG = URL_API_HOST + "SuperviseService/android/egressLeaveAction/updateSignFlag.action";
	
	/***
	 * 外出请假-->查询请假列表
	 */
	public final static String POST_URL_EGRESSLEAVE_GETLEAVELIST = URL_API_HOST + "SuperviseService/android/egressLeaveAction/getLeaveList.action";
	
	/***
	 * 工作通知-->查询工作通知基本内容
	 */
	public final static String POST_URL_INFOPUBLISH_QUERY = URL_API_HOST + "SuperviseService/android/infoPublish/query.action";
	
	/***
	 * 工作提醒-->显示网页版中的工作提醒内容
	 */
	public final static String POST_URL_WORKINGREMIND_QUERYREMIND = URL_API_HOST + "SuperviseService/android/workingRemind/queryRemind.action";
	
	/***
	 * 文件上传-->app中所需图片资源的上传
	 */
	public final static String POST_URL_FILEUPLOAD_UPLOADTOTEMP = URL_API_HOST + "SuperviseService/android/fileupload/uploadToTemp.action";
	
	/***
	 * 版本更新-->集成式接口PS:版本号问题:初始版本为1 每次打包上传后版本号加0.1比较大的更新或者稳定版本吧，版本号可以直接加1
	 */
	public final static String POST_URL_VERSION_GETVERSION = URL_API_HOST + "SuperviseService/android/version/getVersion.action";
	
	/***
	 * 版本更新-->分散式(蓝牙设备接口)PS:版本号问题:初始版本为1 每次打包上传后版本号加0.1比较大的更新或者稳定版本吧，版本号可以直接加1
	 */
	public final static String POST_URL_VERSION_GETBLUEVERSION = URL_API_HOST + "SuperviseService/android/version/getBlueVersion.action";
	
}
