package com.jason.fingerprint.configs;

/**
 * @ClassName: DataConfig
 * @Description: 常用数据类型配置
 * @Author: john
 * @Date: 2014年10月27日
 */
public class DataConfig {

	public static final String SIGN_TYPE_FINGERPRINT = "0"; // 指纹签到

	public static final String SIGN_TYPE_INPUT = "1"; // 平台录入

	public static final String SIGN_TYPE_CARD = "2"; // 打卡签到

	public static final String SIGN_FLAG_BEGIN = "0"; // 开始签到

	public static final String SIGN_FLAG_CALL = "1"; // 点名签到

	public static final String SIGN_FLAG_END = "2"; // 结束签到

	public static final String REGISTER_TYPE_0 = "0"; // 集中教育

	public static final String REGISTER_TYPE_1 = "1"; // 集中劳动

	public static final String REGISTER_TYPE_2 = "2"; // 个别教育

	public static final String REGISTER_TYPE_3 = "3"; // 个别劳动

	/* ---- Audio MIME type ---- */
	public static final String AUDIO_3GPP = "audio/3gpp";
	public static final String AUDIO_AMR = "audio/amr";
	public static final String AUDIO_EVRC = "audio/evrc";
	public static final String AUDIO_QCELP = "audio/qcelp";
	public static final String AUDIO_AAC_MP4 = "audio/aac_mp4";
	public static final String AUDIO_WAVE_6CH_LPCM = "audio/wave_6ch_lpcm";
	public static final String AUDIO_WAVE_2CH_LPCM = "audio/wave_2ch_lpcm";
	public static final String AUDIO_AAC_5POINT1_CHANNEL = "audio/aac_5point1_channel";
	public static final String AUDIO_AMR_WB = "audio/amr-wb";
	public static final String AUDIO_ANY = "audio/*";
	public static final String ANY_ANY = "*/*";

	public static final int BITRATE_AMR = 12800;
	public static final int BITRATE_EVRC = 8500;
	public static final int BITRATE_QCELP = 13300;
	public static final int BITRATE_3GPP = 12800;
	public static final int SAMPLERATE_MULTI_CH = 48000;
	public static final int BITRATE_AMR_WB = 16000;
	public static final int SAMPLERATE_AMR_WB = 16000;
	public static final int SAMPLERATE_8000 = 8000;
    
}
