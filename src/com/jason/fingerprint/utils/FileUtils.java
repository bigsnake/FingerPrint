/**
 * @Title: FileUtils.java
 * @Package: com.jason.fingerprint.utils
 * @Descripton: TODO
 * @Author: Administrator zhangyujn1989ok@gmail.com
 * @Date: 2014年11月4日11:32:35
 * @Version: V1.0
 */
package com.jason.fingerprint.utils;

import java.io.File;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

/**
 * @ClassName: FileUtils
 * @Description: 文件处理
 * @Author: Administrator zhangyujn1989ok@gmail.com
 * @Date: 2014年11月4日11:32:21
 */
public class FileUtils {

	private static final String TAG = "FileUtils";

	public static File getDiskCacheDir(Context context, String uniqueName) {
		boolean flag1 = Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
		boolean flag2 = !isExternalStorageRemovable();
		String path1 = getExternalCacheDir(context).getPath();
		String path2 = context.getCacheDir().getPath();
		Log.i(TAG, "----flag1:" + flag1);
		Log.i(TAG, "----flag2:" + flag2);
		Log.i(TAG, "----path1:" + path1);
		Log.i(TAG, "----path2:" + path2);
		final String cachePath = Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())
				|| !isExternalStorageRemovable() ? getExternalCacheDir(context).getPath() : context.getCacheDir()
				.getPath();
		final File dir = new File(cachePath + File.separator + uniqueName);
		if (dir.exists()) {
			if (!dir.isDirectory()) {
				dir.delete();
			} else {
				return dir;
			}
		} else {
			dir.mkdirs();
		}
		return dir;
	}

	@TargetApi(9)
	public static boolean isExternalStorageRemovable() {
		if (hasGingerbread()) {
			return Environment.isExternalStorageRemovable();
		}
		return true;
	}

	@TargetApi(8)
	public static File getExternalCacheDir(Context context) {
		return new File(Environment.getExternalStorageDirectory().getPath());
	}

	public static boolean hasFroyo() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
	}

	public static boolean hasGingerbread() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD;
	}

	public static boolean hasHoneycomb() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
	}

	public static boolean hasHoneycombMR1() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1;
	}

	public static boolean hasJellyBean() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
	}

}
