/**
 * @Title: HttpParamsUtils.java
 * @Package: com.jason.fingerprint.utils
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月19日 下午12:44:19
 * @Version: V1.0
 */
package com.jason.fingerprint.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.kymjs.aframe.http.KJFileParams;
import org.kymjs.aframe.utils.StringUtils;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @ClassName: HttpParamsUtils
 * @Description: http 参数帮助
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月19日 下午12:44:19
 */
public class HttpParamsUtils {
	
	private static final String TAG = "HttpParamsUtils";
	
	/**
     * 将一个 JavaBean 对象转化为一个  KJFileParams
     * @param bean 要转化的JavaBean 对象
     * @param filePath 需要上传的文件路径
     * @return 转化出来的  KJFileParams 对象
     */
	public static KJFileParams convertBeanToKJFileParams(Object bean,List<String> filePath){
		KJFileParams params = convertBeanToKJFileParams(bean);
		if (params == null) {
			params = new KJFileParams();
		}
		if (filePath != null) {
			int size = filePath.size();
			if (size > 0) {
				for (int i = 0; i < size; i++) {
					String path = filePath.get(i);
					try {
						params.put(new File(path));
					} catch (Exception e) {
						Log.e(TAG, "HttpParamsUtils convertBeanToKJFileParams is not found the file ,it's path is :" + path);
						params = null;
					}
				}
			}
		}
		return params;
	}

    /**
     * 将一个 JavaBean 对象转化为一个  KJFileParams
     * @param bean 要转化的JavaBean 对象
     * @return 转化出来的  KJFileParams 对象
     */
    @SuppressWarnings("rawtypes")
	public static KJFileParams convertBeanToKJFileParams(Object bean){
    	if (bean == null) {
			return null;
		}
        KJFileParams params = new KJFileParams();
        try {
        	Gson gson = new GsonBuilder().create();
        	JSONObject jsonObject = new JSONObject(gson.toJson(bean));
        	Iterator iterator = jsonObject.keys();
        	while (iterator.hasNext()) {
				String key = (String)iterator.next();
				String value = null;
				if (!StringUtils.isEmpty(key)) {
					value = (String)jsonObject.get(key);
					if (!StringUtils.isEmpty(value)) {
						params.put(key, value);
					}
				}
				
			}
		} catch (Exception e) {
			params = null;
		}
        return params;
    }

}
