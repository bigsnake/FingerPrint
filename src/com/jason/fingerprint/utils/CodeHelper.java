package com.jason.fingerprint.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;



/**
 * 
 * <编写代码时提供简单逻辑判断控制的函数类> <功能详细描述>
 * 
 * @author 薛会生
 * @version [版本号, 2013-09-25]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class CodeHelper {


	/**
	 * 对象是否为NULL
	 * 
	 * @param param
	 *            Object
	 * @return Null:TRUE ; NotNull : FALSE
	 */
	public static boolean isNull(Object param) {
		return null == param;
	}

	/**
	 * 对象是否不为NULL
	 * 
	 * @param param
	 *            Object
	 * @return Null:FALSE ; NotNull : TRUE
	 */
	public static boolean isNotNull(Object param) {
		return null != param;
	}

	/**
	 * 判断字符串是否为空
	 * 
	 * @param param
	 *            String
	 * @return TRUE:Null Or Empty ; FALSE:length > 0
	 */
	public static boolean isNullOrEmpty(String param) {
		return null == param || param.length() == 0;
	}

	/**
	 * 判断集合是否为空
	 * 
	 * @param param
	 *            Collection
	 * @return TRUE:Null Or Empty ; FALSE:size > 0
	 */
	public static boolean isNullOrEmpty(Collection<? extends Object> param) {
		return null == param || param.isEmpty();
	}

	/**
	 * 判断字符串是否不为空
	 * 
	 * @param param
	 *            String
	 * @return TRUE:length > 0 ; FALSE:Null Or Empty
	 */
	public static boolean isNotEmpty(String param) {
		return null != param && param.length() > 0;
	}

	/**
	 * 判断集合是否不为空
	 * 
	 * @param param
	 *            Collection
	 * @return TRUE:size > 0 ; FALSE:Null Or Empty
	 */
	public static boolean isNotEmpty(Collection<? extends Object> param) {
		return null != param && !param.isEmpty();
	}

	public static boolean isNotEmpty(Integer integer) {
		return CodeHelper.isNotNull(integer);
	}

	// 获取当前时间 String
	public static String getCurrentDate() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(new Date());
	}

	// 获取当前时间
	public static String getCurrentDateHMS() {
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String s = sdf.format(d);
		return s;
	}

	// 字符串转为整形
	public static int parseInteger(String str) {
		int result = 0;
		try {
			if (str != null && !"".equals(str)) {
				result = Integer.parseInt(str);
			} else {
				result = 0;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public static String parseStrUTF8(String str) {
		String temp = "";
		if (str != null && !"".equals(str)) {
			try {
				temp = new String(str.getBytes("iso-8859-1"), "utf-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return temp;
	}

	public static String parseStrToGb(String str) {
		String temp = "";
		if (str != null && !"".equals(str)) {
			try {
				temp = new String(str.getBytes("iso-8859-1"), "gb2312");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return temp;
	}

	public static String checkNull(String s) {
		return s == null ? "" : s;
	}

	public static boolean checkSpecial(String s) {
		if (CodeHelper.isNotEmpty(s)) {
			return (s.contains("%") || s.contains("_") || s.contains("％"));
		} else {
			return false;
		}
	}



	/**
	 * 计算时长
	 * 
	 * @param minDate
	 *            最小时间
	 * @param maxDate
	 *            最大时间
	 * @return
	 */
	public static String calculateDuration(String minDate, String maxDate) {
		SimpleDateFormat dfs = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date begin = new Date();
		Date end = new Date();

		try {
			begin = dfs.parse(minDate);
			end = dfs.parse(maxDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		double between = (end.getTime() - begin.getTime()) / 1000;// 除以1000是为了转换成秒long
		double hour1 = between % (24 * 3600) / 3600;

		return String.format("%.2f", hour1);
	}

	/**
	 * 计算时间相差天
	 */
	public static int calculateDay(String minDate, String maxDate) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date now = null;
		Date date = null;
		try {
			now = df.parse(maxDate);
			date = df.parse(minDate);
		} catch (ParseException e) {
			e.printStackTrace();
			System.out.println("计算时间相差天失败！\n" + e);
		}
		long l = now.getTime() - date.getTime();
		long day = l / (24 * 60 * 60 * 1000);

		return (int) day;
	}

	/**
	 * 计算差时长
	 * 
	 * @param totalCount
	 *            总时间
	 * @param shouldCount
	 *            应该的时间
	 * @return
	 */
	public static String calculateDifferenceFloat(String totalCount,
			String shouldCount) {
		return String.format("%.2f", (Float.parseFloat(totalCount) - Float
				.parseFloat(shouldCount)));
	}

	public static void main(String[] args) {
		System.out.println(calculateDifferenceFloat("11.00", "8"));
	}
	
	/**
	 * 返回当前年-月
	 * 格式：yyyy-MM
	 */
	public static String getYearMonth() {
		return new SimpleDateFormat("yyyy-MM").format(new Date());
	}
	
	/**
	 * 返回当前年月
	 * 格式：yyyyMM
	 */
	public static String getYearMonthFormat() {
		return new SimpleDateFormat("yyyyMM").format(new Date());
	}

	/**
	 * 得到本月的最后一天
	 * 
	 * @return
	 */
	public static String getMonthLastDay() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, calendar
				.getActualMaximum(Calendar.DAY_OF_MONTH));

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(calendar.getTime());
	}
	/**
	 * 解密
	 * @param url
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String decodeURL(String url) throws UnsupportedEncodingException{
		if(url!=null && url.length() > 0){
			return URLDecoder.decode(url, "UTF-8");
		}else{
			return url;
		}
		
		
	}
	/**
	 * 加密
	 * @param str
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String encodeURL(String url) throws UnsupportedEncodingException{
		if(url!=null && url.length() > 0){
			return URLEncoder.encode(url, "UTF-8");
		}else{
			return url;
		}
		
	}
	

	/**
	 * 根据身份证计算年龄
	 * 
	 * @param identity
	 *            身份证
	 */
	public static Integer countAge(String identity) {
		if (CodeHelper.isNullOrEmpty(identity)) {
			return 0;
		} else {
			// 身份证截取的出生年份
			identity = identity.replace(".", "");

			if (identity.length() > 9) {
				String strAge = identity.substring(6, 10);
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
				String currentYear = sdf.format(new Date());
				return Integer.parseInt(currentYear) - Integer.parseInt(strAge);
			} else {
				return 0;
			}
		}
	}
}