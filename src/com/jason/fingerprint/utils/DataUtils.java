/**
 * @Title: DataUtils.java
 * @Package: com.jason.fingerprint.utils
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月24日 下午8:12:01
 * @Version: V1.0
 */
package com.jason.fingerprint.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.text.ParseException;

import android.annotation.SuppressLint;

import org.kymjs.aframe.utils.StringUtils;

/**
 * @ClassName: DataUtils
 * @Description: 时间格式化工具类
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月24日 下午8:12:01
 */
@SuppressLint({ "UseValueOf", "SimpleDateFormat" })
public class DataUtils {
	
	/***
	 * 
	 * @Descripton: 获取当前时间所在天的开始时间
	 * @Param @return
	 * @Return $
	 * @Throws
	 */
	public static Date getCurrentDayBeginDate(){
		Calendar calendar = new GregorianCalendar();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		return calendar.getTime();
	}
	
	/***
	 * 
	 * @Descripton: 获取当前时间所在天的结束时间
	 * @Param @return
	 * @Return $
	 * @Throws
	 */
	public static Date getCurrentDayEndDate(){
		Calendar calendar = new GregorianCalendar();
		calendar.set(Calendar.HOUR_OF_DAY, 23);  
		calendar.set(Calendar.MINUTE, 59);  
		calendar.set(Calendar.SECOND, 59); 
		return calendar.getTime();
	}
	
	/***
	 * 
	 * @Descripton: 获取当前时间所在天的开始周
	 * @Param @return
	 * @Return $
	 * @Throws
	 */
	public static Date getCurrentWeekBeginDate(){
		Calendar calendar = new GregorianCalendar();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);  
		return calendar.getTime();
	}
	
	/***
	 * 
	 * @Descripton: 获取当前时间所在天的结束周
	 * @Param @return
	 * @Return $
	 * @Throws
	 */
	public static Date getCurrentWeekEndDate(){
		Calendar calendar = new GregorianCalendar();
		calendar.set(Calendar.HOUR_OF_DAY, 23);  
		calendar.set(Calendar.MINUTE, 59);  
		calendar.set(Calendar.SECOND, 59); 
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);  
		return calendar.getTime();
	}
	
	/***
	 * 
	 * @Descripton: long转String eg:1415547962595 --> 2014-11-09 23:46:02
	 * @Param @param time 格式化的时间eg:1415547962595
	 * @Param @param dateFormat 格式化样式 eg:yyyy-MM-dd HH:mm:ss
	 * @Param @return 格式化之后的字符串 eg:2014-11-09 23:46:02
	 * @Return $
	 * @Throws
	 */
	public static String converDatLongToString(String time,String dateFormat){
		if (StringUtils.isEmpty(time) || StringUtils.isEmpty(dateFormat)) {
			return null;
		}
		Date date = new Date(Long.parseLong(time));
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
		return formatter.format(date);
	}
	
	public static String converDatLongToString(long time,String dateFormat){
		if (time == 0l || StringUtils.isEmpty(dateFormat)) {
			return null;
		}
		Date date = new Date(time);
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
		return formatter.format(date);
	}
	
	/**
     * 字符串转化成日期
     *
     * @param dateStrVal 日期字符串
     * @param dateFormat 日期类型
     * @return 日期
     */
    public static Date convertStrToDate(String dateStrVal, String dateFormat) {
        if (StringUtils.isEmpty(dateStrVal) || StringUtils.isEmpty(dateFormat))
            return null;

        Date dateVal = null;

        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        try {
        	dateVal = formatter.parse(dateStrVal);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateVal;
    }


    /**
     * 字符串转化成long类型时间
     *
     * @param dateStrVal 日期字符串
     * @param dateFormat 日期类型
     * @return 日期
     */
    public static Long convertStrToLong(String dateStrVal, String dateFormat) {
        if (StringUtils.isEmpty(dateStrVal) || StringUtils.isEmpty(dateFormat))
            return 0l;

        Date dateVal = convertStrToDate(dateStrVal, dateFormat);

        if (dateVal == null)
            return 0l;

        return new Long(dateVal.getTime());
    }

	/**
	 * 16进制字符串转换成数组
	 * 
	 * @param hex
	 * @return
	 */
	public static byte[] hexStringTobyte(String hex) {
		int len = hex.length() / 2;
		byte[] result = new byte[len];
		char[] achar = hex.toCharArray();
		for (int i = 0; i < len; i++) {
			int pos = i * 2;
			result[i] = (byte) (toByte(achar[pos]) << 4 | toByte(achar[pos + 1]));
		}
		return result;
	}

	public static int toByte(char c) {
		byte b = (byte) "0123456789ABCDEF".indexOf(c);
		return b;
	}

	/**
	 * 数组转成16进制字符串
	 * 
	 * @param b
	 * @return
	 */
	public static String toHexString(byte[] b) {
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < b.length; i++) {
			buffer.append(toHexString1(b[i]));
		}
		return buffer.toString();
	}

	public static String toHexString1(byte b) {
		String s = Integer.toHexString(b & 0xFF);
		if (s.length() == 1) {
			return "0" + s;
		} else {
			return s;
		}
	}

	/**
	 * 十六进制字符串转换成字符串
	 */
	public static String hexStr2Str(String hexStr) {
		String str = "0123456789ABCDEF";
		char[] hexs = hexStr.toCharArray();
		byte[] bytes = new byte[hexStr.length() / 2];
		int n;
		for (int i = 0; i < bytes.length; i++) {
			n = str.indexOf(hexs[2 * i]) * 16;
			n += str.indexOf(hexs[2 * i + 1]);
			bytes[i] = (byte) (n & 0xff);
		}
		return new String(bytes);
	}

	/**
	 * 字符串转换成十六进制字符串
	 */
	public static String str2Hexstr(String str) {
		char[] chars = "0123456789ABCDEF".toCharArray();
		StringBuilder sb = new StringBuilder("");
		byte[] bs = str.getBytes();
		int bit;
		for (int i = 0; i < bs.length; i++) {
			bit = (bs[i] & 0x0f0) >> 4;
			sb.append(chars[bit]);
			bit = bs[i] & 0x0f;
			sb.append(chars[bit]);
		}
		return sb.toString();
	}

	/**
	 * 16进制字符串分割成若干块，每块32个16进制字符，即16字节
	 * 
	 * @param str
	 * @return
	 */
	public static String[] hexStr2StrArray(String str) {
		// 32个十六进制字符串表示16字节
		int len = 32;
		int size = str.length() % len == 0 ? str.length() / len : str.length()
				/ len + 1;
		String[] strs = new String[size];
		for (int i = 0; i < size; i++) {
			if (i == size - 1) {
				String temp = str.substring(i * len);
				for (int j = 0; j < len - temp.length(); j++) {
					temp = temp + "0";
				}
				strs[i] = temp;
			} else {
				strs[i] = str.substring(i * len, (i + 1) * len);
			}
		}
		return strs;
	}

	/**
	 * 把16进制字符串压缩成字节数组，在把字节数组转换成16进制字符串
	 * 
	 * @param hexstr
	 * @return
	 * @throws IOException
	 */
	public static byte[] compress(byte[] data) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		GZIPOutputStream gzip = new GZIPOutputStream(out);
		gzip.write(data);
		gzip.close();
		return out.toByteArray();
	}

	/**
	 * 把16进制字符串解压缩压缩成字节数组，在把字节数组转换成16进制字符串
	 * 
	 * @param hexstr
	 * @return
	 * @throws IOException
	 */
	public static byte[] uncompress(byte[] data) throws IOException {

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ByteArrayInputStream in = new ByteArrayInputStream(data);
		GZIPInputStream gunzip = new GZIPInputStream(in);
		byte[] buffer = new byte[256];
		int n;
		while ((n = gunzip.read(buffer)) >= 0) {
			out.write(buffer, 0, n);
		}
		return out.toByteArray();
	}

	public static byte[] short2byte(short s) {
		byte[] size = new byte[2];
		size[0] = (byte) (s >>> 8);
		short temp = (short) (s << 8);
		size[1] = (byte) (temp >>> 8);

		// size[0] = (byte) ((s >> 8) & 0xff);
		// size[1] = (byte) (s & 0x00ff);
		return size;
	}

	public static short[] hexStr2short(String hexStr) {
		byte[] data = hexStringTobyte(hexStr);
		short[] size = new short[4];
		for (int i = 0; i < size.length; i++) {
			size[i] = getShort(data[i * 2], data[i * 2 + 1]);
		}
		return size;
	}

	public static short getShort(byte b1, byte b2) {
		short temp = 0;
		temp |= (b1 & 0xff);
		temp <<= 8;
		temp |= (b2 & 0xff);
		return temp;
	}

}
