/**
 * @Title: ToastUtil.java
 * @Package: com.jason.fingerprint.utils
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月24日 下午8:14:35
 * @Version: V1.0
 */
package com.jason.fingerprint.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * @ClassName: ToastUtil
 * @Description: Toast 提示框帮助类
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月24日 下午8:14:35
 */
public class ToastUtil {
	
	public static void showToast(Context context,String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}
	
	public static void showToast(Context context,int resId) {
       Toast.makeText(context, resId, Toast.LENGTH_SHORT).show();
	}
}
