/**
 * @Title: DialogFactory.java
 * @Package: com.jason.fingerprint.utils
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月24日 下午8:13:50
 * @Version: V1.0
 */
package com.jason.fingerprint.utils;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;

/**
 * @ClassName: DialogFactory
 * @Description: 弹出框帮助类
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月24日 下午8:13:50
 */
public class DialogFactory {
	
	public static Dialog createDialog(Context context,int layout,int theme) {
		Dialog dialog = new Dialog(context,theme);
		dialog.setContentView(layout);
		return  dialog;
	}
	
	public static ProgressDialog createWaitProgressDialog(Context context,int message){
		ProgressDialog progress = new ProgressDialog(context);
//		progress.setIcon(R.drawable.write_success);
		progress.setMessage(context.getResources().getString(message));
		return progress;
	}
	
}
