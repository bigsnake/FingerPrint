/**
 * @Title: CodecUtils.java
 * @Package: com.jason.fingerprint.utils
 * @Descripton: TODO
 * @Author: Administrator zhangyujn1989ok@gmail.com
 * @Date: 2014年10月18日 上午3:00:08
 * @Version: V1.0
 */
package com.jason.fingerprint.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import org.kymjs.aframe.utils.StringUtils;

import android.util.Base64;

/**
 * @ClassName: CodecUtils
 * @Description: 对于服务器段返回的数据进行解密处理，其目的是解决各平台编码不一致的问题
 * @Author: Administrator zhangyujn1989ok@gmail.com
 * @Date: 2014年10月18日 上午3:00:08
 */
public class CodecUtils {
	
	/***
	 * 四个二进制数组转换为16进制
	 * @Descripton: TODO
	 * @Param @param “111110011000100110110100101010”
	 * @Param @return
	 * @Return $ “1046637866”
	 * @Throws
	 */
	public static String conver8ByteTo10String(int[] sn){
		return conver16StringTo10String(conver8ByteTo16String(sn));
	}
	
	/***
	 * 四个二进制数组转换为16进制
	 * @Descripton: TODO
	 * @Param @param “111110011000100110110100101010”
	 * @Param @return
	 * @Return $ “3e626d2a”
	 * @Throws
	 */
	public static String conver8ByteTo16String(int[] sn){
		String result = "";
		result = Integer.toHexString(sn[3]&0xFF).toUpperCase()+
		Integer.toHexString(sn[2]&0xFF).toUpperCase()+
		Integer.toHexString(sn[1]&0xFF).toUpperCase()+
		Integer.toHexString(sn[0]&0xFF).toUpperCase();
		return result;
	}
	
	/***
	 * “3e626d2a”
	 */
	public static String conver16StringTo10String(String str){
		String result = null;
		try {
			result = Long.toString(Long.parseLong(str.trim(), 16));
		} catch (Exception e) {
			return null;
		}
		if (!StringUtils.isEmpty(result)) {
			int length = result.length();
			if (length < 10) {
				int num = 10 - length;
				String str0 = "";
				for (int i = 0; i < num; i++) {
					str0 = str0 + 0;
				}
				
				return str0 + result;
			}else if(length == 10){
				return result;
			}
		}
		return null;
	}

	// 加密
	public static String base64Encode(String str){
		//byte[] info = Base64.encode(str.getBytes(), Base64.DEFAULT);
		if (StringUtils.isEmpty(str)) {
			return null;
		}
		try {
			return URLEncoder.encode(str, "utf-8");
		} catch (Exception e) {
			return null;
		}
		//return new String(info);
	}

	// 解密
	public static String base64Decode(String str) {
		//byte[] info = Base64.decode(str, Base64.DEFAULT);
		if (!StringUtils.isEmpty(str)) {
			try {
				return URLDecoder.decode(str, "utf-8");
			} catch (UnsupportedEncodingException e) {
				return null;
			}
			
		}else{
			return null;
		}
		//return new String(info);
	}
	
	//加密--byte转String
	public static String base64EncodeToString(byte[] b){
		return Base64.encodeToString(b, Base64.DEFAULT);
	}
	
	//字符串解密byte[] 型
	public static byte[] base64DecodeToByte(String str){
		try {
			return Base64.decode(str, Base64.DEFAULT);
		} catch (Exception e) {
			return null;
		}
	}

}
