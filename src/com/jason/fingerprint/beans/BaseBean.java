/**
 * @Title: BaseBean.java
 * @Package: com.jason.fingerprint.beans
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月18日 下午3:55:52
 * @Version: V1.0
 */
package com.jason.fingerprint.beans;

import java.io.Serializable;

import org.kymjs.aframe.utils.StringUtils;

/**
 * @ClassName: BaseBean
 * @Description: 所有实体的基类
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月18日 下午3:55:52
 */
public class BaseBean implements Serializable {

	private static final long serialVersionUID = 5192581740408629321L;

	private String respMsg;// 响应的错误信息

	private String respCode;// 响应的错误码

	private int currentIndex;// 当前页

	private int offset;// 每页显示多少条

	// 总数据量
	private int totalRowsCount;

	// 总页数
	private int totalPagesCount;

	// 前一页页码
	private int previous;

	// 后一页页码
	private int next;
	
	/**
	 * @return the respMsg
	 */
	public String getRespMsg() {
		return respMsg;
	}



	/**
	 * @param respMsg the respMsg to set
	 */
	public void setRespMsg(String respMsg) {
		this.respMsg = respMsg;
	}



	/**
	 * @return the respCode
	 */
	public String getRespCode() {
		return respCode;
	}



	/**
	 * @param respCode the respCode to set
	 */
	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}



	/**
	 * @return the currentIndex
	 */
	public int getCurrentIndex() {
		return currentIndex;
	}



	/**
	 * @param currentIndex the currentIndex to set
	 */
	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}



	/**
	 * @return the offset
	 */
	public int getOffset() {
		return offset;
	}



	/**
	 * @param offset the offset to set
	 */
	public void setOffset(int offset) {
		this.offset = offset;
	}

	/**
	 * @return the totalRowsCount
	 */
	public int getTotalRowsCount() {
		return totalRowsCount;
	}

	/**
	 * @param totalRowsCount the totalRowsCount to set
	 */
	public void setTotalRowsCount(int totalRowsCount) {
		this.totalRowsCount = totalRowsCount;
	}

	/**
	 * @return the totalPagesCount
	 */
	public int getTotalPagesCount() {
		return totalPagesCount;
	}

	/**
	 * @param totalPagesCount the totalPagesCount to set
	 */
	public void setTotalPagesCount(int totalPagesCount) {
		this.totalPagesCount = totalPagesCount;
	}

	/**
	 * @return the previous
	 */
	public int getPrevious() {
		return previous;
	}

	/**
	 * @param previous the previous to set
	 */
	public void setPrevious(int previous) {
		this.previous = previous;
	}

	/**
	 * @return the next
	 */
	public int getNext() {
		return next;
	}

	/**
	 * @param next the next to set
	 */
	public void setNext(int next) {
		this.next = next;
	}

	public boolean isSuccess() {
		return !StringUtils.isEmpty(respCode) && respCode.equals("0");
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BaseBean [respMsg=" + respMsg + ", respCode=" + respCode
				+ ", currentIndex=" + currentIndex + ", offset=" + offset
				+ ", totalRowsCount=" + totalRowsCount + ", totalPagesCount="
				+ totalPagesCount + ", previous=" + previous + ", next=" + next
				+ "]";
	}

}
