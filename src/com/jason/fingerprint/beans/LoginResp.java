/**
 * @Title: LoginResp.java
 * @Package: com.jason.fingerprint.beans
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月18日 下午11:20:52
 * @Version: V1.0
 */
package com.jason.fingerprint.beans;

import java.util.List;


/**
 * @ClassName: LoginResp
 * @Description: 登录返回数据
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月18日 下午11:20:52
 */
public class LoginResp extends BaseBean {

	private static final long serialVersionUID = -7458858484947797157L;
	
	private List<UserBean> values;
	
	/***
	 * 判断当前返回的数据是否为空
	 */
	public boolean isEmpty(){
		
		return !isSuccess() || (values == null || values.isEmpty() || values.size() < 1);
	}
	
	/***
	 * 判断当期是否是最后一页
	 */
	public boolean isLastPage(){
		return getTotalPagesCount() == getCurrentIndex();
	}

	/**
	 * @return the values
	 */
	public List<UserBean> getValues() {
		return values;
	}

	/**
	 * @param values the vaWlues to set
	 */
	public void setValues(List<UserBean> values) {
		this.values = values;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LoginResp [values=" + values + ", getRespMsg()=" + getRespMsg()
				+ ", getRespCode()=" + getRespCode() + ", getCurrentIndex()="
				+ getCurrentIndex() + ", getOffset()=" + getOffset()
				+ ", getTotalRowsCount()=" + getTotalRowsCount()
				+ ", getTotalPagesCount()=" + getTotalPagesCount()
				+ ", getPrevious()=" + getPrevious() + ", getNext()="
				+ getNext() + ", isSuccess()=" + isSuccess() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}

}
