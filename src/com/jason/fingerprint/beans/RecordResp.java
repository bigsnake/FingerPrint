/**
 * @Title: RecordResp.java
 * @Package: com.jason.fingerprint.beans
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月21日 下午5:50:59
 * @Version: V1.0
 */
package com.jason.fingerprint.beans;

import java.util.List;

/**
 * @ClassName: RecordResp
 * @Description: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月21日 下午5:50:59
 */
public class RecordResp extends BaseBean {
	
	private static final long serialVersionUID = -6456759724507936719L;
	
	private List<RecordBean> values;

	/**
	 * @return the values
	 */
	public List<RecordBean> getValues() {
		return values;
	}

	/**
	 * @param values the values to set
	 */
	public void setValues(List<RecordBean> values) {
		this.values = values;
	}
	
	/***
	 * 判断当前返回的数据是否为空
	 */
	public boolean isEmpty(){
		
		return !isSuccess() || (values == null || values.isEmpty() || values.size() < 1);
	}
	
	/***
	 * 判断当期是否是最后一页
	 */
	public boolean isLastPage(){
		return getTotalPagesCount() == getCurrentIndex();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RecordResp [values=" + values + ", getRespMsg()="
				+ getRespMsg() + ", getRespCode()=" + getRespCode()
				+ ", getCurrentIndex()=" + getCurrentIndex() + ", getOffset()="
				+ getOffset() + ", getTotalRowsCount()=" + getTotalRowsCount()
				+ ", getTotalPagesCount()=" + getTotalPagesCount()
				+ ", getPrevious()=" + getPrevious() + ", getNext()="
				+ getNext() + ", isSuccess()=" + isSuccess() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}
	
}
