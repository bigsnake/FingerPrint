package com.jason.fingerprint.beans;

import java.util.List;

/**
 * @ClassName: SigninRecordResp
 * @Description: 签到响应结果
 * @Author: John zhuqiangping@gmail.com
 * @Date: 2014年11月24日
 */
public class SigninRecordResp extends BaseBean {
	
	// 返回结果
	private List<String> values;
	
	/**
	 * @return the values
	 */
	public List<String> getValues() {
		return values;
	}

	/**
	 * @param values the values list to set
	 */
	public void setValues(List<String> values) {
		this.values = values;
	}
	
	/***
	 * 判断当前返回的数据是否为空
	 */
	public boolean isEmpty(){
		
		return !isSuccess() || (values == null || values.isEmpty() || values.size() < 1);
	}
	
	/***
	 * 判断当期是否是最后一页
	 */
	public boolean isLastPage(){
		return getTotalPagesCount() == getCurrentIndex();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SigninRecordResp [values=" + values + ", getValues()="
				+ getValues() + ", isEmpty()=" + isEmpty() + ", isLastPage()="
				+ isLastPage() + ", getRespMsg()=" + getRespMsg()
				+ ", getRespCode()=" + getRespCode() + ", getCurrentIndex()="
				+ getCurrentIndex() + ", getOffset()=" + getOffset()
				+ ", getTotalRowsCount()=" + getTotalRowsCount()
				+ ", getTotalPagesCount()=" + getTotalPagesCount()
				+ ", getPrevious()=" + getPrevious() + ", getNext()="
				+ getNext() + ", isSuccess()=" + isSuccess() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}

}
