package com.jason.fingerprint.beans;

import java.io.Serializable;
import java.util.Date;

import org.kymjs.aframe.database.annotate.Id;
import org.kymjs.aframe.database.annotate.Table;

/**
 * 用户信息Bean
 * 
 * @author 薛会生
 * 
 */
@Table(name = "table_userBean")
public class UserBean implements Serializable {

	@Id()
	private int id;

	private int userId;// 用户Id

	private String userNo;// 用户编号

	private String loginName;// 用户登录名

	private String userName;// 用户真实姓名

	private String loginPwd;// 用户登录密码

	private String keyID;// CAID

	private String email;// email

	private String mobile;// 手机号

	private String telPhone;// 联系电话

	private int roleId;// 角色ID

	private int organId;// 隶属机构ID
	
	private String organ;//机构名称

	private Date createDate;// 建立时间

	private String state;// 状态 0 停用; 1 有效; 2 删除

	private String sex;// 性别 1、男; 0、女

	private String birthdayDate;// 出生日期

	private String edu;// 文化程度 0 文盲; 1 小学; 2 初中; 3 高中; 4 大专; 5 本科; 6 研究生

	private String teachFlag;// 是否培训 0 未培训; 1 已培训

	private int teachCount;// 培训次数

	private String honorFlag;// 是否表彰 0 未受表彰; 1 收到表彰

	private int honorCount;// 表彰次数

	private String workUnit;// 工作单位

	private String position;// 职务

	private String familyAdd;// 家庭住址

	private String personIdentity;// 人员身份

	private String userType;// 判断人员 1 用户管理 2 工作人员

	private int isDelete;// 能否删除矫正人员信息

	private String fingerprint1;// 工作人员指纹1

	private String fingerprint2;// 工作人员指纹2
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return the userNo
	 */
	public String getUserNo() {
		return userNo;
	}

	/**
	 * @param userNo the userNo to set
	 */
	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	/**
	 * @return the loginName
	 */
	public String getLoginName() {
		return loginName;
	}

	/**
	 * @param loginName the loginName to set
	 */
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the loginPwd
	 */
	public String getLoginPwd() {
		return loginPwd;
	}

	/**
	 * @param loginPwd the loginPwd to set
	 */
	public void setLoginPwd(String loginPwd) {
		this.loginPwd = loginPwd;
	}

	/**
	 * @return the keyID
	 */
	public String getKeyID() {
		return keyID;
	}

	/**
	 * @param keyID the keyID to set
	 */
	public void setKeyID(String keyID) {
		this.keyID = keyID;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return the telPhone
	 */
	public String getTelPhone() {
		return telPhone;
	}

	/**
	 * @param telPhone the telPhone to set
	 */
	public void setTelPhone(String telPhone) {
		this.telPhone = telPhone;
	}

	/**
	 * @return the roleId
	 */
	public int getRoleId() {
		return roleId;
	}

	/**
	 * @param roleId the roleId to set
	 */
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the sex
	 */
	public String getSex() {
		return sex;
	}

	/**
	 * @param sex the sex to set
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}

	/**
	 * @return the birthdayDate
	 */
	public String getBirthdayDate() {
		return birthdayDate;
	}

	/**
	 * @param birthdayDate the birthdayDate to set
	 */
	public void setBirthdayDate(String birthdayDate) {
		this.birthdayDate = birthdayDate;
	}

	/**
	 * @return the edu
	 */
	public String getEdu() {
		return edu;
	}

	/**
	 * @param edu the edu to set
	 */
	public void setEdu(String edu) {
		this.edu = edu;
	}

	/**
	 * @return the teachFlag
	 */
	public String getTeachFlag() {
		return teachFlag;
	}

	/**
	 * @param teachFlag the teachFlag to set
	 */
	public void setTeachFlag(String teachFlag) {
		this.teachFlag = teachFlag;
	}

	/**
	 * @return the teachCount
	 */
	public int getTeachCount() {
		return teachCount;
	}

	/**
	 * @param teachCount the teachCount to set
	 */
	public void setTeachCount(int teachCount) {
		this.teachCount = teachCount;
	}

	/**
	 * @return the honorFlag
	 */
	public String getHonorFlag() {
		return honorFlag;
	}

	/**
	 * @param honorFlag the honorFlag to set
	 */
	public void setHonorFlag(String honorFlag) {
		this.honorFlag = honorFlag;
	}

	/**
	 * @return the honorCount
	 */
	public int getHonorCount() {
		return honorCount;
	}

	/**
	 * @param honorCount the honorCount to set
	 */
	public void setHonorCount(int honorCount) {
		this.honorCount = honorCount;
	}

	/**
	 * @return the workUnit
	 */
	public String getWorkUnit() {
		return workUnit;
	}

	/**
	 * @param workUnit the workUnit to set
	 */
	public void setWorkUnit(String workUnit) {
		this.workUnit = workUnit;
	}

	/**
	 * @return the position
	 */
	public String getPosition() {
		return position;
	}

	/**
	 * @param position the position to set
	 */
	public void setPosition(String position) {
		this.position = position;
	}

	/**
	 * @return the familyAdd
	 */
	public String getFamilyAdd() {
		return familyAdd;
	}

	/**
	 * @param familyAdd the familyAdd to set
	 */
	public void setFamilyAdd(String familyAdd) {
		this.familyAdd = familyAdd;
	}

	/**
	 * @return the personIdentity
	 */
	public String getPersonIdentity() {
		return personIdentity;
	}

	/**
	 * @param personIdentity the personIdentity to set
	 */
	public void setPersonIdentity(String personIdentity) {
		this.personIdentity = personIdentity;
	}

	/**
	 * @return the userType
	 */
	public String getUserType() {
		return userType;
	}

	/**
	 * @param userType the userType to set
	 */
	public void setUserType(String userType) {
		this.userType = userType;
	}

	/**
	 * @return the isDelete
	 */
	public int getIsDelete() {
		return isDelete;
	}

	/**
	 * @param isDelete the isDelete to set
	 */
	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}

	/**
	 * @return the fingerprint1
	 */
	public String getFingerprint1() {
		return fingerprint1;
	}

	/**
	 * @param fingerprint1 the fingerprint1 to set
	 */
	public void setFingerprint1(String fingerprint1) {
		this.fingerprint1 = fingerprint1;
	}

	/**
	 * @return the fingerprint2
	 */
	public String getFingerprint2() {
		return fingerprint2;
	}

	/**
	 * @param fingerprint2 the fingerprint2 to set
	 */
	public void setFingerprint2(String fingerprint2) {
		this.fingerprint2 = fingerprint2;
	}

	/**
	 * @return the organId
	 */
	public int getOrganId() {
		return organId;
	}

	/**
	 * @param organId the organId to set
	 */
	public void setOrganId(int organId) {
		this.organId = organId;
	}

	/**
	 * @return the organ
	 */
	public String getOrgan() {
		return organ;
	}

	/**
	 * @param organ the organ to set
	 */
	public void setOrgan(String organ) {
		this.organ = organ;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UserBean [id=" + id + ", userId=" + userId + ", userNo="
				+ userNo + ", loginName=" + loginName + ", userName="
				+ userName + ", loginPwd=" + loginPwd + ", keyID=" + keyID
				+ ", email=" + email + ", mobile=" + mobile + ", telPhone="
				+ telPhone + ", roleId=" + roleId + ", organId=" + organId
				+ ", organ=" + organ + ", createDate=" + createDate
				+ ", state=" + state + ", sex=" + sex + ", birthdayDate="
				+ birthdayDate + ", edu=" + edu + ", teachFlag=" + teachFlag
				+ ", teachCount=" + teachCount + ", honorFlag=" + honorFlag
				+ ", honorCount=" + honorCount + ", workUnit=" + workUnit
				+ ", position=" + position + ", familyAdd=" + familyAdd
				+ ", personIdentity=" + personIdentity + ", userType="
				+ userType + ", isDelete=" + isDelete + ", fingerprint1="
				+ fingerprint1 + ", fingerprint2=" + fingerprint2 + "]";
	}

}
