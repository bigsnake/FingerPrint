/**
 * @Title: InsertOrUpdateRecordResp.java
 * @Package: com.jason.fingerprint.beans
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年12月2日 下午10:43:29
 * @Version: V1.0
 */
package com.jason.fingerprint.beans;

import java.util.List;

/**
 * @ClassName: InsertOrUpdateRecordResp
 * @Description: 矫正人员信息修改或者添加的响应类
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年12月2日 下午10:43:29
 */
public class InsertOrUpdateRecordResp extends BaseBean {

	private static final long serialVersionUID = 4572726728865638845L;
	
	private List<String> values;

	/**
	 * @return the values
	 */
	public List<String> getValues() {
		return values;
	}

	/**
	 * @param values the values to set
	 */
	public void setValues(List<String> values) {
		this.values = values;
	}
	
	/***
	 * 判断当前返回的数据是否为空
	 */
	public boolean isNotEmpty(){
		
		return isSuccess() && (values != null && !values.isEmpty() && values.size() > 0);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "InsertOrUpdateRecordResp [values=" + values + ", getRespMsg()="
				+ getRespMsg() + ", getRespCode()=" + getRespCode()
				+ ", getCurrentIndex()=" + getCurrentIndex() + ", getOffset()="
				+ getOffset() + ", getTotalRowsCount()=" + getTotalRowsCount()
				+ ", getTotalPagesCount()=" + getTotalPagesCount()
				+ ", getPrevious()=" + getPrevious() + ", getNext()="
				+ getNext() + ", isSuccess()=" + isSuccess() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}
	
}
