/**
 * @Title: VersionBean.java
 * @Package: com.jason.fingerprint.beans
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2015年1月18日 下午8:24:11
 * @Version: V1.0
 */
package com.jason.fingerprint.beans;

import java.io.Serializable;

/**
 * @ClassName: VersionBean
 * @Description: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2015年1月18日 下午8:24:11
 */
public class VersionBean implements Serializable {

	private static final long serialVersionUID = -4599882984585486925L;
	
	private String id; //主键ID
	
	private String content; // 更新内容
	
	private String updateTime; // 更新时间
	
	private String versionNum; //版本号
	
	private String position; //文件位置

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the updateTime
	 */
	public String getUpdateTime() {
		return updateTime;
	}

	/**
	 * @param updateTime the updateTime to set
	 */
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return the versionNum
	 */
	public String getVersionNum() {
		return versionNum;
	}

	/**
	 * @param versionNum the versionNum to set
	 */
	public void setVersionNum(String versionNum) {
		this.versionNum = versionNum;
	}

	/**
	 * @return the position
	 */
	public String getPosition() {
		return position;
	}

	/**
	 * @param position the position to set
	 */
	public void setPosition(String position) {
		this.position = position;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "VersionBean [id=" + id + ", content=" + content
				+ ", updateTime=" + updateTime + ", versionNum=" + versionNum
				+ ", position=" + position + "]";
	}
	
}
