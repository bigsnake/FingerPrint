/**
 * @Title: RecordBean.java
 * @Package: com.jason.fingerprint.beans
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月21日 下午6:58:59
 * @Version: V1.0
 */
package com.jason.fingerprint.beans;

import java.io.Serializable;

import org.kymjs.aframe.database.annotate.Id;
import org.kymjs.aframe.database.annotate.Table;
import org.kymjs.aframe.utils.StringUtils;

/**
 * @ClassName: RecordBean
 * @Description: 矫正人员信息
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月21日 下午6:58:59
 */
@Table(name = "table_record")
public class RecordBean implements Serializable {
	
	@Id()
	private int id;	//存入数据库的主键id
	
	/**
	 * 操作类型 0代表新增,1代表修改
	 */
	private String type;
	/**
	 * 人员ID
	 */
	private String electronicsId;
	/**
	 * 姓名
	 */
	private String name;
	/**
	 * 手机号码
	 */
	private String telephone;
	/**
	 * 身份证号
	 */
	private String identityNO;
	/**
	 * RFID卡号
	 */
	private String cardNO;
	/**
	 * 隶属机构ID
	 */
	private String registeredId;
	/**
	 * 隶属机构名称
	 */
	private String registered;
	/**
	 * 指纹1
	 */
	private String fp_1;
	/**
	 * 指纹2
	 */
	private String fp_2;
	/**
	 * IMIE号
	 * 
	 */
	private String imie;
	
	private int isEntry;//指纹是否已经录入，0未录入，1录入，默认0
	

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the electronicsId
	 */
	public String getElectronicsId() {
		return electronicsId;
	}

	/**
	 * @param electronicsId the electronicsId to set
	 */
	public void setElectronicsId(String electronicsId) {
		this.electronicsId = electronicsId;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the telephone
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * @param telephone the telephone to set
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	/**
	 * @return the identityNO
	 */
	public String getIdentityNO() {
		return identityNO;
	}

	/**
	 * @param identityNO the identityNO to set
	 */
	public void setIdentityNO(String identityNO) {
		this.identityNO = identityNO;
	}

	/**
	 * @return the registeredId
	 */
	public String getRegisteredId() {
		return registeredId;
	}

	/**
	 * @param registeredId the registeredId to set
	 */
	public void setRegisteredId(String registeredId) {
		this.registeredId = registeredId;
	}

	/**
	 * @return the registered
	 */
	public String getRegistered() {
		return registered;
	}

	/**
	 * @param registered the registered to set
	 */
	public void setRegistered(String registered) {
		this.registered = registered;
	}
	
	/**
	 * @return type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type 要设置的 type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return cardNO
	 */
	public String getCardNO() {
		return cardNO;
	}

	/**
	 * @param cardNO 要设置的 cardNO
	 */
	public void setCardNO(String cardNO) {
		this.cardNO = cardNO;
	}

	/**
	 * @return fp_1
	 */
	public String getFp_1() {
		return fp_1;
	}

	/**
	 * @param fp_1 要设置的 fp_1
	 */
	public void setFp_1(String fp_1) {
		this.fp_1 = fp_1;
	}

	/**
	 * @return fp_2
	 */
	public String getFp_2() {
		return fp_2;
	}

	/**
	 * @param fp_2 要设置的 fp_2
	 */
	public void setFp_2(String fp_2) {
		this.fp_2 = fp_2;
	}

	/**
	 * @return imie
	 */
	public String getImie() {
		return imie;
	}

	/**
	 * @param imie 要设置的 imie
	 */
	public void setImie(String imie) {
		this.imie = imie;
	}
	
	/**
	 * @return isEntry
	 */
	public int getIsEntry() {
		
		return StringUtils.isEmpty(fp_1) && StringUtils.isEmpty(fp_2) ? 0 : 1;
	}

	/**
	 * @param isEntry 要设置的 isEntry
	 */
	public void setIsEntry(int isEntry) {
		this.isEntry = isEntry;
	}

	/*
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RecordBean [id=" + id + ", type=" + type + ", electronicsId=" + electronicsId + ", name=" + name
				+ ", telephone=" + telephone + ", identityNO=" + identityNO + ", cardNO=" + cardNO + ", registeredId="
				+ registeredId + ", registered=" + registered + ", fp_1=" + fp_1 + ", fp_2=" + fp_2 + ", imie=" + imie
				+ ", isEntry=" + isEntry + "]";
	}

}
