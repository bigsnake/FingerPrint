package com.jason.fingerprint.beans;

import java.io.Serializable;

import org.kymjs.aframe.database.annotate.Id;
import org.kymjs.aframe.database.annotate.Table;

/**
 * 
 * @ClassName: SponsorInfo
 * @Description: 该类描述了社区服务、集中教育详细信息
 * @author John zhuqiangping@gmail.com
 * @Date: 2014年11月18日
 */
@Table(name = "table_sponsorInfo")
public class SponsorInfo implements Serializable {

	@Id()
	private int id;

	private String sponsorId; // 集中教育、劳动发起表ID

	private String time; // 发起时间

	private String content; // 发起内容

	private String address; // 地点

	private String type; // 发起类型（0:集中教育，1：社区服务）

	private String organName; // 活动发起组织单位

	public SponsorInfo() {
		// nothing
	}

	public SponsorInfo(SponsorBean bean) {
		this.sponsorId = String.valueOf(bean.getId());
		this.time = bean.getTime();
		this.address = bean.getAddress();
		this.content = bean.getContent();
		this.type = bean.getType();
		this.organName = bean.getOrganName();
	}

	public String getSponsorId() {
		return sponsorId;
	}

	public void setSponsorId(String sponsorId) {
		this.sponsorId = sponsorId;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOrganName() {
		return organName;
	}

	public void setOrganName(String organName) {
		this.organName = organName;
	}

	public int getId() {
		return this.id;
	}
}
