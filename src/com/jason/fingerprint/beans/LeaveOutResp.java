/**
 * @Title: LeaveOutResp.java
 * @Package: com.jason.fingerprint.beans
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月18日 下午5:36:58
 * @Version: V1.0
 */
package com.jason.fingerprint.beans;

import java.util.List;

/**
 * @ClassName: LeaveOutResp
 * @Description: 请销假响应结果
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月18日 下午5:36:58
 */
public class LeaveOutResp extends BaseBean {

	private static final long serialVersionUID = -5755583799200855752L;
	
	private List<LeaveOutBean> values;
	
	/**
	 * @return the values
	 */
	public List<LeaveOutBean> getValues() {
		return values;
	}

	/**
	 * @param values the values to set
	 */
	public void setValues(List<LeaveOutBean> values) {
		this.values = values;
	}
	
	/***
	 * 判断当前返回的数据是否为空
	 */
	public boolean isEmpty(){
		
		return !isSuccess() || (values == null || values.isEmpty() || values.size() < 1);
	}
	
	/***
	 * 判断当期是否是最后一页
	 */
	public boolean isLastPage(){
		return getTotalPagesCount() == getCurrentIndex();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LeaveOutResp [values=" + values + ", getValues()="
				+ getValues() + ", isEmpty()=" + isEmpty() + ", isLastPage()="
				+ isLastPage() + ", getRespMsg()=" + getRespMsg()
				+ ", getRespCode()=" + getRespCode() + ", getCurrentIndex()="
				+ getCurrentIndex() + ", getOffset()=" + getOffset()
				+ ", getTotalRowsCount()=" + getTotalRowsCount()
				+ ", getTotalPagesCount()=" + getTotalPagesCount()
				+ ", getPrevious()=" + getPrevious() + ", getNext()="
				+ getNext() + ", isSuccess()=" + isSuccess() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}

	
	
}
