/**
 * @Title: FingerPrintBean.java
 * @Package: com.jason.fingerprint.beans.ui
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月12日 上午10:27:19
 * @Version: V1.0
 */
package com.jason.fingerprint.beans.ui;

import java.io.Serializable;

import org.kymjs.aframe.database.annotate.Id;
import org.kymjs.aframe.database.annotate.Table;

/**
 * @ClassName: FingerPrintBean
 * @Description: 所有用户的指纹信息，包括工作人员和矫正人员
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月12日 上午10:27:19
 */
@Table(name = "table_fingerPrint")
public class FingerPrintBean implements Serializable {

	@Id()
	private int id;
	
	private String userId;//用户Id
	
	private String userName;//用户名字
	
	private String fp;
	
	private String cardNo;//监管卡号
	
	private int type;//1,矫正人员，2，工作人员
	
	private int state;//1指纹一，2指纹二
	
	
	/**
	 * @Descripton: TODO
	 * @Param 
	 * @Return $
	 * @Throws 
	 */
	public FingerPrintBean() {
		super();
	}

	/**
	 * @Descripton: TODO
	 * @Param @param userId
	 * @Param @param fp
	 * @Param @param type
	 * @Param @param state
	 * @Return $
	 * @Throws 
	 */
	public FingerPrintBean(String userId, String fp, int type, int state) {
		super();
		this.userId = userId;
		this.fp = fp;
		this.type = type;
		this.state = state;
	}
	
	public FingerPrintBean(String userId,String userName, String fp, int type, int state) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.fp = fp;
		this.type = type;
		this.state = state;
	}
	
	/**
	 * @Descripton: TODO
	 * @Param @param userId
	 * @Param @param userName
	 * @Param @param fp
	 * @Param @param cardNo
	 * @Param @param type
	 * @Param @param state
	 * @Return $
	 * @Throws 
	 */
	public FingerPrintBean(String userId, String userName, String fp,
			String cardNo, int type, int state) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.fp = fp;
		this.cardNo = cardNo;
		this.type = type;
		this.state = state;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the fp
	 */
	public String getFp() {
		return fp;
	}

	/**
	 * @param fp the fp to set
	 */
	public void setFp(String fp) {
		this.fp = fp;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * @return the state
	 */
	public int getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(int state) {
		this.state = state;
	}
	
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	/**
	 * @return the cardNo
	 */
	public String getCardNo() {
		return cardNo;
	}

	/**
	 * @param cardNo the cardNo to set
	 */
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FingerPrintBean [id=" + id + ", userId=" + userId
				+ ", userName=" + userName + ", fp=" + fp + ", cardNo="
				+ cardNo + ", type=" + type + ", state=" + state + "]";
	}
	
}
