/**
 * @Title: package-info.java
 * @Package: com.jason.fingerprint.beans.ui
 * @Descripton: ui中所使用的bean
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月19日 上午9:31:59
 * @Version: V1.0
 */

package com.jason.fingerprint.beans.ui;