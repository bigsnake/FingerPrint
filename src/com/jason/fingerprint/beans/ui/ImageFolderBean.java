/**
 * @Title: ImageFolderBean.java
 * @Package: com.jason.fingerprint.beans
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月26日 下午2:12:07
 * @Version: V1.0
 */
package com.jason.fingerprint.beans.ui;

import java.io.Serializable;

/**
 * @ClassName: ImageFolderBean
 * @Description: 图片选取文件夹信息
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月26日 下午2:12:07
 */
public class ImageFolderBean implements Serializable {

	private static final long serialVersionUID = -3850361829985380202L;
	
	private String topImagePath;//文件夹的第一张图片路径
	
	private String folderName; //文件夹名 
	
	private int imageCounts;  //文件夹中的图片数

	/**
	 * @return the topImagePath
	 */
	public String getTopImagePath() {
		return topImagePath;
	}

	/**
	 * @param topImagePath the topImagePath to set
	 */
	public void setTopImagePath(String topImagePath) {
		this.topImagePath = topImagePath;
	}

	/**
	 * @return the folderName
	 */
	public String getFolderName() {
		return folderName;
	}

	/**
	 * @param folderName the folderName to set
	 */
	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	/**
	 * @return the imageCounts
	 */
	public int getImageCounts() {
		return imageCounts;
	}

	/**
	 * @param imageCounts the imageCounts to set
	 */
	public void setImageCounts(int imageCounts) {
		this.imageCounts = imageCounts;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ImageFolderBean [topImagePath=" + topImagePath
				+ ", folderName=" + folderName + ", imageCounts=" + imageCounts
				+ "]";
	}
	

}
