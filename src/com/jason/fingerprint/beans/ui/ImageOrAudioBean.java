/**
 * @Title: ImageOrAudioBean.java
 * @Package: com.jason.fingerprint.beans
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月26日 上午11:30:27
 * @Version: V1.0
 */
package com.jason.fingerprint.beans.ui;

import java.io.Serializable;

import org.kymjs.aframe.database.annotate.Id;
import org.kymjs.aframe.database.annotate.Table;

/**
 * @ClassName: ImageOrAudioBean
 * @Description: 本地保存的图片或者录音的信息
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月26日 上午11:30:27
 */
@Table(name = "table_imageAudio")
public class ImageOrAudioBean implements Serializable {

	@Id()
	private int id;
	
	//活动类型，0,-集中劳动，1-个别劳动，2-集中教育，3-个别教育,4-日常报到，5-工作走访，6-外出请假
	private int serviceId;//分发服务ID,1-签到，2-新增个别教育、个别劳动，3-集中劳动 ，4-走访记录，5-外出请假
	
	private String sponsorId; // 集中教育、劳动发起表ID
	
	private String sponsorType; // 活动类型
	
	private String fileName;
	
	private String filePath;//资源本地路径，可以确认MediaStore图片的唯一
	
	private String fileUrl;//资源网络url
	
	private int fileType;//资源类型，1-默认"image",2-"audio"

	private String createDate; //资源建立时间，格式yyyy-MM-dd HH:mm:ss
	
	private String uploadDate; //资源上传时间，格式yyyy-MM-dd HH:mm:ss
	
	private String height;//图片高
	
	private String width;//图片宽
	
	private String size;//图片尺寸，byte
	
	private int state;//资源当前状态，1-默认，"未上传"，2-"已上传"
	
	private String parentId;//工作走访或者外出请假id,日常报到
	
	private String imie;
	
	private String signType;//签到类型(1:请假签到 3:销假签到)
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the parentId
	 */
	public String getParentId() {
		return parentId;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the serviceId
	 */
	public int getServiceId() {
		return serviceId;
	}

	/**
	 * @param serviceId the serviceId to set
	 */
	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	/**
	 * @return the sponsorId
	 */
	public String getSponsorId() {
		return sponsorId;
	}

	/**
	 * @param sponsorId the sponsorId to set
	 */
	public void setSponsorId(String sponsorId) {
		this.sponsorId = sponsorId;
	}

	/**
	 * @return the sponsorType
	 */
	public String getSponsorType() {
		return sponsorType;
	}

	/**
	 * @param sponsorType the sponsorType to set
	 */
	public void setSponsorType(String sponsorType) {
		this.sponsorType = sponsorType;
	}
	
	/**
	 * @return the imie
	 */
	public String getImie() {
		return imie;
	}

	/**
	 * @param imie the imie to set
	 */
	public void setImie(String imie) {
		this.imie = imie;
	}

	/**
	 * @param parentId the parentId to set
	 */
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	/**
	 * @return the signType
	 */
	public String getSignType() {
		return signType;
	}

	/**
	 * @param signType the signType to set
	 */
	public void setSignType(String signType) {
		this.signType = signType;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return the filePath
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * @param filePath the filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	/**
	 * @return the fileUrl
	 */
	public String getFileUrl() {
		return fileUrl;
	}

	/**
	 * @param fileUrl the fileUrl to set
	 */
	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}

	/**
	 * @return the fileType
	 */
	public int getFileType() {
		return fileType;
	}

	/**
	 * @param fileType the fileType to set
	 */
	public void setFileType(int fileType) {
		this.fileType = fileType;
	}

	/**
	 * @return the createDate
	 */
	public String getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	/**
	 * @return the uploadDate
	 */
	public String getUploadDate() {
		return uploadDate;
	}

	/**
	 * @param uploadDate the uploadDate to set
	 */
	public void setUploadDate(String uploadDate) {
		this.uploadDate = uploadDate;
	}

	/**
	 * @return the state
	 */
	public int getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(int state) {
		this.state = state;
	}

	/**
	 * @return height
	 */
	public String getHeight() {
		return height;
	}

	/**
	 * @param height 要设置的 height
	 */
	public void setHeight(String height) {
		this.height = height;
	}

	/**
	 * @return width
	 */
	public String getWidth() {
		return width;
	}

	/**
	 * @param width 要设置的 width
	 */
	public void setWidth(String width) {
		this.width = width;
	}

	/**
	 * @return size
	 */
	public String getSize() {
		return size;
	}

	/**
	 * @param size 要设置的 size
	 */
	public void setSize(String size) {
		this.size = size;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ImageOrAudioBean [id=" + id + ", serviceId=" + serviceId
				+ ", sponsorId=" + sponsorId + ", sponsorType=" + sponsorType
				+ ", fileName=" + fileName + ", filePath=" + filePath
				+ ", fileUrl=" + fileUrl + ", fileType=" + fileType
				+ ", createDate=" + createDate + ", uploadDate=" + uploadDate
				+ ", height=" + height + ", width=" + width + ", size=" + size
				+ ", state=" + state + ", parentId=" + parentId + ", imie="
				+ imie + ", signType=" + signType + "]";
	}

}
