/**
 * @Title: LocationBean.java
 * @Package: com.jason.fingerprint.beans.ui
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月4日 下午9:28:47
 * @Version: V1.0
 */
package com.jason.fingerprint.beans.ui;

import java.io.Serializable;

/**
 * @ClassName: LocationBean
 * @Description: 获取当前位置
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月4日 下午9:28:47
 */
public class LocationBean implements Serializable {

	private static final long serialVersionUID = 9102713140815660823L;

	private String address;//地点
	
	private String latitude;// 纬度
	
	private String lontitude;// 经度
	

	/**
	 * @Descripton: TODO
	 * @Param 
	 * @Return $
	 * @Throws 
	 */
	public LocationBean() {
		super();
	}

	/**
	 * @Descripton: TODO
	 * @Param @param address
	 * @Param @param latitude
	 * @Param @param lontitude
	 * @Return $
	 * @Throws 
	 */
	public LocationBean(String address, String latitude, String lontitude) {
		super();
		this.address = address;
		this.latitude = latitude;
		this.lontitude = lontitude;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the latitude
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the lontitude
	 */
	public String getLontitude() {
		return lontitude;
	}

	/**
	 * @param lontitude the lontitude to set
	 */
	public void setLontitude(String lontitude) {
		this.lontitude = lontitude;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LocationBean [address=" + address + ", latitude=" + latitude
				+ ", lontitude=" + lontitude + "]";
	}
	
}
