package com.jason.fingerprint.beans.ui;

import java.io.Serializable;

/****
 * 
 * @ClassName: ModelBean
 * @Description: 模块实体类
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月19日 下午1:27:08
 */
public class ModelBean implements Serializable{
	
	private static final long serialVersionUID = -5744464109219968959L;

	private String title;	//模块模块名字
	
	private int iconId;		//模块图标
	
	
	/**
	 * @Descripton: TODO
	 * @Param 
	 * @Return $
	 * @Throws 
	 */
	public ModelBean() {
		super();
	}

	/**
	 * @Descripton: TODO
	 * @Param @param title
	 * @Param @param iconId
	 * @Return $
	 * @Throws 
	 */
	public ModelBean(String title, int iconId) {
		super();
		this.title = title;
		this.iconId = iconId;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the iconId
	 */
	public int getIconId() {
		return iconId;
	}

	/**
	 * @param iconId the iconId to set
	 */
	public void setIconId(int iconId) {
		this.iconId = iconId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "IconBean [title=" + title + ", iconId=" + iconId + "]";
	}
	
	
}