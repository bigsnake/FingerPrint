/**
 * @Title: LeaveOutBean.java
 * @Package: com.jason.fingerprint.beans
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月18日 下午4:33:54
 * @Version: V1.0
 */
package com.jason.fingerprint.beans;

import java.io.Serializable;

import org.kymjs.aframe.database.annotate.Id;
import org.kymjs.aframe.database.annotate.Table;

/**
 * @ClassName: LeaveOutBean
 * @Description: 外出请假
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月18日 下午4:33:54
 */
@Table(name = "table_leaveout")
public class LeaveOutBean implements Serializable {

	@Id()
	private int leaveoutId;

	/**
	 * 请假、销假记录的ID
	 */
	private String id;
	/**
	 * 申请时间(系统生成时间)
	 */
	private String time;
	/**
	 * 是否指纹签到 (0:否 1:是)
	 */
	private String signFlag;
	/**
	 * 矫正人员人员ID
	 */
	private String rymcid;
	/**
	 * 矫正人员姓名
	 */
	private String name;
	/**
	 * 视频拍照图片
	 */
	private String photoUrl;

	/**
	 * 签到类型(0日常，1请假，2解矫，3销假)
	 */
	private String signType;
	/**
	 * 是否销假(0:否，1：是)
	 */
	private String isCancel;
	/**
	 * 请假ID(外键，关联本表ID)
	 */
	private String leaveId;
	/**
	 * 是否销假
	 */
	private String isCancelLeave;
	/**
	 * 机构ID
	 */
	private String organid;
	/**
	 * 机构名
	 */
	private String organ;
	
	private int state;//资源当前状态，1-默认，"未上传"，2-"已上传"
	
	/**
	 * @return the leaveoutId
	 */
	public int getLeaveoutId() {
		return leaveoutId;
	}
	/**
	 * @param leaveoutId the leaveoutId to set
	 */
	public void setLeaveoutId(int leaveoutId) {
		this.leaveoutId = leaveoutId;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the time
	 */
	public String getTime() {
		return time;
	}
	/**
	 * @param time the time to set
	 */
	public void setTime(String time) {
		this.time = time;
	}
	/**
	 * @return the signFlag
	 */
	public String getSignFlag() {
		return signFlag;
	}
	/**
	 * @param signFlag the signFlag to set
	 */
	public void setSignFlag(String signFlag) {
		this.signFlag = signFlag;
	}
	/**
	 * @return the rymcid
	 */
	public String getRymcid() {
		return rymcid;
	}
	/**
	 * @param rymcid the rymcid to set
	 */
	public void setRymcid(String rymcid) {
		this.rymcid = rymcid;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the photoUrl
	 */
	public String getPhotoUrl() {
		return photoUrl;
	}
	/**
	 * @param photoUrl the photoUrl to set
	 */
	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}
	/**
	 * @return the signType
	 */
	public String getSignType() {
		return signType;
	}
	/**
	 * @param signType the signType to set
	 */
	public void setSignType(String signType) {
		this.signType = signType;
	}
	/**
	 * @return the isCancel
	 */
	public String getIsCancel() {
		return isCancel;
	}
	/**
	 * @param isCancel the isCancel to set
	 */
	public void setIsCancel(String isCancel) {
		this.isCancel = isCancel;
	}
	/**
	 * @return the leaveId
	 */
	public String getLeaveId() {
		return leaveId;
	}
	/**
	 * @param leaveId the leaveId to set
	 */
	public void setLeaveId(String leaveId) {
		this.leaveId = leaveId;
	}
	/**
	 * @return the isCancelLeave
	 */
	public String getIsCancelLeave() {
		return isCancelLeave;
	}
	/**
	 * @param isCancelLeave the isCancelLeave to set
	 */
	public void setIsCancelLeave(String isCancelLeave) {
		this.isCancelLeave = isCancelLeave;
	}
	/**
	 * @return the organid
	 */
	public String getOrganid() {
		return organid;
	}
	/**
	 * @param organid the organid to set
	 */
	public void setOrganid(String organid) {
		this.organid = organid;
	}
	/**
	 * @return the organ
	 */
	public String getOrgan() {
		return organ;
	}
	/**
	 * @param organ the organ to set
	 */
	public void setOrgan(String organ) {
		this.organ = organ;
	}
	
	/**
	 * @return the state
	 */
	public int getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(int state) {
		this.state = state;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LeaveOutBean [leaveoutId=" + leaveoutId + ", id=" + id
				+ ", time=" + time + ", signFlag=" + signFlag + ", rymcid="
				+ rymcid + ", name=" + name + ", photoUrl=" + photoUrl
				+ ", signType=" + signType + ", isCancel=" + isCancel
				+ ", leaveId=" + leaveId + ", isCancelLeave=" + isCancelLeave
				+ ", organid=" + organid + ", organ=" + organ + ", state="
				+ state + "]";
	}
	
}
