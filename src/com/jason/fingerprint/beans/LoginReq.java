/**
 * @Title: LoginReq.java
 * @Package: com.jason.fingerprint.beans
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月25日 下午4:27:56
 * @Version: V1.0
 */
package com.jason.fingerprint.beans;

import java.io.Serializable;

/**
 * @ClassName: LoginReq
 * @Description: 登录请求参数
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年10月25日 下午4:27:56
 */
public class LoginReq implements Serializable {
	
	private static final long serialVersionUID = 4138011768932452374L;
	
	private String url;
	private String loginName;//登录账号
	private String loginPwd;//登录密码
	private int currentIndex; //当前页
	private int type;//登录方式，1-指纹登录，2-账号登录
	
	
	/**
	 * @Descripton: TODO
	 * @Param @param loginName
	 * @Param @param loginPwd
	 * @Param @param currentIndex
	 * @Param @param type
	 * @Param @param url
	 * @Return $
	 * @Throws 
	 */
	public LoginReq(String loginName, String loginPwd, int currentIndex,
			int type, String url) {
		super();
		this.loginName = loginName;
		this.loginPwd = loginPwd;
		this.currentIndex = currentIndex;
		this.type = type;
		this.url = url;
	}
	
	/**
	 * @Descripton: TODO
	 * @Param @param url
	 * @Param @param type
	 * @Return $
	 * @Throws 
	 */
	public LoginReq(String url, int type) {
		super();
		this.url = url;
		this.type = type;
	}

	/**
	 * @Descripton: TODO
	 * @Param @param currentIndex
	 * @Param @param type
	 * @Param @param url
	 * @Return $
	 * @Throws 
	 */
	public LoginReq(int currentIndex, int type, String url) {
		super();
		this.currentIndex = currentIndex;
		this.type = type;
		this.url = url;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * @return the loginName
	 */
	public String getLoginName() {
		return loginName;
	}
	/**
	 * @param loginName the loginName to set
	 */
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	/**
	 * @return the loginPwd
	 */
	public String getLoginPwd() {
		return loginPwd;
	}
	/**
	 * @param loginPwd the loginPwd to set
	 */
	public void setLoginPwd(String loginPwd) {
		this.loginPwd = loginPwd;
	}
	/**
	 * @return the currentIndex
	 */
	public int getCurrentIndex() {
		return currentIndex;
	}
	/**
	 * @param currentIndex the currentIndex to set
	 */
	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}
	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LoginReq [url=" + url + ", loginName=" + loginName
				+ ", loginPwd=" + loginPwd + ", currentIndex=" + currentIndex
				+ ", type=" + type + "]";
	}
	
}
