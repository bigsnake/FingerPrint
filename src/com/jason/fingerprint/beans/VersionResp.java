/**
 * @Title: VersionResp.java
 * @Package: com.jason.fingerprint.beans
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2015年1月18日 下午8:23:52
 * @Version: V1.0
 */
package com.jason.fingerprint.beans;

import java.util.List;

/**
 * @ClassName: VersionResp
 * @Description: 版本更新
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2015年1月18日 下午8:23:52
 */
public class VersionResp extends BaseBean {

	private static final long serialVersionUID = -3724190796139638312L;
	
	private List<VersionBean> values;

	/**
	 * @return the values
	 */
	public List<VersionBean> getValues() {
		return values;
	}

	/**
	 * @param values the values to set
	 */
	public void setValues(List<VersionBean> values) {
		this.values = values;
	}
	
	/***
	 * 判断当前返回的数据是否为空
	 */
	public boolean isEmpty(){
		
		return !isSuccess() || (values == null || values.isEmpty() || values.size() < 1);
	}
	
	/***
	 * 判断当期是否是最后一页
	 */
	public boolean isLastPage(){
		return getTotalPagesCount() == getCurrentIndex();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "VersionResp [values=" + values + ", getValues()=" + getValues()
				+ ", isEmpty()=" + isEmpty() + ", isLastPage()=" + isLastPage()
				+ ", getRespMsg()=" + getRespMsg() + ", getRespCode()="
				+ getRespCode() + ", getCurrentIndex()=" + getCurrentIndex()
				+ ", getOffset()=" + getOffset() + ", getTotalRowsCount()="
				+ getTotalRowsCount() + ", getTotalPagesCount()="
				+ getTotalPagesCount() + ", getPrevious()=" + getPrevious()
				+ ", getNext()=" + getNext() + ", isSuccess()=" + isSuccess()
				+ ", toString()=" + super.toString() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + "]";
	}
	
}
