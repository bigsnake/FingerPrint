package com.jason.fingerprint.beans;

import java.io.Serializable;

import org.kymjs.aframe.database.annotate.Id;
import org.kymjs.aframe.database.annotate.Table;

@Table(name = "table_organ")
public class OrganBean implements Serializable {

	@Id()
	private int id;
	
	private int machId; // 机构ID

	private int parentId; // 父节点

	private String machNo; // 机构编号

	private String machName; // 机构名称

	private String telephone; // 电话号码

	private String fax; // 传真

	private String email; // email

	private String linkMan; // 联系人

	private String address; // 地址

	private String postCode; // 邮编

	private String flag; // 加区分乡镇与街道 1、市 2、区 3、县 4、乡镇 5、街道

	private String orderNo;// 机构排序编号

	private Double organOrder; // 机构排序

	private int zcrs;// 在册人数
	
	private String imie;//终端IMIE号;
	
	
	/**
	 * @param zcrs the zcrs to set
	 */
	public void setZcrs(int zcrs) {
		this.zcrs = zcrs;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	public int getMachId() {
		return machId;
	}

	public void setMachId(int machId) {
		this.machId = machId;
	}

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	public String getMachNo() {
		return machNo;
	}

	public void setMachNo(String machNo) {
		this.machNo = machNo;
	}

	public String getMachName() {
		return machName;
	}

	public void setMachName(String machName) {
		this.machName = machName;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLinkMan() {
		return linkMan;
	}

	public void setLinkMan(String linkMan) {
		this.linkMan = linkMan;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public Double getOrganOrder() {
		return organOrder;
	}

	public void setOrganOrder(Double organOrder) {
		this.organOrder = organOrder;
	}

	public Integer getZcrs() {
		return zcrs;
	}

	public void setZcrs(Integer zcrs) {
		this.zcrs = zcrs;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "OrganBean [id=" + id + ", machId=" + machId + ", parentId="
				+ parentId + ", machNo=" + machNo + ", machName=" + machName
				+ ", telephone=" + telephone + ", fax=" + fax + ", email="
				+ email + ", linkMan=" + linkMan + ", address=" + address
				+ ", postCode=" + postCode + ", flag=" + flag + ", orderNo="
				+ orderNo + ", organOrder=" + organOrder + ", zcrs=" + zcrs
				+ ", imie=" + imie + "]";
	}
	
}
