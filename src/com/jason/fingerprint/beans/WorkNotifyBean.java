/**
 * @Title: WorkNotifyBean.java
 * @Package: com.jason.fingerprint.beans
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月4日 下午10:42:39
 * @Version: V1.0
 */
package com.jason.fingerprint.beans;

import java.io.Serializable;

import org.kymjs.aframe.database.annotate.Id;
import org.kymjs.aframe.database.annotate.Table;

/**
 * @ClassName: WorkNotifyBean
 * @Description: 工作通知详细
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月4日 下午10:42:39
 */
@Table(name = "table_workNotify")
public class WorkNotifyBean implements Serializable {

	@Id()
	private int notifyId;
	
	private String id;
	
	private String pubTime;
	
	private String machId;
	
	private String infoType;
	
	private String pubUnit;
	
	private String isMajor;
	
	private String STitle;
	
	private String contentUrl;

	/**
	 * @return the notifyId
	 */
	public int getNotifyId() {
		return notifyId;
	}

	/**
	 * @param notifyId the notifyId to set
	 */
	public void setNotifyId(int notifyId) {
		this.notifyId = notifyId;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the pubTime
	 */
	public String getPubTime() {
		return pubTime;
	}

	/**
	 * @param pubTime the pubTime to set
	 */
	public void setPubTime(String pubTime) {
		this.pubTime = pubTime;
	}

	/**
	 * @return the machId
	 */
	public String getMachId() {
		return machId;
	}

	/**
	 * @param machId the machId to set
	 */
	public void setMachId(String machId) {
		this.machId = machId;
	}

	/**
	 * @return the infoType
	 */
	public String getInfoType() {
		return infoType;
	}

	/**
	 * @param infoType the infoType to set
	 */
	public void setInfoType(String infoType) {
		this.infoType = infoType;
	}

	/**
	 * @return the pubUnit
	 */
	public String getPubUnit() {
		return pubUnit;
	}

	/**
	 * @param pubUnit the pubUnit to set
	 */
	public void setPubUnit(String pubUnit) {
		this.pubUnit = pubUnit;
	}

	/**
	 * @return the isMajor
	 */
	public String getIsMajor() {
		return isMajor;
	}

	/**
	 * @param isMajor the isMajor to set
	 */
	public void setIsMajor(String isMajor) {
		this.isMajor = isMajor;
	}

	/**
	 * @return the sTitle
	 */
	public String getSTitle() {
		return STitle;
	}

	/**
	 * @param sTitle the sTitle to set
	 */
	public void setSTitle(String sTitle) {
		STitle = sTitle;
	}

	/**
	 * @return the contentUrl
	 */
	public String getContentUrl() {
		return contentUrl;
	}

	/**
	 * @param contentUrl the contentUrl to set
	 */
	public void setContentUrl(String contentUrl) {
		this.contentUrl = contentUrl;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "WorkNotifyBean [notifyId=" + notifyId + ", id=" + id
				+ ", pubTime=" + pubTime + ", machId=" + machId + ", infoType="
				+ infoType + ", pubUnit=" + pubUnit + ", isMajor=" + isMajor
				+ ", STitle=" + STitle + ", contentUrl=" + contentUrl + "]";
	}

}
