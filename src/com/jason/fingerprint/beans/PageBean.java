package com.jason.fingerprint.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jason.fingerprint.utils.CodeHelper;


/**
 * 
 * <对搜索分页时进行查询条件保存> <功能详细描述>
 * 
 * @author qiaochunguang
 * @version [版本号, Nov 18, 2011]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class PageBean implements Serializable {

	/**
	 * @Fields serialVersionUID
	 */
	private static final long serialVersionUID = -123356884225428174L;
	
	private Integer currentIndex;// 当前页

	private Integer offset;// 每页显示多少条

	// 总数据量
	private Integer totalRowsCount;

	// 总页数
	private Integer totalPagesCount;

	// 前一页页码
	private Integer previous;

	// 后一页页码
	private Integer next;

	// 当前页封装的数据
	private List<? extends Object> values;

	// 临近的页码
	private List<Integer> nearest;

	// 参数列表,检索条件
	private Map<String, Object> parameters;
	
	
	/**
	 * @return the currentIndex
	 */
	public Integer getCurrentIndex() {
		return currentIndex;
	}

	/**
	 * @param currentIndex the currentIndex to set
	 */
	public void setCurrentIndex(Integer currentIndex) {
		this.currentIndex = currentIndex;
	}

	/**
	 * @return the offset
	 */
	public Integer getOffset() {
		return offset;
	}

	/**
	 * @param offset the offset to set
	 */
	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	/**
	 * 获取总记录数<br />
	 * 未赋值时,结果为NULL
	 * 
	 * @return 总记录数
	 */
	public Integer getTotalRowsCount() {
		return totalRowsCount;
	}

	/**
	 * 配置总记录数
	 * 
	 * @param totalRowsCount
	 *            总记录数
	 */
	public void setTotalRowsCount(Integer totalRowsCount) {
		this.totalRowsCount = totalRowsCount;
		this.calc();
	}

	/**
	 * 获取总页数
	 * 
	 * @return 总页数
	 */
	public Integer getTotalPagesCount() {
		// 总记录数为NULL或0时,总页数为0
		if (CodeHelper.isNull(this.getTotalRowsCount())
				|| this.getTotalRowsCount().equals(0)) {
			this.totalPagesCount = 0;
		}
		return totalPagesCount;
	}

	/**
	 * 配置总页数,函数由自身调用
	 * 
	 * @param totalPagesCount
	 *            总页数
	 */
	private void setTotalPagesCount(Integer totalPagesCount) {
		this.totalPagesCount = totalPagesCount;
	}

	/**
	 * 获取前一页的页码
	 * 
	 * @return 前一页页面
	 */
	public Integer getPrevious() {
		return previous;
	}

	/**
	 * 配置前一页页面,函数由自身调用
	 * 
	 * @param previous
	 *            前一页页码
	 */
	private void setPrevious(Integer previous) {
		this.previous = previous;
	}

	/**
	 * 获取下一页页面
	 * 
	 * @return 下一页页码
	 */
	public Integer getNext() {
		return next;
	}

	/**
	 * 配置下一页页码,函数由自身调用
	 * 
	 * @param next
	 *            下一页页码
	 */
	private void setNext(Integer next) {
		this.next = next;
	}

	/**
	 * 获取第一页页码
	 * 
	 * @return 常量值1
	 */
	public Integer getFirst() {
		return 1;
	}

	/**
	 * 获取最后一页页码
	 * 
	 * @return 返回最大页数
	 */
	public Integer getLast() {
		return this.getTotalPagesCount();
	}

	/**
	 * 获取当前页数中的对象列表
	 * 
	 * @return 当前页数中的对象列表
	 */
	public List<? extends Object> getValues() {
		if (CodeHelper.isNull(this.values)) {
			this.values = new ArrayList<Object>();
		}
		return values;
	}

	/**
	 * 配置当前页数中的对象列表
	 * 
	 * @param values
	 */
	public void setValues(List<? extends Object> values) {
		this.values = values;
	}

	/**
	 * 获取当前页面所在的10页页码
	 * 
	 * @return 当前页码所在的10页页码
	 */
	public List<Integer> getNearest() {
		if (CodeHelper.isNull(this.nearest)) {
			this.nearest = new ArrayList<Integer>();
		}
		return nearest;
	}

	/**
	 * SerialVersionUID
	 * 
	 * @return SerialVersionUID
	 */
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	/**
	 * 获取参数列表、检索条件信息
	 * 
	 * @return 参数列表、检索条件
	 */
	public Map<String, Object> getParameters() {
		if (CodeHelper.isNull(this.parameters)) {
			this.parameters = new HashMap<String, Object>();
		}
		return parameters;
	}

	/**
	 * 配置参数信息
	 * 
	 * @param parameters
	 *            参数信息、检索条件
	 */
	public void setParameters(Map<String, Object> parameters) {
		this.parameters = parameters;
	}

	/**
	 * 获取查询开始记录
	 */
	public Integer getStartRecord() {
		return CodeHelper.isNotEmpty(this.getCurrentIndex()) ? (getCurrentIndex() - 1)
				* getOffset() : 0;
	}

	/**
	 * 获取查询结束记录
	 */
	public Integer getEndRecord() {
		return CodeHelper.isNotEmpty(this.getStartRecord()) ? (this
				.getStartRecord() + this.getOffset()) : this.getOffset();
	}

	/**
	 * 计算参数
	 */
	private void calc() {
		if (CodeHelper.isNotNull(this.getTotalRowsCount())) {
			// 计算总页数
			double rows = Double.valueOf(this.getTotalRowsCount().toString());
			double offset = Double.valueOf(getOffset());
			double pages = Math.ceil(rows / offset);
			this.setTotalPagesCount(Integer.valueOf(Double.valueOf(pages)
					.intValue()));
			// 计算临近的页码数
			int start = (getCurrentIndex() - 1) / 10 * 10 + 1;
			int end = start + 9;
			if (end > this.getTotalPagesCount().intValue()) {
				end = this.getTotalPagesCount().intValue();
			}
			this.getNearest().clear();
			int index = 0;
			for (int i = start; i <= end; i++) {
				this.getNearest().add(index, Integer.valueOf(i));
				index++;
			}
			// 计算前一页
			this.setPrevious(Integer
					.valueOf(getCurrentIndex() - 1));
			if (this.getPrevious().intValue() < 1) {
				this.setPrevious(1);
			}
			// 计算下一页
			this
					.setNext(Integer
							.valueOf(getCurrentIndex() + 1));
			if (this.getNext().intValue() > this.getTotalPagesCount()) {
				this.setNext(this.getTotalPagesCount());
			}
		}
	}
}
