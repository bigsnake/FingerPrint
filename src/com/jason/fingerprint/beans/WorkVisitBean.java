/**
 * @Title: WorkVisitBean.java
 * @Package: com.jason.fingerprint.beans
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月25日 上午11:18:33
 * @Version: V1.0
 */
package com.jason.fingerprint.beans;

import java.io.Serializable;

import org.kymjs.aframe.database.annotate.Id;
import org.kymjs.aframe.database.annotate.Table;

/**
 * @ClassName: WorkVisitBean
 * @Description: 上传走访记录
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月25日 上午11:18:33
 */
@Table(name = "table_workVisit")
public class WorkVisitBean implements Serializable {
	
	@Id
	private int id;//主键，自增长
	
	private int workId;//网络返回的走访记录id
	
	private String userId;//工作人员id
	
	private String userName;//工作人员名字
	
	private String rymcId;//矫正人员id
	
	private String rymcName;//矫正人员名字
	
	private String fileNumber;//矫正人员档案号
	
	private String machId;// 矫正人员所属机构ID
	
	private String machName;//矫正人员所属机构
	
	private long interViewTime;//走访时间
	
	private String interViewAddress;//走访地点
	
	private String longitude;//经度
	
	private String latitude;//纬度
	
	private String interViewWqk;//访问情况
	
	private int state;//是否上传 1，没有上传 2已经上传

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the workId
	 */
	public int getWorkId() {
		return workId;
	}

	/**
	 * @param workId the workId to set
	 */
	public void setWorkId(int workId) {
		this.workId = workId;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the rymcId
	 */
	public String getRymcId() {
		return rymcId;
	}

	/**
	 * @param rymcId the rymcId to set
	 */
	public void setRymcId(String rymcId) {
		this.rymcId = rymcId;
	}

	/**
	 * @return the interViewTime
	 */
	public long getInterViewTime() {
		return interViewTime;
	}

	/**
	 * @param interViewTime the interViewTime to set
	 */
	public void setInterViewTime(long interViewTime) {
		this.interViewTime = interViewTime;
	}

	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the latitude
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the interViewWqk
	 */
	public String getInterViewWqk() {
		return interViewWqk;
	}

	/**
	 * @param interViewWqk the interViewWqk to set
	 */
	public void setInterViewWqk(String interViewWqk) {
		this.interViewWqk = interViewWqk;
	}

	/**
	 * @return the state
	 */
	public int getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(int state) {
		this.state = state;
	}
	

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the rymcName
	 */
	public String getRymcName() {
		return rymcName;
	}

	/**
	 * @param rymcName the rymcName to set
	 */
	public void setRymcName(String rymcName) {
		this.rymcName = rymcName;
	}

	/**
	 * @return the fileNumber
	 */
	public String getFileNumber() {
		return fileNumber;
	}

	/**
	 * @param fileNumber the fileNumber to set
	 */
	public void setFileNumber(String fileNumber) {
		this.fileNumber = fileNumber;
	}

	/**
	 * @return the machId
	 */
	public String getMachId() {
		return machId;
	}

	/**
	 * @param machId the machId to set
	 */
	public void setMachId(String machId) {
		this.machId = machId;
	}

	/**
	 * @return the machName
	 */
	public String getMachName() {
		return machName;
	}

	/**
	 * @param machName the machName to set
	 */
	public void setMachName(String machName) {
		this.machName = machName;
	}

	/**
	 * @return the interViewAddress
	 */
	public String getInterViewAddress() {
		return interViewAddress;
	}

	/**
	 * @param interViewAddress the interViewAddress to set
	 */
	public void setInterViewAddress(String interViewAddress) {
		this.interViewAddress = interViewAddress;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "WorkVisitBean [id=" + id + ", workId=" + workId + ", userId="
				+ userId + ", userName=" + userName + ", rymcId=" + rymcId
				+ ", rymcName=" + rymcName + ", fileNumber=" + fileNumber
				+ ", machId=" + machId + ", machName=" + machName
				+ ", interViewTime=" + interViewTime + ", interViewAddress="
				+ interViewAddress + ", longitude=" + longitude + ", latitude="
				+ latitude + ", interViewWqk=" + interViewWqk + ", state="
				+ state + "]";
	}

}
