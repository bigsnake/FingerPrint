/**
 * @Title: WorkVisitResp.java
 * @Package: com.jason.fingerprint.beans
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月25日 下午8:47:40
 * @Version: V1.0
 */
package com.jason.fingerprint.beans;

import java.util.List;

/**
 * @ClassName: WorkVisitResp
 * @Description: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月25日 下午8:47:41
 */
public class WorkVisitResp extends BaseBean {

	private static final long serialVersionUID = 1L;
	
	private List<String> values;

	/**
	 * @return the values
	 */
	public List<String> getValues() {
		return values;
	}

	/**
	 * @param values the values to set
	 */
	public void setValues(List<String> values) {
		this.values = values;
	}
	
	/***
	 * 判断当前返回的数据是否为空
	 */
	public boolean isEmpty(){
		
		return !isSuccess() || (values == null || values.isEmpty() || values.size() < 1);
	}
	
	/***
	 * 判断当期是否是最后一页
	 */
	public boolean isLastPage(){
		return getTotalPagesCount() == getCurrentIndex();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "WorkVisitResp [values=" + values + ", getRespMsg()="
				+ getRespMsg() + ", getRespCode()=" + getRespCode()
				+ ", getCurrentIndex()=" + getCurrentIndex() + ", getOffset()="
				+ getOffset() + ", getTotalRowsCount()=" + getTotalRowsCount()
				+ ", getTotalPagesCount()=" + getTotalPagesCount()
				+ ", getPrevious()=" + getPrevious() + ", getNext()="
				+ getNext() + ", isSuccess()=" + isSuccess() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}
	
}
