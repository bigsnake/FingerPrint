/**
 * @Title: DailyReportBean.java
 * @Package: com.jason.fingerprint.beans
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月9日 下午10:27:27
 * @Version: V1.0
 */
package com.jason.fingerprint.beans;

import java.io.Serializable;

import org.kymjs.aframe.database.annotate.Id;
import org.kymjs.aframe.database.annotate.Table;

/**
 * @ClassName: DailyReportBean
 * @Description: 日常报到
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月9日 下午10:27:27
 */
@Table(name = "table_dailyReport")
public class DailyReportBean implements Serializable {
	
	@Id
	private int id;
	
	private String rymcId;	//人员Id
	
	private String rymcName;	//人员名字
	
	private long startTime;	//签到开始时间
	
	private long endTime;		//签到结束时间
	
	private long updateTime;	//数据修改时间
	
	private int state;//当前状态，1-默认，"未上传"，2-"已上传"
	
	/**
	 * @Descripton: TODO
	 * @Param 
	 * @Return $
	 * @Throws 
	 */
	public DailyReportBean() {
		super();
	}

	/**
	 * @Descripton: TODO
	 * @Param @param rymcId
	 * @Param @param rymcName
	 * @Param @param startTime
	 * @Param @param endTime
	 * @Param @param updateTime
	 * @Param @param state
	 * @Return $
	 * @Throws 
	 */
	public DailyReportBean(String rymcId, String rymcName, long startTime,
			long endTime, long updateTime, int state) {
		super();
		this.rymcId = rymcId;
		this.rymcName = rymcName;
		this.startTime = startTime;
		this.endTime = endTime;
		this.updateTime = updateTime;
		this.state = state;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the rymcId
	 */
	public String getRymcId() {
		return rymcId;
	}

	/**
	 * @param rymcId the rymcId to set
	 */
	public void setRymcId(String rymcId) {
		this.rymcId = rymcId;
	}

	/**
	 * @return the rymcName
	 */
	public String getRymcName() {
		return rymcName;
	}

	/**
	 * @param rymcName the rymcName to set
	 */
	public void setRymcName(String rymcName) {
		this.rymcName = rymcName;
	}


	/**
	 * @return the startTime
	 */
	public long getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return the endTime
	 */
	public long getEndTime() {
		return endTime;
	}

	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	/**
	 * @return the updateTime
	 */
	public long getUpdateTime() {
		return updateTime;
	}

	/**
	 * @param updateTime the updateTime to set
	 */
	public void setUpdateTime(long updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return the state
	 */
	public int getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(int state) {
		this.state = state;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DailyReportBean [id=" + id + ", rymcId=" + rymcId
				+ ", rymcName=" + rymcName + ", startTime=" + startTime
				+ ", endTime=" + endTime + ", updateTime=" + updateTime
				+ ", state=" + state + "]";
	}
	
}
