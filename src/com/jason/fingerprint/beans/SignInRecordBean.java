package com.jason.fingerprint.beans;

import java.io.Serializable;

import org.kymjs.aframe.database.annotate.Id;
import org.kymjs.aframe.database.annotate.Table;

/**
 * 
 * @ClassName: SignInRecordBean
 * @Description: 该类描述了矫正人员的签到记录
 * @author John
 * 
 */
@Table(name = "table_signInRecord")
public class SignInRecordBean implements Serializable {

	@Id()
	private int id;

	private String time; // 签到时间

	private String flag; // 类型 0开始签到，1点名签到，2结束签到

	private String sponsorId; // 参加活动id

	private String sponsorType; // 签到方式 0指纹，1，平台录入，2打卡

	private String type; // 签到类型（ 0集中教育、1集中劳动、2个别教育、3个别劳动）

	private String imie; // IMIE号

	private String electronicsId; // 人员ID

	private String name; // 姓名

	private int state; // 签到状态，1-默认，"未上传"，2-"已上传"

	private String eduComld; // 新增签到记录ID，用于上传个别教育、个别劳动照片

	private String filePath; // 资源路径，仅用于个别教育、个别劳动照片上传


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getSponsorId() {
		return sponsorId;
	}

	public void setSponsorId(String sponsorId) {
		this.sponsorId = sponsorId;
	}

	public String getSponsorType() {
		return sponsorType;
	}

	public void setSponsorType(String sponsorType) {
		this.sponsorType = sponsorType;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getElectronicsId() {
		return electronicsId;
	}

	public void setElectronicsId(String electronicsId) {
		this.electronicsId = electronicsId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getEduComld() {
		return eduComld;
	}

	public void setEduComld(String eduComld) {
		this.eduComld = eduComld;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SignInRecordBean [id=" + id + ", time=" + time + ", flag="
				+ flag + ", sponsorId=" + sponsorId + ", sponsorType="
				+ sponsorType + ", type=" + type + ", imie=" + imie
				+ ", electronicsId=" + electronicsId + ", name=" + name
				+ ", state=" + state + ", eduComld=" + eduComld + ", filePath="
				+ filePath + "]";
	}
	
}
