/**
 * @Title: WorkNotifyResp.java
 * @Package: com.jason.fingerprint.beans
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月4日 下午10:41:39
 * @Version: V1.0
 */
package com.jason.fingerprint.beans;

import java.util.List;

/**
 * @ClassName: WorkNotifyResp
 * @Description: 工作通知
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月4日 下午10:41:39
 */
public class WorkNotifyResp extends BaseBean {

	private static final long serialVersionUID = 6516869248471320593L;
	
	private List<WorkNotifyBean> values;

	/**
	 * @return the values
	 */
	public List<WorkNotifyBean> getValues() {
		return values;
	}

	/**
	 * @param values the values to set
	 */
	public void setValues(List<WorkNotifyBean> values) {
		this.values = values;
	}
	
	/***
	 * 判断当前返回的数据是否为空
	 */
	public boolean isEmpty(){
		
		return !isSuccess() || (values == null || values.isEmpty() || values.size() < 1);
	}
	
	/***
	 * 判断当期是否是最后一页
	 */
	public boolean isLastPage(){
		return getTotalPagesCount() == getCurrentIndex();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "WorkNotifyResp [values=" + values + ", getValues()="
				+ getValues() + ", isEmpty()=" + isEmpty() + ", isLastPage()="
				+ isLastPage() + ", getRespMsg()=" + getRespMsg()
				+ ", getRespCode()=" + getRespCode() + ", getCurrentIndex()="
				+ getCurrentIndex() + ", getOffset()=" + getOffset()
				+ ", getTotalRowsCount()=" + getTotalRowsCount()
				+ ", getTotalPagesCount()=" + getTotalPagesCount()
				+ ", getPrevious()=" + getPrevious() + ", getNext()="
				+ getNext() + ", isSuccess()=" + isSuccess() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}
	
}
