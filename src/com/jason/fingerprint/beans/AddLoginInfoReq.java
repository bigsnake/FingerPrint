/**
 * @Title: AddLoginInfoReq.java
 * @Package: com.jason.fingerprint.beans
 * @Descripton: TODO
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月4日 下午11:29:24
 * @Version: V1.0
 */
package com.jason.fingerprint.beans;

import java.io.Serializable;

/**
 * @ClassName: AddLoginInfoReq
 * @Description: 添加登录信息
 * @Author: Jason.Zhang zhangyujn1989ok@gmail.com
 * @Date: 2014年11月4日 下午11:29:24
 */
public class AddLoginInfoReq implements Serializable {

	private static final long serialVersionUID = 7600201451779616222L;
	
	private int type;	//登录类型
	private String loginName;	//登录名
	private String userName; //实际姓名
	private String organ;	//组织机构名称
	private String organId;	//组织机构ID
	
	/**
	 * @Descripton: TODO
	 * @Param 
	 * @Return $
	 * @Throws 
	 */
	public AddLoginInfoReq() {
		super();
	}
	
	/**
	 * @Descripton: TODO
	 * @Param @param type
	 * @Param @param loginName
	 * @Param @param userName
	 * @Param @param organ
	 * @Param @param organId
	 * @Return $
	 * @Throws 
	 */
	public AddLoginInfoReq(int type, String loginName, String userName,
			String organ, String organId) {
		super();
		this.type = type;
		this.loginName = loginName;
		this.userName = userName;
		this.organ = organ;
		this.organId = organId;
	}
	
	
	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}
	/**
	 * @return the loginName
	 */
	public String getLoginName() {
		return loginName;
	}
	/**
	 * @param loginName the loginName to set
	 */
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the organ
	 */
	public String getOrgan() {
		return organ;
	}
	/**
	 * @param organ the organ to set
	 */
	public void setOrgan(String organ) {
		this.organ = organ;
	}
	/**
	 * @return the organId
	 */
	public String getOrganId() {
		return organId;
	}
	/**
	 * @param organId the organId to set
	 */
	public void setOrganId(String organId) {
		this.organId = organId;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AddLoginInfoReq [type=" + type + ", loginName=" + loginName
				+ ", userName=" + userName + ", organ=" + organ + ", organId="
				+ organId + "]";
	}
	
}
