package com.jason.fingerprint.beans;

import java.io.Serializable;

/**
 * 社区服务、集中教育发起表
 * 
 * @author 薛会生
 * 
 */
public class SponsorBean implements Serializable {

	private Integer id;// 集中教育、劳动发起表ID   手机用到的

	private String time;// 发起时间        手机用到的

	private String content;// 发起内容               手机用到的

	private String address;// 地点                         手机用到的
	
	private String type;// 发起类型（0:集中教育，1：社区服务）  手机用到的
	
	private String organName;//活动发起组织单位     手机用到的

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOrganName() {
		return organName;
	}

	public void setOrganName(String organName) {
		this.organName = organName;
	}

	
	
	//-------------------------------------------------

	


}
