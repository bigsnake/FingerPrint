package com.jason.fingerprint.beans;

import java.util.List;

/**
 * @ClassName: SponsorResp
 * @Description: TODO
 * @Author: John
 * @Date: 2014/10/21
 */
public class SponsorResp extends BaseBean {

	private static final long serialVersionUID = -6456759724507936719L;

	private List<SponsorBean> values;

	/**
	 * @return the values
	 */
	public List<SponsorBean> getValues() {
		return values;
	}

	/**
	 * @param values the values to set
	 */
	public void setValues(List<SponsorBean> values) {
		this.values = values;
	}

	/***
	 * 判断当前返回的数据是否为空
	 */
	public boolean isEmpty() {
		return !isSuccess() || (values == null || values.isEmpty() || values.size() < 1);
	}

	/***
	 * 判断当期是否是最后一页
	 */
	public boolean isLastPage() {
		return getTotalPagesCount() == getCurrentIndex();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SponsorResp [values=" + values + ", getRespMsg()="
				+ getRespMsg() + ", getRespCode()=" + getRespCode()
				+ ", getCurrentIndex()=" + getCurrentIndex() + ", getOffset()="
				+ getOffset() + ", getTotalRowsCount()=" + getTotalRowsCount()
				+ ", getTotalPagesCount()=" + getTotalPagesCount()
				+ ", getPrevious()=" + getPrevious() + ", getNext()="
				+ getNext() + ", isSuccess()=" + isSuccess() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}

}
